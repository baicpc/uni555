//
//  BaseEntity.swift
//  Rest
//
//  Created by Delphinus on 6/6/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
public class RestEntity:NSObject,Mappable {
    public var status: String!
    public var message: String!
    public var records: String!

    
    public var FilePathUrlImage:String!
    var dicQuery:NSMutableDictionary!
    var rawQuery:NSString!=""
    
    @objc public override init(){
        super.init()
        //initCache()
    }

    public required init?(_ map: Map) {
        super.init()
        defineMapping(map)
    }

    
    public func defineMapping(map: Map) {
       // initCache()
        defineResponseMapping(map)
    }
    /**
    *   define responseMapping
    */
    
    public func defineResponseMapping (map: Map) {
       
        status              <~ map["status"]
        message             <~ map["message"]
        records             <~ map["records"]

    }

    /*
    *   add query
    */
    public func addQuery(keyPathToAttributeNames:NSDictionary){
        if dicQuery == nil { dicQuery = NSMutableDictionary()}
        self.dicQuery.addEntriesFromDictionary(keyPathToAttributeNames as [NSObject : AnyObject])
    }
    /*
    *   compile query
    */
    private func complieQuery(){
        if self.dicQuery != nil {
            var jsonData: NSData?
            do {
                jsonData = try NSJSONSerialization.dataWithJSONObject(self.dicQuery, options: NSJSONWritingOptions())
                self.rawQuery = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
            } catch{
                jsonData = nil
            }
            
        }
        
    }
    /*
    *   define request
    */
    public func defineRequestMapping()->[String : AnyObject]?{
        self.complieQuery()
        return self.rawQuery.length > 0 ? ["q":self.rawQuery ] : nil
        let x = self.rawQuery
        print(self.rawQuery)
    }
}