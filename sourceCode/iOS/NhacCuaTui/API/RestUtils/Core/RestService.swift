//
//  BaseService.swift
//  Rest
//
//  Created by Delphinus on 6/6/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation

@available(iOS 8.0, *)
public class RestService:OAuth1Manager{
    
    public override init(options: [String : String]) {
        super.init(options: options)
    }
    
    public required init(configuration: NSURLSessionConfiguration?) {
        fatalError("init(configuration:) has not been implemented")
    }
    
    /*
    *   upload file
    */
    public func upload<T:Mappable>(api:String,requestEntity:RestEntity,data:NSData,progress:((Double)->Void)?,success:((T?)->Void)?,failure:((T?,String)->Void)?){
        var url1 = "http://wsdev.betteradvice.com/upload.php"

        super.upload(Method.POST, api, parameters: requestEntity.defineRequestMapping(), encoding: ParameterEncoding.URL, data: data)
            .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                progress!(Double(totalBytesWritten)/Double(totalBytesExpectedToWrite))
            }
            .responseObject { (request, response, responseEntity: T?, JSON, error) -> Void in
                print("> " + NSDate().description)
                print(Method.POST.rawValue + " '" + self.baseURL.URLByAppendingPathComponent(api).description + "'")
                print("request.body=" + (requestEntity.rawQuery as String))
                
                if responseEntity != nil {
                   // print("response.body=" + Mapper<T>().toJSONString(responseEntity!, prettyPrint: true)! as String)
                    let tmp = responseEntity as! RestEntity
                    
                    if error == nil {
                        if tmp.message == nil{
                            failure!(nil,"Error")
                        }else{
                            if tmp.message == "Error"{
                                failure!(responseEntity!,tmp.message)
                            }else{
                                if tmp.message == "Success" {
                                    success!(responseEntity!)
                                }else{
                                    failure!(nil,tmp.message)
                                }
                            }
                        }
                    }else{
                        failure!(nil,error!.localizedDescription)
                    }
                    
                }else{
                    failure!(nil,"NETWORK ERROR")
                }
                
                
        }
        
    }
    
    /*
    *   reponse T class
    */
    public func request<T: Mappable>(method:Method,api:String,requestEntity:RestEntity,success:((T?)->Void)?,failure:((T?,String)->Void)?){
        
        
        let parameters = requestEntity.defineRequestMapping()
        super.request(method, api, parameters: parameters, encoding: ParameterEncoding.URL).responseObject { (request, response, responseEntity: T?, JSON, error) -> Void in
        
//                print("> " + NSDate().description)
                print(method.rawValue + " '" + self.baseURL.URLByAppendingPathComponent(api).description + "'")
                print("request.body=" + (requestEntity.rawQuery as String))
            
                if responseEntity != nil {
                    //print("response.body=" + Mapper<T>().toJSONString(responseEntity!, prettyPrint: true)! as String)
                    let tmp = responseEntity as! RestEntity
                    
                    if error == nil {
                        if tmp.message == nil{
                            failure!(nil,"Error")
                        }else{
                            if tmp.message == "Error"{
                                failure!(responseEntity!,tmp.message)
                            }else{
                                if tmp.message == "Find success!" {
                                    success!(responseEntity!)
                                }else{
                                    failure!(nil,tmp.message)
                                }
                            }
                        }
                    }else{
                        //failure!(nil,error!.localizedDescription)
                    }
                }else{
                    
                    //failure!(nil,"NETWORK ERROR")
                }
                
                
        }
        
    }
}