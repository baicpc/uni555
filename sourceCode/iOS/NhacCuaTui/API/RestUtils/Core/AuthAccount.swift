//
//  Account.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation

@available(iOS 8.0, *)

@available(iOS 8.0, *)
public class AuthAccount : NSObject {
    
    public unowned let manager  : AuthManager
    public var userID           : String?
    public var username         : String?
    public var userInfo         : [String: AnyObject]?
    
    
    @available(iOS 8.0, *)
    public init(manager: AuthManager) {
        self.manager = manager
    }
}