//
//  AuthManager.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
@available(iOS 8.0, *)
public protocol AuthManagerDelegate {
    
    func didCancelAuthentication(manager:AuthManager)
    func didFinishAuthentication(manager:AuthManager, account:AuthAccount)
}


@available(iOS 8.0, *)
public class AuthManager : Manager {
    
    public let baseURL            : NSURL!
    public var keychainIdentifier : String?
    public var idKey              : String?
    public var usernameKey        : String?
    public var authDelegate       : AuthManagerDelegate?
    public var account            : AuthAccount?
    
    
    public init(baseURL:NSURL) {
        self.baseURL = baseURL
        super.init(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    }
    
    
    public init(options: [String:String]) {
        var url : NSURL?
        if options["baseURL"] != nil { url = NSURL(string: options["baseURL"]!) }
        var x = AppsSettings.Static.BASE_URL
        self.baseURL = url
        self.keychainIdentifier = options["keychainIdentifier"]
        self.idKey = options["idKey"]
        self.usernameKey = options["usernameKey"]
        let configuration: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
        super.init(configuration: configuration)
        if self.baseURL == nil {
            print("Base URL is missing")
           // return nil
        }
    }
    
    
    public required init(configuration: NSURLSessionConfiguration?) {
        fatalError("init(configuration:) has not been implemented")
        
    }
    

    public override func request(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?, encoding: ParameterEncoding) -> Request {
        let url = self.baseURL.URLByAppendingPathComponent(URLString.URLString)
        return super.request(method, url.URLString, parameters: parameters, encoding: encoding)
    }
}