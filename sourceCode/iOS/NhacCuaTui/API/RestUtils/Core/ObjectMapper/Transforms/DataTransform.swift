//
//  DataTransform.swift
//  ecoinsystem
//
//  Created by Delphinus on 8/17/15.
//
//

import Foundation

public class DataBase64Transform: TransformType {
    public typealias Object = NSData
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(value: AnyObject?) -> NSData? {
        if let data = value as? String {
            return NSData(base64EncodedString: data, options:NSDataBase64DecodingOptions())
        }
        
        return nil
    }
    
    public func transformToJSON(value: NSData?) -> String? {
        if let data = value {
            return data.base64Encoded
        }
        return nil
    }
}
public class DataUtf8Transform: TransformType {
    public typealias Object = NSData
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(value: AnyObject?) -> NSData? {
        if let data = value as? String {
            return NSData(base64EncodedString: data, options:NSDataBase64DecodingOptions())
        }
        
        return nil
    }
    
    public func transformToJSON(value: NSData?) -> String? {
        if let data = value {
            return data.base64Encoded
        }
        return nil
    }
}
extension NSData {
    var utf8Decoded:String {
        return NSString(data: self, encoding: NSUTF8StringEncoding) as? String ?? ""
    }
    
    var base64Encoded : String {
        return self.base64EncodedStringWithOptions(NSDataBase64EncodingOptions()) ?? ""
    }
    
    
    
}
