//
//  URLTransform.swift
//  Rest
//
//  Created by Delphinus on 6/7/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation

public class URLTransform: TransformType {
    public typealias Object = NSURL
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(value: AnyObject?) -> NSURL? {
        if let URLString = value as? String {
            return NSURL(string: URLString)
        }
        return nil
    }
    
    public func transformToJSON(value: NSURL?) -> String? {
        if let URL = value {
            return URL.absoluteString
        }
        return nil
    }
}