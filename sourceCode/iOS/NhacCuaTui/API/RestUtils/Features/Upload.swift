//
//  Upload.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation

@available(iOS 8.0, *)
extension Manager {
    private enum Uploadable {
        case Data(NSURLRequest, NSData)
        case File(NSURLRequest, NSURL)
        case Stream(NSURLRequest, NSInputStream)
    }
    
    @available(iOS 8.0, *)
    private func upload(uploadable: Uploadable) -> Request {
        var uploadTask: NSURLSessionUploadTask!
        var HTTPBodyStream: NSInputStream?
        
        switch uploadable {
        case .Data(let request, let data):
            dispatch_sync(queue) {
                uploadTask = self.session.uploadTaskWithRequest(request, fromData: data)
            }
        case .File(let request, let fileURL):
            dispatch_sync(queue) {
                uploadTask = self.session.uploadTaskWithRequest(request, fromFile: fileURL)
            }
        case .Stream(let request, let stream):
            dispatch_sync(queue) {
                uploadTask = self.session.uploadTaskWithStreamedRequest(request)
            }
            
            HTTPBodyStream = stream
        }
        
        let request = Request(session: session, task: uploadTask)
        
        if HTTPBodyStream != nil {
            request.delegate.taskNeedNewBodyStream = { _, _ in
                return HTTPBodyStream
            }
        }
        
        delegate[request.delegate.task] = request.delegate
        
        if startRequestsImmediately {
            request.resume()
        }
        
        return request
    }
    
    // MARK: File
    
    /**
    Creates a request for uploading a file to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter URLRequest: The URL request
    - parameter file: The file to upload
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(URLRequest: URLRequestConvertible, file: NSURL) -> Request {
        return Manager.sharedInstance.upload(.File(URLRequest.URLRequest, file))
    }
    
    /**
    Creates a request for uploading a file to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter method: The HTTP method.
    - parameter URLString: The URL string.
    - parameter headers: The HTTP headers. `nil` by default.
    - parameter file: The file to upload.
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(method: Method, _ URLString: URLStringConvertible, headers: [String: String]? = nil, file: NSURL) -> Request {
        let mutableURLRequest = URLRequest(method, URLString: URLString, headers: headers)
        return upload(mutableURLRequest, file: file)
    }
    
    // MARK: Data
    
    /**
    Creates a request for uploading data to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter URLRequest: The URL request.
    - parameter data: The data to upload.
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(URLRequest: URLRequestConvertible, data: NSData) -> Request {
        return Manager.sharedInstance.upload(.Data(URLRequest.URLRequest, data))
    }
    
    /**
    Creates a request for uploading data to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter method: The HTTP method.
    - parameter URLString: The URL string.
    - parameter headers: The HTTP headers. `nil` by default.
    - parameter data: The data to upload.
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(method: Method, _ URLString: URLStringConvertible, headers: [String: String]? = nil, data: NSData) -> Request {
        let mutableURLRequest = URLRequest(method, URLString: URLString, headers: headers)
        
        return upload(mutableURLRequest, data: data)
    }
    
    // MARK: Stream
    
    /**
    Creates a request for uploading a stream to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter URLRequest: The URL request.
    - parameter stream: The stream to upload.
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(URLRequest: URLRequestConvertible, stream: NSInputStream) -> Request {
        return Manager.sharedInstance.upload(.Stream(URLRequest.URLRequest, stream))
    }
    
    /**
    Creates a request for uploading a stream to the specified URL request.
    
    If `startRequestsImmediately` is `true`, the request will have `resume()` called before being returned.
    
    - parameter method: The HTTP method.
    - parameter URLString: The URL string.
    - parameter headers: The HTTP headers. `nil` by default.
    - parameter stream: The stream to upload.
    
    - returns: The created upload request.
    */
    @available(iOS 8.0, *)
    public func upload(method: Method, _ URLString: URLStringConvertible, headers: [String: String]? = nil, stream: NSInputStream) -> Request {
        let mutableURLRequest = URLRequest(method, URLString: URLString, headers: headers)
        
        return upload(mutableURLRequest, stream: stream)
    }
}

// MARK: -

extension Request {
    
    // MARK: - UploadTaskDelegate
    
    class UploadTaskDelegate: DataTaskDelegate {
        var uploadTask: NSURLSessionUploadTask? { return task as? NSURLSessionUploadTask }
        var uploadProgress: ((Int64, Int64, Int64) -> Void)!
        
        // MARK: - NSURLSessionTaskDelegate
        
        // MARK: Override Closures
        
        var taskDidSendBodyData: ((NSURLSession, NSURLSessionTask, Int64, Int64, Int64) -> Void)?
        
        // MARK: Delegate Methods
        
        func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
            if taskDidSendBodyData != nil {
                taskDidSendBodyData!(session, task, bytesSent, totalBytesSent, totalBytesExpectedToSend)
            } else {
                progress.totalUnitCount = totalBytesExpectedToSend
                progress.completedUnitCount = totalBytesSent
                
                uploadProgress?(bytesSent, totalBytesSent, totalBytesExpectedToSend)
            }
        }
    }
}
