//
//  OAuth1.swift
//  Rest
//
//  Created by Delphinus on 6/5/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
#if os(iOS)
    import UIKit
    #else
    import AppKit
#endif


@available(iOS 8.0, *)
public class OAuth1Manager : AuthManager {
    
    let consumerKey             : String!
    let consumerSecret          : String!
    let requestTokenPath        : String!
    let authorizePath           : String!
    let accessTokenPath         : String!
    let callbackURL             : NSURL!
    
    let scope                   : String?
    let realm                   : String?
    let signatureMethod         : String?
    
    
    public var authorizeURL : NSURL? {
        if let key = (self.account as? OAuth1Account)?.requestToken?.key { return NSURL(string: "\(self.baseURL.URLString)/\(self.authorizePath)?oauth_token=\(key)")! }
        return nil
    }
    
    
    lazy var OAuthParameters : [String: AnyObject] = {
        var parameters = [String: AnyObject]()
        parameters["oauth_version"]           = "1.0"
        parameters["oauth_consumer_key"]      = self.consumerKey;
        parameters["oauth_timestamp"]         = String(Int64(NSDate().timeIntervalSince1970))
        parameters["oauth_signature_method"]  = "HMAC-SHA1";
        parameters["oauth_nonce"]             = (NSUUID().UUIDString as NSString).substringToIndex(8)
        return parameters
        }()
    
    
    
    
    
    public override init(options: [String : String]) {
        self.consumerKey = options["consumerKey"]
        self.consumerSecret = options["consumerSecret"]
        self.requestTokenPath = options["requestTokenPath"]
        self.authorizePath = options["authorizePath"]
        self.accessTokenPath = options["accessTokenPath"]
        var callbackURL : NSURL?
        if let url = options["callbackURL"] { callbackURL = NSURL(string: url) }
        self.callbackURL = callbackURL
        self.scope = options["scope"]
        self.realm = options["realm"]
        self.signatureMethod = options["signatureMethod"]
        super.init(options: options)
        if self.consumerKey == nil { print("consumerKey is missing") }
        if self.consumerSecret == nil { print("consumerSecret is missing") }
        if self.requestTokenPath == nil { print("requestTokenPath is missing") }
        if self.authorizePath == nil { print("authorizePath is missing") }
        if self.accessTokenPath == nil { print("accessTokenPath is missing") }
        if (self.consumerKey == nil || self.consumerSecret == nil || self.requestTokenPath == nil || self.authorizePath == nil || self.accessTokenPath == nil) {
            //return nil
        }
    }
    
    
    public required init(configuration: NSURLSessionConfiguration?) {
        fatalError("init(configuration:) has not been implemented")
    }
    
    
    private func OAuth1Signature(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]) -> String {
        var tokenSecret = "\(self.consumerSecret.urlEncodedStringWithEncoding(NSUTF8StringEncoding))&"
        if let accessToken = (self.account as? OAuth1Account)?.accessToken { tokenSecret +=  accessToken.secret.urlEncodedStringWithEncoding(NSUTF8StringEncoding) }
        else if let requestToken = (self.account as? OAuth1Account)?.requestToken { tokenSecret += requestToken.secret.urlEncodedStringWithEncoding(NSUTF8StringEncoding) }
        
        
        let key = tokenSecret.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        var queryString = ""
        var queryStrings = parameters.urlEncodedQueryStringWithEncoding(NSUTF8StringEncoding).componentsSeparatedByString("&") as [String]
        queryStrings.sortInPlace { $0 < $1 }
        queryString = queryStrings.joinWithSeparator("&")
        
        queryString = queryString.urlEncodedStringWithEncoding(NSUTF8StringEncoding)
        
        let encodedURL = self.baseURL.URLByAppendingPathComponent(URLString.URLString).absoluteString.urlEncodedStringWithEncoding(NSUTF8StringEncoding)
        
        let message = "\(method.rawValue)&\(encodedURL)&\(queryString)".dataUsingEncoding(NSUTF8StringEncoding) //base signature
        
        let sha1 = HMAC.sha1(key: key!, message: message!)
        
        return sha1!.base64EncodedStringWithOptions([])
    }
    
    
    private func OAuth1AuthorizationHeader(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?) -> String {
        var authorizationParameters = self.OAuthParameters
        var result = ""
        var query = ""
        if let params = parameters {
            for (key,value) in params {
                
                //if key.hasPrefix("oauth_") {
                authorizationParameters[key] = value
                //}
                
                
            }
        }
        if let token = (self.account as? OAuth1Account)?.accessToken { authorizationParameters["oauth_token"] = token.key }
        authorizationParameters["oauth_signature"] = self.OAuth1Signature(method, URLString, parameters: authorizationParameters)
        var parameterComponents = authorizationParameters.urlEncodedQueryStringWithEncoding(NSUTF8StringEncoding).componentsSeparatedByString("&") as [String]
        parameterComponents.sortInPlace { $0 < $1 }
        var components = [String]()
        for component in parameterComponents {
            let subComponent = component.componentsSeparatedByString("=") as [String]
            if subComponent.count == 2 { components.append("\(subComponent[0])=\"\(subComponent[1])\"") }
        }
        
        let tmp:NSString  = NSString(format: "%@","OAuth "+components.joinWithSeparator(", "))
        
        return tmp as String
    }
    
    
    public func upload(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?, encoding: ParameterEncoding, data: NSData) -> Request {
        let request_upload = getRequestUpload(method, URLString, parameters: parameters, encoding: encoding,data: data)
        return super.upload(request_upload.0, data: request_upload.1)
    }
    private func getRequestUpload(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?, encoding: ParameterEncoding,data:NSData)-> (URLRequestConvertible,NSData) {
        let mutableRequest = self.getRequest(method, URLString, parameters: parameters, encoding: encoding)
        
        mutableRequest.HTTPMethod = method.rawValue
        let boundaryConstant = "93ufb-2uvn-euvnwjenvrbvi";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add media
        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Disposition: form-data; name=\"fieldname\"; filename=\"file.jpg\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Type: media\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData(data)
        // add parameters
        var mutableParameters = [String: AnyObject]()
        if let params = parameters {
            mutableParameters = params
            for (key,value) in params {
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
            }
        }
        
        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        return (mutableRequest,uploadData)
    }
    private func getRequest(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?, encoding: ParameterEncoding)-> NSMutableURLRequest {
        var mutableParameters = [String: AnyObject]()
        var query:String! = ""
        if let params = parameters {
            mutableParameters = params
            for (key,value) in params {
                if key.hasPrefix("oauth_") { mutableParameters.removeValueForKey(key)}
                else{
                    query = key + "=" + (value as! String)
                }
            }
        }
        var temp_url_string  = URLString.URLString
        if method == Method.GET  && parameters?.count > 0 {
            temp_url_string += "?" + query
        }
        
        
        let mutableRequest = NSMutableURLRequest(URL: self.baseURL.URLByAppendingPathComponent(temp_url_string.URLString))
        mutableRequest.HTTPMethod = method.rawValue
        var tmp:(rq:NSURLRequest, er:NSError?)!
        switch encoding {
        case .URL: tmp = ParameterEncoding.URL.encode(mutableRequest, parameters: parameters)
        case .JSON: tmp = ParameterEncoding.JSON.encode(mutableRequest, parameters: parameters)
        default: break
        }
        // var JSONSerializationError: NSError? = nil
        //add query string
        //mutableRequest.HTTPBody = NSJSONSerialization.dataWithJSONObject(mutableParameters, options: nil, error: &JSONSerializationError)
        
        mutableRequest.HTTPBody = tmp.rq.HTTPBody
        mutableRequest.setValue(self.OAuth1AuthorizationHeader(method, URLString, parameters: parameters), forHTTPHeaderField: "Authorization")
        return mutableRequest
    }
    public override func request(method: Method, _ URLString: URLStringConvertible, parameters: [String : AnyObject]?, encoding: ParameterEncoding) -> Request {
        return Manager.sharedInstance.request(self.getRequest(method, URLString, parameters: parameters, encoding: encoding))
    }
    
}


@available(iOS 8.0, *)
extension OAuth1Manager {
    
    public func requestToken(completionHandler: ((account: AuthAccount?, error: NSError?) -> Void)) {
        var parameters = [String:AnyObject]()
        parameters["oauth_callback"] = self.callbackURL
        parameters["scope"] = self.scope
        
        let request = self.request(.POST, self.requestTokenPath, parameters: parameters, encoding: ParameterEncoding.URL)
        
        request.response { (request, response, result, error) -> Void in
            if error != nil { completionHandler(account: nil, error: error) }
            else if let data = result as? NSData, queryString = NSString(data: data, encoding: NSUTF8StringEncoding) as? String {
                let parameters = queryString.parametersFromQueryString()
                if let key = parameters["oauth_token"], secret = parameters["oauth_token_secret"] {
                    let account = OAuth1Account(manager: self)
                    account.requestToken = OAuth1Token(key: key, secret: secret)
                    self.account = account
                    completionHandler(account: self.account!, error: nil)
                } else {
                    let error = NSError(domain: NapErrorDomain, code: NapError.CannotReadOAuth1DataFromQueryString.rawValue, userInfo: nil)
                    completionHandler(account: nil, error: error)
                }
            }
        }
    }
    
    
    public func verifierWithURLRequest(request:NSURLRequest) -> String? {
        let urlString = "\(request.URL!.scheme)://\(request.URL!.host!)\(request.URL!.path!)"
        if let parameters = request.URL?.query?.parametersFromQueryString() where urlString == self.callbackURL.URLString {
            if let token = parameters["oauth_token"], verifier = parameters["oauth_verifier"] { return verifier }
        }
        return nil
    }
    
    
    public func accessToken(verifier: String, completionHandler:((account: AuthAccount?, error: NSError?)  -> Void)) {
        if let oauth1Account = self.account as? OAuth1Account, requestToken = oauth1Account.requestToken {
            var parameters = [String: AnyObject]()
            parameters["oauth_token"]    = requestToken.key
            parameters["oauth_verifier"] = verifier
            
            let request = self.request(.GET, self.accessTokenPath, parameters: parameters, encoding: ParameterEncoding.URL)
            request.response { (request, response, object, error) -> Void in
                if error != nil { completionHandler(account: nil, error: error) }
                else if let data = object as? NSData, parameterString = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    let parameters = (parameterString as String).parametersFromQueryString()
                    if let token = parameters["oauth_token"], secret = parameters["oauth_token_secret"] {
                        let accessToken = OAuth1Token(key: token, secret: secret)
                        (self.account as? OAuth1Account)?.accessToken = accessToken
                        if let key = self.idKey, id = parameters[key] { self.account?.userID = id }
                        if let key = self.usernameKey, username = parameters[key] { self.account?.username = username }
                        completionHandler(account: self.account, error: nil)
                    }
                } else {
                    let error = NSError(domain: NapErrorDomain, code: NapError.CannotReadOAuth1DataFromQueryString.rawValue, userInfo: nil)
                    completionHandler(account: nil, error: error)
                }
            }
        } else {
            let error : NSError?
          //  completionHandler(account: nil,error: error)
        }
    }
}


public struct OAuth1Token {
    
    public let key    : String
    public let secret : String
    
    public init(key:String, secret:String) {
        self.key = key
        self.secret = secret
    }
}
@available(iOS 8.0, *)
public class OAuth1Account: AuthAccount {
    public var requestToken : OAuth1Token?
    public var accessToken  : OAuth1Token?
}
