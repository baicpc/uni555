//
//  Downloader.swift
//  NhacCuaTui
//
//  Created by Nguyễn Hà on 1/21/16.
//  Copyright © 2016 Nguyễn Hà. All rights reserved.
//

import Foundation
import UIKit

class Downloader : NSObject {
    
    var documentsUrl: NSURL!
    var destinationUrl: NSURL!
    
    class func loadFileSync(url: NSURL, completion:(path:String, error:NSError!) -> Void) {
        let documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first as NSURL!
        let destinationUrl = documentsUrl.URLByAppendingPathComponent(url.lastPathComponent!)
        if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
            print("file already exists [\(destinationUrl.path!)]")
            completion(path: destinationUrl.path!, error:nil)
        } else if let dataFromURL = NSData(contentsOfURL: url){
            if dataFromURL.writeToURL(destinationUrl, atomically: true) {
                print("file saved [\(destinationUrl.path!)]")
                completion(path: destinationUrl.path!, error:nil)
            } else {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(path: destinationUrl.path!, error:error)
            }
        } else {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(path: destinationUrl.path!, error:error)
        }
    }
    
    func loadFileAsync(url: NSURL, completion:(path:String, error:NSError!) -> Void) {
        documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first as NSURL!
        destinationUrl = documentsUrl.URLByAppendingPathComponent(url.lastPathComponent!)
        if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
            print("file already exists [\(destinationUrl.path!)]")
            completion(path: destinationUrl.path!, error:nil)
        } else {
            let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            let task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData? , response: NSURLResponse? , error: NSError? ) -> Void in
                if (error == nil) {
                    if let response = response as? NSHTTPURLResponse {
                        print("response=\(response)")
                        if response.statusCode == 200 {
                            if data!.writeToURL(self.destinationUrl, atomically: true) {
                                print("file saved [\(self.destinationUrl.path!)]")
                                completion(path: self.destinationUrl.path!, error:error)
                            } else {
                               print("error saving file")
                                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                                completion(path: self.destinationUrl.path!, error:error)
                            }
                        }
                    }
                }
                else {
                    print("Failure: \(error!.localizedDescription)");
                    completion(path: self.destinationUrl.path!, error:error)
                }
            })
            task.resume()
        }
    }
    
    class func loadSong(url:String) {
        if let audioUrl = NSURL(string: url) {
            // then lets create your document folder url
            let documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first as NSURL!
            
            // lets create your destination file url
            let destinationUrl = documentsUrl.URLByAppendingPathComponent("\(AppsSettings.tittle_Song_Drive).mp3")
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
                print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                //  just download the data from your url
                if let myAudioDataFromUrl = NSData(contentsOfURL: audioUrl){
                    
                    // after downloading your data you need to save it to your destination url
                    if myAudioDataFromUrl.writeToURL(destinationUrl, atomically: true) {
                        print("file saved")
                    } else {
                        print("error saving file")
                    }
                }
            }
        }
    }
    class func deletefile(nameSong:String) {
        do {
            let fileManager = NSFileManager.defaultManager()
            let documentDirectoryURLs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
            
            if let filePath = documentDirectoryURLs.first?.URLByAppendingPathComponent(nameSong).path {
                try fileManager.removeItemAtPath(filePath)
                print("delete: \(nameSong)")
            }
            
        } catch let error as NSError {
            print("ERROR: \(error)")
        }
    }
    class func deleteSong(nameSong: String){
        let fm = NSFileManager.defaultManager()
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        do {
            let folderPath = documentsDirectory
            let pathSong = "\(folderPath)\(nameSong)"
            let myString = pathSong as NSString
            try fm.removeItemAtPath(myString.substringWithRange(NSRange(location: 7, length: pathSong.length - 7)))
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
}
extension String
{
    func substringWithRange(start: Int, end: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > self.characters.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(end))
        return self.substringWithRange(range)
    }
    
    func substringWithRange(start: Int, location: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > self.characters.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(start + location))
        return self.substringWithRange(range)
    }
}