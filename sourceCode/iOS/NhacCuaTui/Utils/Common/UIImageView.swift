//
//  UIImageView.swift
//  IMMO
//
//  Created by mac01 on 21/12/15.
//  Copyright © 2015 Nguyễn Hà. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if let imageData = data as NSData? {
                    self.image = UIImage(data: imageData)
                }
            }
        }
    }
}
extension UIButton{
    public func setBackgroundFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if(data != nil){
                    self.setBackgroundImage(UIImage(data: data!), forState: UIControlState.Normal)
                }
            }
        }
    }
}