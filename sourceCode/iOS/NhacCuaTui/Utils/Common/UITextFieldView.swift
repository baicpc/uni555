//
//  UITextFieldView.swift
//  ecoinsystem
//
//  Created by Delphinus on 5/12/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class UITextFieldView:UITextField{
    @IBInspectable var color:UIColor = UIColor.blackColor() {
        didSet{
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var holderExtra:String!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    override var placeholder:String!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var underline:Bool! = true{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var btnLeftView:UIButton!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        initComponent()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponent()
        
    }
    func initComponent(){
        self.backgroundColor = UIColor.clearColor()
        self.contentMode = .Redraw
        self.font = UIFont.font56Italic(13)
        self.textColor = color
        initLeftView()
    }
    func initLeftView(){
        btnLeftView = UIButton(frame: CGRectMake(0, 0, 10, 10))
        self.leftViewMode = UITextFieldViewMode.Always
        self.leftView = btnLeftView
    }
    override func drawRect(rect: CGRect) {
        let box = CGRectMake(0,self.bounds.height - 1.0,self.bounds.width,1.0)
        
        let ctx = UIGraphicsGetCurrentContext()
        
        func draw_rect(color: CGColor) {
            CGContextBeginPath(ctx)
            CGContextMoveToPoint(ctx, box.midX, box.midY)
            CGContextSetFillColorWithColor(ctx, color)
            
            CGContextAddRect(ctx, box)
            
            CGContextClosePath(ctx)
            CGContextFillPath(ctx)
            
        }
        
        if self.underline == true { draw_rect(color.CGColor)}
        
    }
    

    override func drawPlaceholderInRect(rect: CGRect) {
        let holderOne:String! = self.placeholder!
        let attributesOne: NSDictionary = [
            NSForegroundColorAttributeName: color,
            NSFontAttributeName: UIFont.font56Italic(13)
        ]
        holderOne.drawInRect(CGRectMake(0, rect.height*0.15, rect.width, rect.height), withAttributes: attributesOne as? [String : AnyObject])
        let sizeOne:CGSize = holderOne.sizeWithAttributes(attributesOne as? [String : AnyObject])
        
        let attributesTwo: NSDictionary = [
            NSForegroundColorAttributeName: color,
            NSFontAttributeName: UIFont.font56Italic(13)
        ]
       
        if holderExtra != nil {
            holderExtra.drawInRect(CGRectMake(sizeOne.width, rect.height * 0.3 , rect.width - sizeOne.width, rect.height), withAttributes: attributesTwo as? [String : AnyObject])
        }
    }
      
}