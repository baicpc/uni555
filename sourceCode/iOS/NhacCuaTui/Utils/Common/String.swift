//
//  String.swift
//  IMMO
//
//  Created by Nguyễn Hà on 13/12/2015.
//  Copyright © Năm 2015 Nguyễn Hà. All rights reserved.
//

import Foundation
extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
    
    func substring(from: Int) -> String {
        return self.substringFromIndex(self.startIndex.advancedBy(from))
    }
    
    var length: Int {
        return self.characters.count
    }
    
    
//    var floatValue: CGFloat {
//        return CGFloat((self as NSString).floatValue)
//    }
    
    var utf8Encoded:NSData! {
        return dataUsingEncoding(NSUTF8StringEncoding)
    }
    func stringByTrimingWhitespace() ->String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    func stringByTrimingQuote() ->String {
        return self.stringValue.stringByReplacingOccurrencesOfString("\"", withString: "")
    }
    
    
    var stringValue:NSString {
        return NSString(format: "%@", self)
    }
    func toDouble() ->Double {
        return self.stringValue.doubleValue
    }
    //    func toPhone() -> String{
    //        var temp = self.stringByReplacingOccurrencesOfString("+", withString: "")
    //        temp = temp.stringByReplacingOccurrencesOfString("-", withString: "")
    //        temp = temp.stringByReplacingOccurrencesOfString("(", withString: "")
    //        temp = temp.stringByReplacingOccurrencesOfString(")", withString: "")
    //        temp = "".join(temp.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet))
    //        return temp.stringByReplacingOccurrencesOfString(" ", withString: "")
    //    }
    var first:String{
        if self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0 {
            let index: String.Index = self.startIndex.advancedBy(1)
            var temp = self.substringToIndex(index)
            return temp.uppercaseString
        }
        return ""
        
    }
//    var base64Decoded : String {
//        
//        let decodedData = NSData(base64EncodedString: self, options:NSDataBase64DecodingOptions())
//        return decodedData!.utf8Decoded
//    }
    func checkRequest() -> Bool {
        return self != ""
    }
    func toDateString() ->String
        
        
    {
        
        let timeinterval : NSTimeInterval = (self as NSString).doubleValue
        
        let dateFromServer = NSDate(timeIntervalSince1970:timeinterval)
        
        let dateFormater : NSDateFormatter = NSDateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        return dateFormater.stringFromDate(dateFromServer)
    }
    
}