//
//  UITextFieldSearch.swift
//  ecoinsystem
//
//  Created by Delphinus on 6/25/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import Foundation
import UIKit

class UITextFieldSearch: UITextFieldView {
    override init(frame: CGRect) {
        super.init(frame: frame);
        initComponent()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initComponent()
    }
    override func initComponent() {
        super.initComponent()
//        let btnClear = self.valueForKey("_clearButton") as! UIButton;
//        btnClear.setImage(UIImage(named: "nav_cancel.png"), forState: UIControlState.Normal);
        self.clearButtonMode = UITextFieldViewMode.WhileEditing;
        self.returnKeyType = UIReturnKeyType.Search
        self.tintColor = UIColor.mainAppColor()
    }
    override func initLeftView() {
        btnLeftView = UIButton(frame: CGRectMake(0, 0, 30, 30));
        btnLeftView.setImage(UIImage(named: "ic_searchclick.png"), forState: UIControlState.Normal);
        btnLeftView.imageEdgeInsets = UIEdgeInsetsMake(8,10, 8, 6);
        
        self.leftView = btnLeftView
        self.leftViewMode = UITextFieldViewMode.Always;
    }
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectOffset(CGRectMake(30, 0, bounds.width - 50, bounds.height), 0, 3)
    }
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.textRectForBounds(bounds)
    }
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.editingRectForBounds(bounds)
    }
    
    override func drawPlaceholderInRect(rect: CGRect) {
        var holderOne:String! = self.placeholder!
        let attributesOne: NSDictionary = [
            NSForegroundColorAttributeName: color,
            NSFontAttributeName: UIFont.font56Italic(13)
        ]
//        if CGFloat(holderOne.characters.count) > 20 * ScreenSize.MUL_WIDTH {
//            holderOne = holderOne.substringWithRange(Range(start: holderOne.startIndex, end: holderOne.startIndex.advancedBy(20))) + "..."
//        }
        holderOne.drawInRect(CGRectMake(0, rect.height*0.25, rect.width, rect.height), withAttributes: attributesOne as? [String : AnyObject])
        let sizeOne:CGSize = holderOne.sizeWithAttributes(attributesOne as? [String : AnyObject])
        
        let attributesTwo: NSDictionary = [
            NSForegroundColorAttributeName: color,
            NSFontAttributeName: UIFont.font56Italic(13)
        ]
        
        if holderExtra != nil {
            holderExtra.drawInRect(CGRectMake(sizeOne.width, rect.height * 0.3 , rect.width - sizeOne.width, rect.height), withAttributes: attributesTwo as? [String : AnyObject] )
        }
    }

}
