//
//  BaseUICollectionViewCellImage.swift
//  NhacCuaTui
//
//  Created by Nguyễn Hà on 1/6/16.
//  Copyright © 2016 Nguyễn Hà. All rights reserved.
//

import UIKit

public class BaseUICollectionViewCellImage: UICollectionViewCell {
    class var identifier: String { return String.className(self) }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //    override init(style: UICollectionViewCell, reuseIdentifier: String?) {
    //        super.init(style: style, reuseIdentifier: reuseIdentifier)
    //        setup()
    //    }
    
    public override func awakeFromNib() {
    }
    
    public func setup() {
        
    }
    
    public class func height() -> CGFloat {
        return 110 * ScreenSize.MUL_HEIGHT
    }
    
    public class func width() -> CGFloat{
        return 80 * ScreenSize.MUL_WIDTH
    }
    public func setData(data: Any?) {
        self.backgroundColor = UIColor .clearColor()
            }
}
