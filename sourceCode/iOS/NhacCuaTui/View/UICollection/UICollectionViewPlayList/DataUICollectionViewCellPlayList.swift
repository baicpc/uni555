//
//  DataUICollectionViewCellImage.swift
//  NhacCuaTui
//
//  Created by Nguyễn Hà on 1/6/16.
//  Copyright © 2016 Nguyễn Hà. All rights reserved.
//

import Foundation
import UIKit
struct DataUICollectionViewCellPlayListData{
    init(image_Song: String , text_Count_Listen: String , text_Title_PlayList: String , text_Author: String) {
        self.imageSong = image_Song
        self.textAuthor = text_Author
        self.textCountListen = text_Count_Listen
        self.textTitlePlayList = text_Title_PlayList
    }
    
    var imageSong: String
    var textCountListen: String
    var textTitlePlayList: String
    var textAuthor: String
}

class DataUICollectionViewCellPlayList: BaseUICollectionViewCellPlayList {
    
    @IBOutlet weak var image_Song: UIImageView!
    //@IBOutlet weak var image_Song: UIButton!
    @IBOutlet weak var text_Count_listen: UILabel!
    @IBOutlet weak var text_titile_playList: UILabel!
    @IBOutlet weak var text_author: UILabel!

    
    override func awakeFromNib() {
        self.backgroundColor = UIColor .clearColor()
        self.text_titile_playList.font = UIFont.font65Medium(10)
        self.text_author.font = UIFont.font66MediumItalic(8)
    }
    
    override class func height() -> CGFloat {
        return 110 * ScreenSize.MUL_HEIGHT
    }
    
    override func setData(data: Any?) {
        self.image_Song.image = UIImage(named: "")
        if let data = data as? DataUICollectionViewCellPlayListData {
            if data.imageSong.isEmpty == false && data.imageSong != "" && data.imageSong != "http://222.255.46.7:8080/music/public/media/playlists/tb/" && data.imageSong != "http://222.255.46.7:8080/music/public/media/videos/tb/" && data.imageSong != "http://222.255.46.7:8080/music/public/media/artists/tb/" {
                self.image_Song.imageFromUrl(data.imageSong)
            }else {
                self.image_Song.image = UIImage(named: "down")
            }
            
            if data.textAuthor.isEmpty == false {
                self.text_author.text = data.textAuthor
            }else {
                self.text_author.text = ""
            }
            
            if data.textCountListen.isEmpty == false {
                self.text_Count_listen.text = data.textCountListen
            }else{
                self.text_Count_listen.text = ""
            }
            
            if data.textTitlePlayList.isEmpty == false {
                self.text_titile_playList.text = data.textTitlePlayList
            }else{
                self.text_titile_playList.text = ""
            }   
        }
    }
    
}