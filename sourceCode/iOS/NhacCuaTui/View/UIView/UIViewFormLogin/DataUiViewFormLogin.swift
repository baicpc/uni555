//
//  DataUiViewFormLogin.swift
//  NhacCuaTui
//
//  Created by Nguyễn Hà on 1/18/16.
//  Copyright © 2016 Nguyễn Hà. All rights reserved.
//
import FBSDKLoginKit
import UIKit

class DataUiViewFormLogin: UIView, FBSDKLoginButtonDelegate, GPPSignInDelegate, UITextFieldDelegate {

    //khai bao UIview
    var titleForm: UILabel!
    var textUserName: UITextField!
    var textPassWord: UITextField!
    var buttonOk: UIButton!
    var buttonCancel: UIButton!
    var textHoac: UILabel!
    var btRegister: UIButton!
    var buttonFacebook: FBSDKLoginButton!
    var buttonGoogle: UIButton!
    let preferences = NSUserDefaults.standardUserDefaults()
    var userIDFacebook: String!
    var checklogin: String! = ""
    
    var signInGoogle: GPPSignIn!
    var moveTextField:CGFloat! = 0
    
    //khai bao bien
    var idChoose: Int!
    var loginUser: LoginUser!
    
    var txtRegisEmail: UITextField!
    var txtRegisPass: UITextField!
    var btAcceptRegis: UIButton!
    override func awakeFromNib() {
        
        createInterface()
        buttonFacebook.delegate = self
        buttonFacebook.readPermissions = ["public_profile", "email", "user_friends"]
        signInGoogle = GPPSignIn.sharedInstance()
        signInGoogle.shouldFetchGooglePlusUser = true
        signInGoogle.shouldFetchGoogleUserEmail = true  // Uncomment to get the user's email
        signInGoogle.shouldFetchGoogleUserID = true
        signInGoogle.clientID = "469372234063-m4nbat60o8icqmu68f3a3k1o3cbfddmo.apps.googleusercontent.com"
        signInGoogle.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
                self.addGestureRecognizer(tap)
        AppsSettings.dimBackgroundColor.addGestureRecognizer(tap)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }

    
    /***************************************************
    ham xet su kien khi click vào các button
    ****************************************************/
    func actionEven(sender:UIButton){
        idChoose = sender.tag
        if idChoose == 1 { // xet su kien button đăng nhập
            AppsSettings.dimBackgroundColor.hidden = true
            AppsSettings.formLogin.hidden = true
            
        }else if idChoose == 2 { // xet sự kiện button thoát
            hideLoginUI()
        }
    }
    
    /***************************************************
    ham parce api login
    ****************************************************/
    func parceApiLogin(email: String , firstname: String , lastname: String){
        loginUser = LoginUser()
        loginUser.emailUser = email
        loginUser.firstnameUser = firstname
        loginUser.lastname = lastname
        AccountService(viewController: AppsSettings.root, isShowLoading: true).getIdUser("api/registerAndLogin", loginUser: loginUser, success: { (response) -> Void in
            let a = response
            print(String(a.idUser))
            
            let useridkey = "useridkey"
            let usernamekey = "usernamekey"
            
            let username = firstname
            AppsSettings.nameUserAvatar.text = username
            let userid = String(a.idUser)
            self.preferences.setValue(userid, forKey: useridkey)
            self.preferences.setValue(username, forKey: usernamekey)
            
            AppsSettings.dimBackgroundColor.hidden = true
            AppsSettings.formLogin.hidden = true
            //  Save to disk
            let didSave = self.preferences.synchronize()
            if !didSave {
                print("Error to check login")
            }
            }) { (error) -> Void in
                print("error")
        }
    }
    
    // dang nhap thuong 0
    func parceApiLoginNomarl(){
        buttonOk.enabled = false
        var register: Register = Register()
        register.username = textUserName.text
        register.password = textPassWord.text
        AccountService(viewController: AppsSettings.root, isShowLoading: true).register("api/login", register: register, success: { (response) -> Void in
            let a = response
            print(String(a.username))
            let useridkey = "useridkey"
            let usernamekey = "usernamekey"
            let checkloginkey = "checklogin"
            let username = a.username
   
            let userid = String(a.id)
            self.preferences.setValue(userid, forKey: useridkey)
            self.preferences.setValue(username, forKey: usernamekey)
            self.preferences.setValue(a.email, forKey: "useremailkey")
            self.preferences.setInteger(1, forKey: checkloginkey)
            let didSave = self.preferences.synchronize()
            AppsSettings.dimBackgroundColor.hidden = true
            AppsSettings.formLogin.hidden = true
            AppsSettings.checklog = "1"
            AppsSettings.shit = true
            //thaydoi1.3
            AppsSettings.statusLoginNomal = true
            AppsSettings.saveNameLoginNomal = username
            self.textUserName.text = ""
            self.textPassWord.text = ""
            if !didSave {
                print("Error to check login")
            }
            
        }) { (error) -> Void in
            print("error")
            UIToast.makeText("ពត៍មានចូលគណនីមិនត្រឹមត្រូវ").show()
            self.buttonOk.enabled = true
        }
        
    }

    
    /***************************************************
    Khoi tao View
    ****************************************************/
    func createInterface(){
        titleForm = UILabel(frame: CGRect(x: ScreenSize.MUL_WIDTH * 8, y:ScreenSize.MUL_HEIGHT * 10, width: ScreenSize.MUL_WIDTH * 234, height: ScreenSize.MUL_HEIGHT * 20))
        titleForm.textAlignment = .Center
        titleForm.font = UIFont.font65Medium(15)
        titleForm.text = "ចូលគណនី"
        
        textUserName = UITextField(frame: CGRect(x: ScreenSize.MUL_WIDTH * 29, y:ScreenSize.MUL_HEIGHT * 35, width: ScreenSize.MUL_WIDTH * 193, height: ScreenSize.MUL_HEIGHT * 23))
        textUserName.layer.borderColor = UIColor(hex: "000000").CGColor
        textUserName.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        textUserName.layer.cornerRadius = 3.0 * ScreenSize.MUL_WIDTH
        textUserName.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        textUserName.placeholder = "  ឈ្មោះចូលគណនី"
        textUserName.delegate = self
        
        textPassWord = UITextField(frame: CGRect(x: ScreenSize.MUL_WIDTH * 29, y:ScreenSize.MUL_HEIGHT * 65, width: ScreenSize.MUL_WIDTH * 193, height: ScreenSize.MUL_HEIGHT * 23))
        textPassWord.layer.borderColor = UIColor(hex: "000000").CGColor
        textPassWord.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        textPassWord.layer.cornerRadius = 3.0 * ScreenSize.MUL_WIDTH
        textPassWord.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        textPassWord.placeholder = "  លេខសំងាត់"
        textPassWord.secureTextEntry = true
        textPassWord.delegate = self
        
        buttonOk = UIButton(frame: CGRect(x: ScreenSize.MUL_WIDTH * 35, y:ScreenSize.MUL_HEIGHT * 100, width: ScreenSize.MUL_WIDTH * 80, height: ScreenSize.MUL_HEIGHT * 23))
        buttonOk.layer.borderColor = UIColor(hex: "E21D2B").CGColor
        buttonOk.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        buttonOk.layer.cornerRadius = 5.0 * ScreenSize.MUL_WIDTH
        buttonOk.setTitle("ចូលគណនី", forState: UIControlState.Normal)
        buttonOk.backgroundColor = UIColor(hex: "E21D2B")
        buttonOk.titleLabel?.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        buttonOk.addTarget(self, action: "parceApiLoginNomarl", forControlEvents: UIControlEvents.TouchUpInside)
        buttonOk.tag = 1
        
        buttonCancel = UIButton(frame: CGRect(x: ScreenSize.MUL_WIDTH * 135, y:ScreenSize.MUL_HEIGHT * 100, width: ScreenSize.MUL_WIDTH * 80, height: ScreenSize.MUL_HEIGHT * 23))
        buttonCancel.layer.borderColor = UIColor(hex: "E21D2B").CGColor
        buttonCancel.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        buttonCancel.layer.cornerRadius = 5.0 * ScreenSize.MUL_WIDTH
        buttonCancel.setTitle("ចាកចេញ", forState: UIControlState.Normal)
        buttonCancel.backgroundColor = UIColor(hex: "E21D2B")
        buttonCancel.titleLabel?.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        buttonCancel.addTarget(self, action: "actionEven:", forControlEvents: UIControlEvents.TouchUpInside)
        buttonCancel.tag = 2
        
        textHoac = UILabel(frame: CGRect(x: ScreenSize.MUL_WIDTH * 20, y:ScreenSize.MUL_HEIGHT * 135, width: ScreenSize.MUL_WIDTH * 100, height: ScreenSize.MUL_HEIGHT * 20))
        textHoac.textAlignment = .Center
        textHoac.font = UIFont.font65Medium(12)
        textHoac.text = "ផ្សេងៗ"
        buttonFacebook = FBSDKLoginButton()
        buttonFacebook.frame = CGRect(x: ScreenSize.MUL_WIDTH * 100, y:ScreenSize.MUL_HEIGHT * 130, width: ScreenSize.MUL_WIDTH * 25, height: ScreenSize.MUL_HEIGHT * 25)
        buttonFacebook.setBackgroundImage(UIImage(named:"btn_facebook.png"), forState: UIControlState.Normal)
        buttonFacebook.tag = 3
        btRegister = UIButton(frame: CGRect(x: ScreenSize.MUL_WIDTH * 135, y:ScreenSize.MUL_HEIGHT * 130, width: ScreenSize.MUL_WIDTH * 80, height: ScreenSize.MUL_HEIGHT * 20))
        btRegister.setTitle("ចុះឈ្មោះ", forState: UIControlState.Normal)
        btRegister.titleLabel?.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT)
        btRegister.backgroundColor = UIColor(hex: "E21D2B")
        btRegister.layer.borderColor = UIColor(hex: "E21D2B").CGColor
        btRegister.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        btRegister.layer.cornerRadius = 5.0 * ScreenSize.MUL_WIDTH
        btRegister.addTarget(self, action: "formRegister", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        self.addSubview(titleForm)
        self.addSubview(textUserName)
        self.addSubview(textPassWord)
        self.addSubview(buttonOk)
        self.addSubview(buttonCancel)
        self.addSubview(textHoac)
        self.addSubview(buttonFacebook)
        self.addSubview(btRegister)
    }
    
    
    func formRegister(){
        textUserName.removeFromSuperview()
        textPassWord.removeFromSuperview()
        buttonOk.removeFromSuperview()
        buttonCancel.removeFromSuperview()
        textHoac.removeFromSuperview()
        buttonFacebook.removeFromSuperview()
        btRegister.removeFromSuperview()
        titleForm.removeFromSuperview()
        
        titleForm = UILabel(frame: CGRect(x: ScreenSize.MUL_WIDTH * 8, y:ScreenSize.MUL_HEIGHT * 10, width: ScreenSize.MUL_WIDTH * 234, height: ScreenSize.MUL_HEIGHT * 20))
        titleForm.textAlignment = .Center
        titleForm.font = UIFont.font65Medium(15)
        titleForm.text = "ចុះឈ្មោះ"
        textUserName = UITextField(frame: CGRect(x: ScreenSize.MUL_WIDTH * 29, y:ScreenSize.MUL_HEIGHT * 35, width: ScreenSize.MUL_WIDTH * 193, height: ScreenSize.MUL_HEIGHT * 23))
        textUserName.layer.borderColor = UIColor(hex: "000000").CGColor
        textUserName.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        textUserName.layer.cornerRadius = 3.0 * ScreenSize.MUL_WIDTH
        textUserName.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        textUserName.placeholder = "  ឈ្មោះចូលគណនី"
        textUserName.delegate = self
        
        txtRegisEmail = UITextField(frame: CGRect(x: ScreenSize.MUL_WIDTH * 29, y:ScreenSize.MUL_HEIGHT * 65, width: ScreenSize.MUL_WIDTH * 193, height: ScreenSize.MUL_HEIGHT * 23))
        txtRegisEmail.layer.borderColor = UIColor(hex: "000000").CGColor
        txtRegisEmail.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        txtRegisEmail.layer.cornerRadius = 3.0 * ScreenSize.MUL_WIDTH
        txtRegisEmail.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        txtRegisEmail.placeholder = "  អ៊ីម៉ែល"
//        txtRegisEmail.secureTextEntry = true
        txtRegisEmail.delegate = self
        
        
        txtRegisPass = UITextField(frame: CGRect(x: ScreenSize.MUL_WIDTH * 29, y:ScreenSize.MUL_HEIGHT * 95, width: ScreenSize.MUL_WIDTH * 193, height: ScreenSize.MUL_HEIGHT * 23))
        txtRegisPass.layer.borderColor = UIColor(hex: "000000").CGColor
        txtRegisPass.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        txtRegisPass.layer.cornerRadius = 3.0 * ScreenSize.MUL_WIDTH
        txtRegisPass.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        txtRegisPass.placeholder = "  លេខសំងាត់"
        txtRegisPass.secureTextEntry = true
        txtRegisPass.delegate = self
        
        btAcceptRegis = UIButton(frame: CGRect(x: ScreenSize.MUL_WIDTH * 35, y:ScreenSize.MUL_HEIGHT * 130, width: ScreenSize.MUL_WIDTH * 80, height: ScreenSize.MUL_HEIGHT * 23))
        btAcceptRegis.layer.borderColor = UIColor(hex: "E21D2B").CGColor
        btAcceptRegis.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        btAcceptRegis.layer.cornerRadius = 5.0 * ScreenSize.MUL_WIDTH
        btAcceptRegis.setTitle("ចុះឈ្មោះ", forState: UIControlState.Normal)
        btAcceptRegis.backgroundColor = UIColor(hex: "E21D2B")
        btAcceptRegis.titleLabel?.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        btAcceptRegis.addTarget(self, action: "parseAPIRegister", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        buttonCancel = UIButton(frame: CGRect(x: ScreenSize.MUL_WIDTH * 135, y:ScreenSize.MUL_HEIGHT * 130, width: ScreenSize.MUL_WIDTH * 80, height: ScreenSize.MUL_HEIGHT * 23))
        buttonCancel.layer.borderColor = UIColor(hex: "E21D2B").CGColor
        buttonCancel.layer.borderWidth = 0.5 * ScreenSize.MUL_WIDTH
        buttonCancel.layer.cornerRadius = 5.0 * ScreenSize.MUL_WIDTH
        buttonCancel.setTitle("ចាកចេញ", forState: UIControlState.Normal)
        buttonCancel.backgroundColor = UIColor(hex: "E21D2B")
        buttonCancel.titleLabel?.font = UIFont(name: "Arial", size: 11 * ScreenSize.MUL_HEIGHT )
        buttonCancel.addTarget(self, action: "hideFormRegister", forControlEvents: UIControlEvents.TouchUpInside)
        buttonCancel.tag = 2
        
        self.addSubview(titleForm)
        self.addSubview(textUserName)
        self.addSubview(txtRegisEmail)
        self.addSubview(txtRegisPass)
        self.addSubview(btAcceptRegis)
        self.addSubview(buttonCancel)
        //        btAcceptRegis.tag = 1
    }
    
    //validate email
    
    func isValidEmail(testStr:String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        var result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    
    
    
    func hideFormRegister(){
        textUserName.removeFromSuperview()
        txtRegisEmail.removeFromSuperview()
        txtRegisPass.removeFromSuperview()
        btAcceptRegis.removeFromSuperview()
        buttonCancel.removeFromSuperview()
        titleForm.removeFromSuperview()
        createInterface()
    }
    func parseAPIRegister(){
        
        var entity: Register! = Register()
        if txtRegisPass.text != "" && txtRegisEmail.text != "" && textUserName.text != ""{
            if isValidEmail(txtRegisEmail.text!){
                entity.username = textUserName.text
                entity.email = txtRegisEmail.text
                entity.password = txtRegisPass.text
                AccountService(viewController: AppsSettings.root, isShowLoading: true).register("api/register", register: entity, success: { (response) -> Void in
                    let a = response
                    print("THis register:" + String(response))
                    UIToast.makeText("ចុះឈ្មោះជោគជ័យ!").show()
                    self.hideFormRegister()
                }) { (error) -> Void in
                    print("error")
                    UIToast.makeText("ចុះឈ្មោះមិនជោគជ័យ!").show()
                }

            }else{
                UIToast.makeText("លេកអ្នកបញ្ចូលអ៊ីម៉ែលខុស!").show()
            }
        }else {
            UIToast.makeText("លោកអ្នកត្រូវតែបញ្ចូលពត៍មានអោយពេញលេញ!").show()
        }

    }
    
    func hideLoginUI(){
        AppsSettings.formLogin.hidden = true
        AppsSettings.dimBackgroundColor.hidden = true
    }
    
//Login 1
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!){
        if(error != nil){
            print(error.localizedDescription)
            return
        }
        if let userToken = result.token{
            let token:FBSDKAccessToken = result.token
            print("Token = \(FBSDKAccessToken.currentAccessToken().tokenString)")
            print("Token = \(FBSDKAccessToken.currentAccessToken().userID)")
            //AppSetting.UseridFacebook = FBSDKAccessToken.currentAccessToken().userID
            
            let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: userToken.tokenString, version: nil, HTTPMethod: "GET")
            
            req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
                if(error == nil)
                {
                    let userEmail = result.valueForKey("email") as! String!
                    //print("email hoang ha: " + userEmail)
                    let name = result.valueForKey("name") as! String!
                    //print("name hoang ha: " + name)
                    self.preferences.setValue(name, forKey: "usernamekey")
                    self.preferences.setValue(userEmail, forKey: "useremailkey")
                    if userEmail != nil
                    {
                    self.parceApiLogin(userEmail, firstname: name, lastname: "test")
                    }
                    else
                    {
                        self.parceApiLogin("", firstname: name, lastname: "test")
                    }
                }
                else
                {
                    print("error \(error)")
                }
            })
            
            let checkloginkey = "checklogin"
            let idfacebooks = "idfacebook"
            let idfacebook = FBSDKAccessToken.currentAccessToken().userID
            print("this id facebook: " + idfacebook)
            let loginkey = 1
            preferences.setInteger(loginkey, forKey: checkloginkey)
            preferences.setValue(idfacebook, forKey: idfacebooks)
            //  Save to disk
            let didSave = preferences.synchronize()
            if !didSave {
                print("Error to check login")
            }
            AppsSettings.checklog = "1"
            AppsSettings.shit = true
            //thaydoi1.2
            AppsSettings.formLogin.removeFromSuperview()
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!){
    }
    
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        if (GPPSignIn.sharedInstance().userID != nil) {
            let user = GPPSignIn.sharedInstance().googlePlusUser
            print("user name: " + user.name.JSONString() + "\nemail: ")
            if (user.emails != nil){
                print(user.emails.first?.JSONString() ?? "no email")
            } else {
                print("no email")
            }
        } else {
            print("User ID is nil")
        }
    }
    func signGoogle(sender: AnyObject?){
        signInGoogle.authenticate()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == textUserName){ moveTextField = 10}
        else { moveTextField = 10}
        
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.endEditing(true)
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.endEditing(true)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.endEditing(true)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.frame.origin.y = -moveTextField
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.frame.origin.y = +25
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }

}
