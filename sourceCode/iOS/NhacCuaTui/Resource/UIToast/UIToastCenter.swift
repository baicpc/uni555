//
//  UIToastCenter.swift
//  ecoinsystem
//
//  Created by Delphinus on 6/1/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//
import UIKit

@objc public class UIToastCenter: NSObject {

    private var _queue: NSOperationQueue!

    private struct Singletone {
        static let defaultCenter = UIToastCenter()
    }
    
    public class func defaultCenter() -> UIToastCenter {
        return Singletone.defaultCenter
    }
    
    override init() {
        super.init()
        self._queue = NSOperationQueue()
        self._queue.maxConcurrentOperationCount = 1
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "deviceOrientationDidChange:",
            name: UIDeviceOrientationDidChangeNotification,
            object: nil
        )
    }
    
    public func addToast(toast: UIToast) {
        self._queue.addOperation(toast)
    }
    
    func deviceOrientationDidChange(sender: AnyObject?) {
        if self._queue.operations.count > 0 {
            let lastToast: UIToast = _queue.operations[0] as! UIToast
            lastToast.view.updateView()
        }
    }
}
