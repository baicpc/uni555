//
//  UIToastDelay.swift
//  ecoinsystem
//
//  Created by Delphinus on 6/1/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//
import UIKit

public struct UIToastDelay {
    public static let ShortDelay: NSTimeInterval = 2.0
    public static let LongDelay: NSTimeInterval = 3.5
}

@objc public class UIToast: NSOperation {

    public var view: UIToastView = UIToastView()
    
    public var text: String? {
        get {
            return self.view.textLabel.text
        }
        set {
            self.view.textLabel.text = newValue
        }
    }

    public var delay: NSTimeInterval = 0
    public var duration: NSTimeInterval = UIToastDelay.ShortDelay

    private var _executing = false
    override public var executing: Bool {
        get {
            return self._executing
        }
        set {
            self.willChangeValueForKey("isExecuting")
            self._executing = newValue
            self.didChangeValueForKey("isExecuting")
        }
    }

    private var _finished = false
    override public var finished: Bool {
        get {
            return self._finished
        }
        set {
            self.willChangeValueForKey("isFinished")
            self._finished = newValue
            self.didChangeValueForKey("isFinished")
        }
    }
    
    
    public class func makeText(text: String) -> UIToast {
        
        
        return UIToast.makeText(text, delay: 0, duration: UIToastDelay.ShortDelay)
    }
    
    public class func makeText(text: String, duration: NSTimeInterval) -> UIToast {
        return UIToast.makeText(text, delay: 0, duration: duration)
    }
    
    public class func makeText(text: String, delay: NSTimeInterval, duration: NSTimeInterval) -> UIToast {
        let toast = UIToast()
        toast.view.textLabel.font = UIFont(name: AppsSettings.Static.AvenirBook as String, size: AppsSettings.Static.MulHeight*12)
        toast.text = text
        toast.delay = delay
        toast.duration = duration
        return toast
    }
    
    public func show() {
        UIToastCenter.defaultCenter().addToast(self)
    }
    
    override public func start() {
        if !NSThread.isMainThread() {
            dispatch_async(dispatch_get_main_queue(), {
                self.start()
            })
        } else {
            super.start()
        }
    }

    override public func main() {
        self.executing = true

        dispatch_async(dispatch_get_main_queue(), {
            self.view.updateView()
            self.view.alpha = 0
            UIApplication.sharedApplication().windows.first?.addSubview(self.view)
            UIView.animateWithDuration(
                0.5,
                delay: self.delay,
                options: .BeginFromCurrentState,
                animations: {
                    self.view.alpha = 1
                },
                completion: { completed in
                    UIView.animateWithDuration(
                        self.duration,
                        animations: {
                            self.view.alpha = 1.0001
                        },
                        completion: { completed in
                            self.finish()
                            UIView.animateWithDuration(0.5, animations: {
                                self.view.alpha = 0
                            })
                        }
                    )
                }
            )
        })
    }
    
    public func finish() {
        self.executing = false
        self.finished = true
    }
}