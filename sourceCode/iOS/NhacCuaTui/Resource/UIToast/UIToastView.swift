//
//  UIToastView.swift
//  ecoinsystem
//
//  Created by Delphinus on 6/1/15.
//  Copyright (c) 2015 Delphinus. All rights reserved.
//

import UIKit

public let UIToastViewBackgroundColorAttributeName = "UIToastViewBackgroundColorAttributeName"
public let UIToastViewCornerRadiusAttributeName = "UIToastViewCornerRadiusAttributeName"
public let UIToastViewTextInsetsAttributeName = "UIToastViewTextInsetsAttributeName"
public let UIToastViewTextColorAttributeName = "UIToastViewTextColorAttributeName"
public let UIToastViewFontAttributeName = "UIToastViewFontAttributeName"
public let UIToastViewPortraitOffsetYAttributeName = "UIToastViewPortraitOffsetYAttributeName"
public let UIToastViewLandscapeOffsetYAttributeName = "UIToastViewLandscapeOffsetYAttributeName"

@objc public class UIToastView: UIView {
    
    var backgroundView: UIView!
    var textLabel: UILabel!
    var textInsets: UIEdgeInsets!
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))

        let userInterfaceIdiom = UIDevice.currentDevice().userInterfaceIdiom

        self.userInteractionEnabled = false

        self.backgroundView = UIView()
        self.backgroundView.frame = self.bounds
        self.backgroundView.backgroundColor = self.dynamicType.defaultValueForAttributeName(
            UIToastViewBackgroundColorAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as? UIColor
        self.backgroundView.layer.cornerRadius = self.dynamicType.defaultValueForAttributeName(
            UIToastViewCornerRadiusAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as! CGFloat
        self.backgroundView.clipsToBounds = true
        self.addSubview(self.backgroundView)

        self.textLabel = UILabel()
        self.textLabel.frame = self.bounds
        self.textLabel.textColor = self.dynamicType.defaultValueForAttributeName(
            UIToastViewTextColorAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as? UIColor
        self.textLabel.backgroundColor = UIColor.clearColor()
        self.textLabel.font = self.dynamicType.defaultValueForAttributeName(
            UIToastViewFontAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as! UIFont
        self.textLabel.numberOfLines = 0
        self.textLabel.textAlignment = .Center;
        self.addSubview(self.textLabel)

        self.textInsets = (self.dynamicType.defaultValueForAttributeName(
            UIToastViewTextInsetsAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as! NSValue).UIEdgeInsetsValue()
    }
    
    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
    }
    
    func updateView() {
        let deviceWidth = CGRectGetWidth(UIScreen.mainScreen().bounds)
        let constraintSize = CGSize(width: deviceWidth * (280.0 / 320.0), height: CGFloat.max)
        let textLabelSize = self.textLabel.sizeThatFits(constraintSize)
        self.textLabel.frame = CGRect(
            x: self.textInsets.left,
            y: self.textInsets.top,
            width: textLabelSize.width,
            height: textLabelSize.height
        )
        self.backgroundView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.textLabel.frame.size.width + self.textInsets.left + self.textInsets.right,
            height: self.textLabel.frame.size.height + self.textInsets.top + self.textInsets.bottom
        )

        var x: CGFloat
        var y: CGFloat
        var width:CGFloat
        var height:CGFloat

        let screenSize = UIScreen.mainScreen().bounds.size
        let backgroundViewSize = self.backgroundView.frame.size

        let orientation = UIApplication.sharedApplication().statusBarOrientation
        let systemVersion = (UIDevice.currentDevice().systemVersion as NSString).floatValue

        let userInterfaceIdiom = UIDevice.currentDevice().userInterfaceIdiom
        let portraitOffsetY = self.dynamicType.defaultValueForAttributeName(
            UIToastViewPortraitOffsetYAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as! CGFloat
        let landscapeOffsetY = self.dynamicType.defaultValueForAttributeName(
            UIToastViewLandscapeOffsetYAttributeName,
            forUserInterfaceIdiom: userInterfaceIdiom
        ) as! CGFloat

        if UIInterfaceOrientationIsLandscape(orientation) && systemVersion < 8.0 {
            width = screenSize.height
            height = screenSize.width
            y = landscapeOffsetY
        } else {
            width = screenSize.width
            height = screenSize.height
            if UIInterfaceOrientationIsLandscape(orientation) {
                y = landscapeOffsetY
            } else {
                y = portraitOffsetY
            }
        }

        x = (width - backgroundViewSize.width) * 0.5
        y = height - (backgroundViewSize.height + y)
        self.frame = CGRect(x: x, y: y, width: backgroundViewSize.width, height: backgroundViewSize.height);
    }
    
    override public func hitTest(point: CGPoint, withEvent event: UIEvent!) -> UIView? {
        if let superview = self.superview {
            let pointInWindow = self.convertPoint(point, toView: self.superview)
            let contains = CGRectContainsPoint(self.frame, pointInWindow)
            if contains && self.userInteractionEnabled {
                return self
            }
        }
        return nil
    }

}

public extension UIToastView {
    private struct Singleton {
        static var defaultValues: [String: [UIUserInterfaceIdiom: AnyObject]] = [
            // backgroundView.color
            UIToastViewBackgroundColorAttributeName: [
                .Unspecified: UIColor(white: 0, alpha: 0.7)
            ],

            // backgroundView.layer.cornerRadius
            UIToastViewCornerRadiusAttributeName: [
                .Unspecified: 5
            ],

            UIToastViewTextInsetsAttributeName: [
                .Unspecified: NSValue(UIEdgeInsets: UIEdgeInsets(top: 6, left: 10, bottom: 6, right: 10))
            ],

            // textLabel.textColor
            UIToastViewTextColorAttributeName: [
                .Unspecified: UIColor.whiteColor()
            ],

            // textLabel.font
            UIToastViewFontAttributeName: [
                .Unspecified: UIFont.systemFontOfSize(12),
                .Phone: UIFont.systemFontOfSize(12),
                .Pad: UIFont.systemFontOfSize(16),
            ],

            UIToastViewPortraitOffsetYAttributeName: [
                .Unspecified: 60,
                .Phone: 60,
                .Pad: 60,
            ],
            UIToastViewLandscapeOffsetYAttributeName: [
                .Unspecified: 20,
                .Phone: 20,
                .Pad: 40,
            ],
        ]
    }

    class func defaultValueForAttributeName(attributeName: String,
                                            forUserInterfaceIdiom userInterfaceIdiom: UIUserInterfaceIdiom)
                                            -> AnyObject {
        let valueForAttributeName = Singleton.defaultValues[attributeName]!
        if let value: AnyObject = valueForAttributeName[userInterfaceIdiom] {
            return value
        }
        return valueForAttributeName[.Unspecified]!
    }

    class func setDefaultValue(value: AnyObject,
                               forAttributeName attributeName: String,
                               userInterfaceIdiom: UIUserInterfaceIdiom) {
        var values = Singleton.defaultValues[attributeName]!
        values[userInterfaceIdiom] = value
        Singleton.defaultValues[attributeName] = values
    }
}
