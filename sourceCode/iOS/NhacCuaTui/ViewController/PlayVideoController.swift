//
//  PlayVideoController.swift
//  NhacCuaTui
//
//  Created by Nguyễn Hà on 1/15/16.
//  Copyright © 2016 Nguyễn Hà. All rights reserved.
//

import UIKit
import MediaPlayer
class PlayVideoController: BaseViewController {
    
    var thamsotruyen: NSUserDefaults!
    var linkUrlVideo: String!
    var moviePlayer:MPMoviePlayerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.hidden = false // for navigation bar hide
        UIApplication.sharedApplication().statusBarHidden=false; // for status bar hide
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Bordered, target: self, action: "back:")
        self.navigationItem.leftBarButtonItem = newBackButton;
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object: nil)
        thamsotruyen = NSUserDefaults()
        linkUrlVideo = thamsotruyen.objectForKey("linkUrlVideo") as! String
        let prString = linkUrlVideo.stringByReplacingOccurrencesOfString(" ", withString: "%20", options:NSStringCompareOptions.LiteralSearch, range: nil)
        print("video:  " + prString + "----------------")
        playVideo(prString)
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        print("check back video: " + String(AppsSettings.isCheckedButtonPlay))
        if AppsSettings.isCheckedButtonPlay == false{
            AppsSettings.isCheckedButtonPlay = true
            AppsSettings.playAudio?.play()
            
        }else {
            AppsSettings.isCheckedButtonPlay = false
            AppsSettings.playAudio?.pause()
            
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidAppear(animated: Bool) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)  ///< Don't forget this BTW!
        if let player = self.moviePlayer {
            player.play()
        }
    }
//    func doneButtonClick(sender:NSNotification?){
//        AppsSettings.playAudio?.play()
//    }
    func playVideo(URLVideo: String!){
        let url:NSURL = NSURL(string: URLVideo)!
        moviePlayer = MPMoviePlayerController(contentURL: url)
        moviePlayer.view.frame = CGRect(x: 0, y: 0, width: ScreenSize.MUL_WIDTH * 568, height: ScreenSize.MUL_HEIGHT * 320)
        self.view.addSubview(moviePlayer.view)
        //moviePlayer.fullscreen = true
        moviePlayer.play()
        
        //moviePlayer.controlStyle = MPMovieControlStyle.Embedded
    }
}
