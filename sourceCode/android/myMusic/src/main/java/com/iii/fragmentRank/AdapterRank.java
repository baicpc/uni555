package com.iii.fragmentRank;

import java.util.ArrayList;
import com.iii.mymusic.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterRank extends BaseAdapter {
	ArrayList<RankEntity> listrank;
	ArrayList<RankEntity> finalListRank = new ArrayList<RankEntity>();
	LayoutInflater inflater;
	Context context;
	int color;

	public AdapterRank(Context context, ArrayList<RankEntity> listsrank,
			int type) {
		this.listrank = listsrank;
		inflater = LayoutInflater.from(context);
		this.context = context;
		for (int i = 0; i < listsrank.size(); i++) {
			if (listsrank.get(i).getType() == type) {
				finalListRank.add(listsrank.get(i));
			}
		}
		Log.d("LIST SIZE ", finalListRank.size() + "");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return finalListRank.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return finalListRank.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHoldler holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.custom_item_rank, null);
			holder = new ViewHoldler();
			holder.stt = (TextView) convertView.findViewById(R.id.tv_sttrank);
			holder.title = (TextView) convertView
					.findViewById(R.id.tv_namerank);
			convertView.setTag(holder);
		} else {
			holder = (ViewHoldler) convertView.getTag();
		}
		final RankEntity rank = finalListRank.get(position);
		if (rank.getType() == RankEntity.TYPE_SONG) {
			// holder.icon.setImageResource(R.drawable.ic_song_search);
			// holder.stt.setText(String.valueOf(position + 1 + "."));
			holder.title.setText(rank.getTitle());

		} else if (rank.getType() == RankEntity.TYPE_PLAYLIST) {
			// holder.icon.setImageResource(R.drawable.ic_song_search);
			// holder.stt.setText(String.valueOf(position + 1 + "."));
			holder.title.setText(rank.getTitle());
		} else if (rank.getType() == RankEntity.TYPE_VIDEO) {
			// holder.icon.setImageResource(R.drawable.ic_video_search);
			// holder.stt.setText(String.valueOf(position + 1 + "."));
			holder.title.setText(rank.getTitle());
		}
		return convertView;
	}

	static class ViewHoldler {
		ImageView icon;
		TextView title, stt;
	}

}
