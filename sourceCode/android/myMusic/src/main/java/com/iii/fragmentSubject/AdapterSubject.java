package com.iii.fragmentSubject;

import java.util.ArrayList;

import com.iii.Ultils.Constant;
import com.iii.mymusic.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterSubject extends ArrayAdapter<EntitySubject> {
	Activity context;
	ArrayList<EntitySubject> arrSub = null;
	EntitySubject sub;
	int rs_layout;

	public AdapterSubject(Activity context, int resource, ArrayList<EntitySubject> arrSub) {
		super(context, resource, arrSub);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrSub = arrSub;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.ivSub = (ImageView) convertView.findViewById(R.id.ivSubject);
			holder.tvName = (TextView) convertView.findViewById(R.id.tvNamesubss);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		sub = arrSub.get(position);
		holder.tvName.setText(sub.getSubname());
		Picasso.with(context).load(Constant.URL_IMG + sub.getImage()).placeholder(R.drawable.down)
				.into(holder.ivSub);
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSub;
		TextView tvName;
	}

}
