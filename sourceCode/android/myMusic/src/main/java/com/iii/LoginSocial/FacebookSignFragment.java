package com.iii.LoginSocial;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WSLoginSocial;
import com.iii.mymusic.R;
import com.iii.mymusic.SplashActivity;

public class FacebookSignFragment extends Fragment {
	public static final String TAG = FacebookSignFragment.class.getSimpleName();

	public static final String KEY_FACEBOOK_PICTURE_LINK = "url";
	public static final String KEY_FACEBOOK_USER_NAME = "name";

	private LoginButton loginBtn;
	private UiLifecycleHelper uiHelper;

	View view;

	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state, final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this.getActivity(), statusCallback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.facebook_fragment, container, false);
		loginBtn = (LoginButton) view.findViewById(R.id.fb_login_button);
		loginBtn.setFragment(this);
		loginBtn.setReadPermissions(
				Arrays.asList("public_profile", "user_likes", "user_friends", "user_status", "email", "user_birthday"));

		return view;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onResume() {
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
		super.onResume();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
	}

	public void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.commit();
	}

	private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			loginBtn.setVisibility(View.INVISIBLE);
			Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (session == Session.getActiveSession()) {
						if (user != null) {
							Log.e(TAG + "FB access token: ", session.getAccessToken());
							try {
								String email = (String) user.getProperty("email");
								final String firstname = user.getFirstName();
								final String lastname = user.getLastName();
								final String id = user.getId();
								final String name = user.getName();
								Session session = Session.getActiveSession();
								session.closeAndClearTokenInformation();
								Log.i("USER INFOMATION", user.getInnerJSONObject().toString());
								new WSLoginSocial(getActivity(), email, firstname, lastname).execute();
								WSLoginSocial.HANDLER_SUCCESS = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										if (Constant.RESULT_API != null) {
											if (Constant.RESULT_API.getCode()
													.equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
//												UltilFunction.clearUserAccount(getActivity());
												String urlPicture = "http://graph.facebook.com/" + id
														+ "/picture?type=large";
												try {
													Intent intent = new Intent(getActivity(), SplashActivity.class);
													intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
													UltilFunction.saveUserWithSocialAccount(Constant.RESULT.getRecord(),
															getActivity(), urlPicture);
													startActivity(intent);
													getActivity().finish();
												}
												catch (Exception e) {

													Log.e("onSessionStateChange", e.toString());
												}
											}
										}
									};
								};
								return;
							} catch (Exception e) {
								session.closeAndClearTokenInformation();
							}
						} else {
							//
						}
					}
				}
			});

			Bundle params = new Bundle();
			params.putString("fields", "id, first_name, last_name, picture, email, name, birthday");
			request.setParameters(params);
			request.executeAsync();

		} else if (state.isClosed()) {
			Log.i(TAG, "Logout");
		}
	}

	private void saveUser(ResultAPI resultAPI, String id) {
		JSONObject json;
		try {
			json = new JSONObject(resultAPI.getRecord());
			SharedPreferences sharedPreference = getActivity().getSharedPreferences(Constant.LOGIN_USER,
					Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = sharedPreference.edit();
			edit.putString(Constant.USERNAME, json.getString("username"));
			edit.putString(Constant.PASSWORD, json.getString("password"));
			edit.putString(Constant.USER_ID, json.getString("id"));
			edit.putString(Constant.PASSWORD, json.getString("password"));
			edit.putString(Constant.FIRSTNAME, json.getString("firstname"));
			edit.putString(Constant.LASTNAME, json.getString("lastname"));
			edit.putString(Constant.EMAIL, json.getString("email"));
			edit.putString(Constant.FACEBOOK_ID, id);
			edit.commit();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}