package com.iii.fragmentRank;



public class RankEntity{
	public static final int TYPE_SONG = 0;
	public static final int TYPE_PLAYLIST = 1;
	public static final int TYPE_VIDEO = 2;

	private int id;
	private String title;
	private String url;
	private String image;
	private String lyric;
	private String nameart;
	private String filepath;
	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	public String getNameart() {
		return nameart;
	}

	public void setNameart(String nameart) {
		this.nameart = nameart;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	private int type;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	public RankEntity(){}
	
	public RankEntity(int id,String title, String url,String image,String lyric,String nameart,String filepath, int type){
		this.id=id;
		this.title=title;
		this.image=image;
		this.url=url;
		this.type=type;
		this.filepath=filepath;
		this.lyric=lyric;
		this.nameart=nameart;
	}
	public RankEntity(int id,String title, String url,String image,String nameart,String filepath, int type){
		this.id=id;
		this.title=title;
		this.image=image;
		this.url=url;
		this.type=type;
		this.filepath=filepath;
		this.nameart=nameart;
	}
	public RankEntity(int id,String title,String image , int type){
		this.id=id;
		this.title=title;
		this.image=image;
		this.type=type;
	}

	
	
}
