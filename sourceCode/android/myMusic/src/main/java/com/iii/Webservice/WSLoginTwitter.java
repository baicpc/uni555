package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class WSLoginTwitter extends AsyncTask<Void, Void, Void> {
	private static final String TAG = WSLoginTwitter.class.getSimpleName();

	Context context;
	private String username = "";
	private String firstname = "";

	public WSLoginTwitter(Context context, String username, String firstname) {
		this.context = context;
		this.username = username;
		this.firstname = firstname;
	}

	@Override
	protected void onPreExecute() {
		Ultilities.showProgress(context);
		if (Constant.RESULT != null)
			Constant.RESULT = null;
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		try {
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/registerAndLogin";
			JSONObject json = new JSONObject();
			json.put("username", this.username);
			json.put("firstname", this.firstname);

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = ConfigurationWS.getInstance(context).getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setRecord(jsonObject.getString("records"));
				Constant.RESULT = result;
				Log.d(TAG, "RESULT NOT NULL");
			}
		} catch (Exception e) {
			Log.e(TAG, "ERROR: " + e.getMessage());
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		Ultilities.hideProgress(context);
		Constant.HANDLER_PROGRESS_COMPLETE.sendMessage(Constant.HANDLER_PROGRESS_COMPLETE.obtainMessage());
	}

}
