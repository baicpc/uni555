package com.iii.fragmentPlaylist;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetPlaylistLike;
import com.iii.Webservice.WSgetPlaylistbyidtype;
import com.iii.Webservice.WSgetType;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentPlaylist extends Fragment {
	public static Handler HANDLER_UPDATE_COUNT;
	View view;
	EntityPlaylist list;
	AdapterPlaylist AdapterPlaylist = null;
	ArrayList<EntityPlaylist> arrPlaylists = null;
	GridView grvPlaylist;
	TextView tvTitle, tvKhac, tv_counter_playlist;
	Button tvMoi, tvnhactre, tvtrutinh;
	PopupWindow popUpWindow;
	EntityType type;
	ImageView imageOther;
	LinearLayout layoutOther;

	static int id_list = 18;
	static int cusorSave = 0;
	static String nameType = "";

	private static FragmentPlaylist fragment;

	public static FragmentPlaylist getInstants() {
		if (fragment == null)
			fragment = new FragmentPlaylist();
		return fragment;
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_accountplaylist, container, false);
		grvPlaylist = (GridView) view.findViewById(R.id.grvplaylist);
		tvKhac = (TextView) view.findViewById(R.id.tvKhac);
		tvMoi = (Button) view.findViewById(R.id.tvNewhotPL);
		tvnhactre = (Button) view.findViewById(R.id.tvNhactrePL);
		tvtrutinh = (Button) view.findViewById(R.id.tvTrutinhPL);
		tvTitle = (TextView) view.findViewById(R.id.tvTilePL);
		layoutOther = (LinearLayout) view.findViewById(R.id.layout_other);
		tv_counter_playlist = (TextView) view.findViewById(R.id.tv_counter_playlist);
		imageOther = (ImageView) view.findViewById(R.id.imageOther);
		if (nameType.equals("")) {
			nameType = getString(R.string.new_hot);
			tvTitle.setText(R.string.new_hot);
		} else {
			tvTitle.setText(nameType);
		}
		tvTitle.setTextColor(getResources().getColor(R.color.blue));
		grvPlaylist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				list = (EntityPlaylist) parent.getItemAtPosition(position);
				Ultilities.UpdatePlaylist(getActivity(), list.getId());
				HANDLER_UPDATE_COUNT = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						fragmentSongList fragsonglist = new fragmentSongList();
						Bundle bundle = new Bundle();
						bundle.putInt("cusor", 1);
						bundle.putInt("idplaylist", list.getId());
						bundle.putString("name", list.getName());
						bundle.putString("image", list.getImage());
						bundle.putInt("counter", list.getCounter() + 1);
						fragsonglist.setArguments(bundle);
						transationToFragment(fragsonglist);
					};
				};
			}
		});

		arrPlaylists = new ArrayList<EntityPlaylist>();
		AdapterPlaylist = new AdapterPlaylist(getActivity(), R.layout.custom_playlist_gridview, arrPlaylists);
		grvPlaylist.setAdapter(AdapterPlaylist);
		tvMoi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AdapterPlaylist.clear();
				id_list = 18;
				getListSongbyidtype();
				tvTitle.setText(R.string.new_hot);
				nameType = getResources().getString(R.string.new_hot);
			}
		});
		tvnhactre.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AdapterPlaylist.clear();
				id_list = 3;
				getListSongbyidtype();
				tvTitle.setText(R.string.nhactre);
				nameType = getResources().getString(R.string.nhactre);
			}
		});
		tvtrutinh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AdapterPlaylist.clear();
				id_list = 2;
				getListSongbyidtype();
				tvTitle.setText(R.string.trutinh);
				nameType = getResources().getString(R.string.trutinh);
			}
		});
		Bundle bundle = getArguments();
		int cusor = 0;
		try {
			cusor = bundle.getInt("cusor");
		} catch (Exception e) {
			cusor = cusorSave;
			e.printStackTrace();
		}
		switch (cusor) {
		case 1:
			getListSongbyidtype();
			cusorSave = 1;
			break;

		case 2:
			SharedPreferences sharedPreference = getActivity().getSharedPreferences(Constant.LOGIN_USER,
					Context.MODE_PRIVATE);
			int userid = sharedPreference.getInt(Constant.USER_ID, 0);
			getPlaylistLike(userid);
			updateTextAccount();
			cusorSave = 2;
			break;
		}

		layoutOther.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (popUpWindow == null) {
					showPopup(v);
				} else {
					if (popUpWindow.isShowing()) {
						popUpWindow.dismiss();
					} else {
						showPopup(v);
					}
				}

			}
		});

		if (!UltilFunction.isConnectingToInternet(getActivity())) {
			Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
		}

		return view;
	}

	private void getType(final ArrayList<EntityType> arrType, final ArrayAdapter<EntityType> adapterType) {
		// TODO Auto-generated method stub
		try {
			UltilFunction.startAsyncTask2(new WSgetType(getActivity()));
			WSgetType.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
						String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
						try {
							JSONObject jsonRootObject = new JSONObject(strJson);
							// Get the instance of JSONArray that contains
							// JSONObjects
							JSONArray jsonArray = jsonRootObject.optJSONArray("records");
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject jsonObject = jsonArray.getJSONObject(i);
								int id = Integer.parseInt(jsonObject.opt("idtype").toString());
								String name = jsonObject.optString("nametype").toString();
								type = new EntityType(id, name);
								arrType.add(type);
							}
							adapterType.notifyDataSetChanged();
						} catch (Exception e) {

						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}


	public void getListSongbyidtype() {
		try {
			UltilFunction.startAsyncTask2(new WSgetPlaylistbyidtype(view.getContext(), id_list));
			WSgetPlaylistbyidtype.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						addPlaylist(Constant.RESULT_API);
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void addPlaylist(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = jsonRootObject.optJSONArray("records");
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					int id = Integer.parseInt(jsonObject.opt("idplaylist").toString());
					String name = jsonObject.opt("playlist").toString();
					String image = jsonObject.opt("image").toString();
					int counter = Integer.parseInt(jsonObject.opt("counter").toString());
					list = new EntityPlaylist(id, name, image, counter, EntityPlaylist.TYPE_ONLINE);
					arrPlaylists.add(list);
					AdapterPlaylist.notifyDataSetChanged();
				}
			} catch (Exception e) {

			}
		}
	}

	public void getPlaylistLike(int userid) {
		try {
			UltilFunction.startAsyncTask2(new WSgetPlaylistLike(view.getContext(), userid));
			WSgetPlaylistLike.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						addPlaylist(Constant.RESULT_API);
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("deprecation")
	private void showPopup(View v) {
		try {
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View layout = inflater.inflate(R.layout.popup_type, null);
			popUpWindow = new PopupWindow(layout, (int) ((8.2 * Constant.widthPixels) / 10), LayoutParams.WRAP_CONTENT);
			int location[] = new int[2];
			v.getLocationOnScreen(location);
			popUpWindow.setOutsideTouchable(true);
			imageOther.setImageResource(R.drawable.btn_choose);
			popUpWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					imageOther.setImageResource(R.drawable.btn_chosesdown);
				}
			});
			popUpWindow.setBackgroundDrawable(new BitmapDrawable());
			popUpWindow.showAtLocation(layout, Gravity.NO_GRAVITY, Constant.widthPixels - popUpWindow.getWidth() - 2,
					location[1] + v.getHeight());
			popUpWindow.setFocusable(true);
			popUpWindow.update();

			final GridView grvType = (GridView) layout.findViewById(R.id.grvType);

			final ArrayList<EntityType> arrType = new ArrayList<EntityType>();
			ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_menu_popup,
					arrType);
			getType(arrType, adapterType);
			grvType.setAdapter(adapterType);
			grvType.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					if (popUpWindow.isShowing()) {
						popUpWindow.dismiss();
					}
					type = (EntityType) parent.getItemAtPosition(position);
					AdapterPlaylist.clear();
					id_list = type.getIdtype();
					getListSongbyidtype();
					AdapterPlaylist.notifyDataSetChanged();
					tvTitle.setText(((EntityType) parent.getItemAtPosition(position)).getNametype());
					nameType = tvTitle.getText().toString();
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			e.toString();
		}
	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

	private void updateTextAccount() {
		tvKhac.setVisibility(View.GONE);
		tvMoi.setVisibility(View.GONE);
		tvnhactre.setVisibility(View.GONE);
		tvtrutinh.setVisibility(View.GONE);
		layoutOther.setVisibility(View.GONE);
		tvTitle.setText(R.string.likesong);
	}

}
