package com.iii.fragmentAudioBooks;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iii.Ultils.Constant;
import com.iii.fragmentSong.EntitySong;
import com.iii.mymusic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterAudioBook extends ArrayAdapter<EntitySong> {
	Activity context;
	ArrayList<EntitySong> arrSong = null;
	int rs_layout;

	public AdapterAudioBook(Activity context, int resource, ArrayList<EntitySong> arrSong) {
		super(context, resource, arrSong);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrSong = arrSong;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.ivSong = (ImageView) convertView.findViewById(R.id.ivSonghome);
			holder.tvSong = (TextView) convertView.findViewById(R.id.tvName);
			holder.tvCounter = (TextView) convertView.findViewById(R.id.tv_counter_playlist);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final EntitySong song = getItem(position);
		holder.tvSong.setText(song.getName());
		holder.tvCounter.setText(String.valueOf(song.getCounter()));
		Picasso.with(context).load(Constant.URL_IMG + song.getImage()).placeholder(R.drawable.down).into(holder.ivSong);
		return convertView;
	}

	static class ViewHolder {
		ImageView ivSong;
		TextView tvSong, tvCounter;

	}

}
