package com.iii.mymusic;

import java.util.ArrayList;

import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentType.EntityType;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class DownloadAdapter extends BaseAdapter {
	private ArrayList<EntitySong> list;
	private Context context;

	public DownloadAdapter(Context ctx, ArrayList<EntitySong> list) {
		// TODO Auto-generated constructor stub
		this.list = list;
		this.context = ctx;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.download_list_item, parent, false);
		}
		final TextView music = (TextView) v.findViewById(R.id.text_name);
		final CheckBox check = (CheckBox) v.findViewById(R.id.check_select);
		final EntitySong entity = list.get(position);
		music.setText(entity.getName());
		check.setChecked(entity.isSelect());
		check.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				entity.setSelect(check.isChecked());
			}
		});
		v.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (check.isChecked()){
					check.setChecked(false);
					entity.setSelect(false);
				}else{
					check.setChecked(true);
					entity.setSelect(true);
				}
			}
		});
		return v;
	}

}
