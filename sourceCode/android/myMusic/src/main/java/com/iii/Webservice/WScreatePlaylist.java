package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class WScreatePlaylist extends AsyncTask<Void,  Void, Void>{
	public static Handler HANDLER_SUCCESS;
	private ConfigurationWS mWS;
	private Context mContext;
	private String nameplaylist;
	private int userid;
	

	public WScreatePlaylist(Context mContext,String nameplaylist,int userid) {
		this.mContext = mContext;
		this.nameplaylist=nameplaylist;
		this.userid=userid;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		Ultilities.showProgress(mContext);
		
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer
					+ "api/updateplaylist";
			JSONObject json = new JSONObject();
			json.put("playlist", nameplaylist);
			json.put("id",userid);

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts",
					stroau);
			Log.e("API-------", json.toString());

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setSuccess(true);
				result.setMessage(jsonObject.getString("mMessage"));
				//result.setStatus(jsonObject.getString("status"));
				//result.setRecord(jsonObject.getString("records"));
				Constant.RESULT_API = result;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		Ultilities.hideProgress(mContext);
		HANDLER_SUCCESS.sendMessage(HANDLER_SUCCESS.obtainMessage());
	}
}
