package com.iii.fragmentSong;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.iii.Account.UserAccount;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Helper;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSLike;
import com.iii.Webservice.WSgetSongbyidartist;
import com.iii.Webservice.WSgetSongbyidplayslist;
import com.iii.Webservice.WSgetSongbyidsub;
import com.iii.Webservice.WSgetSongbyidtype;
import com.iii.fragmentSong.AdapterSong.customButtonAddListener;
import com.iii.fragmentSong.AdapterSong.customButtonDownloadListener;
import com.iii.fragmentSong.AdapterSong.customButtonShareListener;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.DownloadAdapter;
import com.iii.mymusic.R;
import com.squareup.picasso.Picasso;

public class fragmentSongList extends Fragment
        implements customButtonDownloadListener, customButtonShareListener, customButtonAddListener {
    View view;
    ImageView ivplaylist, ivBack;
    TextView tvnameplaylist, tvcounter, tvdescription, ivdownload, tvcounterSong;
    static TextView ivlike;
    ListView lvSong;
    EntitySong song;
    AdapterSong adapterSong = null;
    ArrayList<EntitySong> arrSong = null;
    int userid;
    int idplaylist;
    String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        SharedPreferences sharedPreference = getActivity().getSharedPreferences(Constant.LOGIN_USER,
                Context.MODE_PRIVATE);
        userid = sharedPreference.getInt(Constant.USER_ID, 0);
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ivlike = (TextView) view.findViewById(R.id.btnlike_songlist);
        ivdownload = (TextView) view.findViewById(R.id.btnDown_songlist);
        tvcounterSong = (TextView) view.findViewById(R.id.tvcounterSong);
        ivplaylist = (ImageView) view.findViewById(R.id.ivPlaylist);
        tvnameplaylist = (TextView) view.findViewById(R.id.tvnameplaylist);
        tvcounter = (TextView) view.findViewById(R.id.tvcountersonglist);
        lvSong = (ListView) view.findViewById(R.id.lv_songlist);
        ivBack = (ImageView) view.findViewById(R.id.btnBack);

        arrSong = new ArrayList<EntitySong>();
        adapterSong = new AdapterSong(getActivity(), R.layout.custom_item_home, arrSong);
        lvSong.setAdapter(adapterSong);
        adapterSong.setCustomButtonDownloadListener(fragmentSongList.this);
        adapterSong.setCustomButtonShareListener(fragmentSongList.this);
        adapterSong.setCustomButtonAddListener(fragmentSongList.this);
        lvSong.setEmptyView(view.findViewById(R.id.text_empty));
        lvSong.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListMusic(arrSong);
                song = (EntitySong) parent.getItemAtPosition(position);
                // Ultilities.setOneSongListMusic(arrSong.get(position));
                Ultilities.UpdateSong(getActivity(), song.getId(), adapterSong, null, arrSong, position);
                UltilFunction.updateTextListTitle(title);

            }
        });
        ivlike.setOnClickListener(new OnClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (idplaylist != 0) {
                    UserAccount userAccount = UltilFunction.getUserFromSharedPreference(getActivity());
                    if (UltilFunction.isConnectingToInternet(getActivity())) {
                        if (userAccount != null) {

                            new WSLike(getActivity(), String.valueOf(userid), String.valueOf(idplaylist)).execute();
                            ivlike.setBackground(getResources().getDrawable(R.drawable.like_click));
                        } else
                            showMessage(getString(R.string.fragment_song_list_alert_like), getActivity());
                    } else {
                        showMessage(getString(R.string.main_activity_no_internet), getActivity());
                    }
                }
            }
        });
        Bundle bundle = getArguments();
        final int cusor = bundle.getInt("cusor");

        switch (cusor) {
            case 1:
                int id = bundle.getInt("idplaylist");
                idplaylist = id;
                title = bundle.getString("name");
                String image = bundle.getString("image");
                int counter = bundle.getInt("counter");
                tvnameplaylist.setText(title);
                tvcounter.setText(String.valueOf(counter));
                Picasso.with(getActivity()).load(Constant.URL_IMG + image).placeholder(R.drawable.down)
                        .error(R.drawable.down).into(ivplaylist);
                getListSongbyidplaylist(id);
                break;
            case 2:
                int idsub = bundle.getInt("idsub");
                idplaylist = idsub;
                Log.e("idsub", idsub + "");
                title = bundle.getString("namesub");
                String imagesub = bundle.getString("imgsub");
                tvnameplaylist.setText(title);
                Picasso.with(getActivity()).load(Constant.URL_IMG + imagesub).placeholder(R.drawable.down)
                        .error(R.drawable.down).into(ivplaylist);
                getListSongbyidsub(idsub);
                view.findViewById(R.id.image_headphone).setVisibility(View.INVISIBLE);
                break;
            case 3:
                int idtype = bundle.getInt("idtype");
                idplaylist = idtype;
                getListSongbyidtype(idtype);
                break;
            case 4:
                int idartist = bundle.getInt("idartist");
                idplaylist = idartist;
                title = bundle.getString("nameart");
                String imageart = bundle.getString("imgart");
                tvnameplaylist.setText(title);
                Picasso.with(getActivity()).load(Constant.URL_IMG + imageart).placeholder(R.drawable.down)
                        .error(R.drawable.down).into(ivplaylist);
                getListSongbyidartist(idartist);
                view.findViewById(R.id.image_headphone).setVisibility(View.INVISIBLE);
                break;
            case 7:
                int idplaylist_account = bundle.getInt("idplaylist");
                idplaylist = idplaylist_account;
                title = bundle.getString("name");
                String image_account = bundle.getString("image");
                tvnameplaylist.setText(title);
                Picasso.with(getActivity()).load(Constant.URL_IMG + image_account)
                        .placeholder(R.drawable.down).error(R.drawable.down).into(ivplaylist);
                getListSongbyidplaylist(idplaylist_account);
                view.findViewById(R.id.image_headphone).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.tvcountersonglist).setVisibility(View.INVISIBLE);
                break;
            default:
                break;

        }
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				if (cusor == 4) {
//					FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//					fragmentTransaction.replace(R.id.main_fragment, FragmentSearch.getInstants(),
//							FragmentSearch.class.getSimpleName());
//					fragmentTransaction.addToBackStack(FragmentSearch.class.getSimpleName());
//					fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//					fragmentTransaction.commit();
//				} else {
//					getActivity().onBackPressed();
//				}
                getActivity().onBackPressed();
            }
        });
        ivdownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showPopUpDownload();
            }
        });
        adapterSong.notifyDataSetChanged();
        return view;
    }

    public void getListSongbyidplaylist(int idplaylist) {

        try {
            UltilFunction.startAsyncTask2(new WSgetSongbyidplayslist(view.getContext(), idplaylist));
            WSgetSongbyidplayslist.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    public void getListSongbyidsub(int idsub) {
        try {
            UltilFunction.startAsyncTask2(new WSgetSongbyidsub(view.getContext(), idsub));
            WSgetSongbyidsub.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("getListSongbyidsub", e.toString());
        }
    }

    public void getListSongbyidtype(int idtype) {
        try {
            UltilFunction.startAsyncTask2(new WSgetSongbyidtype(view.getContext(), idtype));
            WSgetSongbyidtype.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    public void getListSongbyidartist(int idartist) {
        try {
            UltilFunction.startAsyncTask2(new WSgetSongbyidartist(view.getContext(), idartist));
            WSgetSongbyidartist.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    public void addToList(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
            try {
                JSONObject jsonRootObject = new JSONObject(strJson);
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = Integer.parseInt(jsonObject.opt("idsong").toString());
                    String name = jsonObject.optString("namesong").toString();
                    String linksong = jsonObject.getString("linksong").toString();
                    String lyric = jsonObject.getString("lyric").toString();
                    int counter = Integer.parseInt(jsonObject.opt("counter").toString());

                    int idplaylist = Integer.parseInt(jsonObject.getString("idplaylist").toString());
                    int idart = 0;
                    if (!jsonObject.getString("idartist").toString().equals("null"))
                        idart = Integer.parseInt(jsonObject.getString("idartist").toString());
                    String image = jsonObject.opt("image").toString();
                    String nameart = jsonObject.optString("artistname").toString();
                    String filepath = jsonObject.opt("filepath").toString();
                    String driver = jsonObject.getString("linkdrive").toString();
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, idplaylist, nameart,
                            filepath, driver);
                    arrSong.add(song);
                }
                adapterSong.notifyDataSetChanged();
                Helper.getListViewSize(lvSong);
            } catch (Exception e) {

                Log.e("testtest", e.toString());
            }
        }
    }

    @Override
    public void onButtonDownloadClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), new EntitySong[]{song});
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onButtonShareClickListener(EntitySong song, int position) {
        String url = song.getLinksong().replaceAll(" ", "%20");
        if (url.equals("") || url.equals("null")) {
            url = song.getFilepath().replaceAll(" ", "%20");
        }
        Log.e("share", url);
        if (!url.equals("null") && !url.equals("")) {
            try {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        song.getName() + " - " + url);
                Log.e("share", song.getName() + " - " + url);

                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getActivity().getPackageManager()
                        .queryIntentActivities(shareIntent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith(
                            "com.facebook.katana")) {
                        shareIntent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }
                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u="
                            + song.getName() + " - " + url;
                    shareIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(sharerUrl));
                }

                getActivity().startActivity(
                        Intent.createChooser(shareIntent, "Share via"));
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    public void popupShowAddplaylist(final int idsong) {
        try {

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_playlist, null);
            final PopupWindow addplaylistPopup = new PopupWindow(getActivity());
            addplaylistPopup.setContentView(layout);
            addplaylistPopup.setWidth((1 * Constant.widthPixels) / 3);
            addplaylistPopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            addplaylistPopup.setFocusable(true);
            addplaylistPopup.update();

            final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_type, arrType);
            Ultilities.getPlaylistUser(adapterType, arrType, getActivity(), userid);
            lvplaylist.setAdapter(adapterType);
            lvplaylist.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    addplaylistPopup.dismiss();
                    EntityType type = (EntityType) parent.getItemAtPosition(position);
                    Ultilities.InsertSong(getActivity(), type.getIdtype(), idsong);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void onButtonAddClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        popupShowAddplaylist(song.getId());

    }

    private void showPopUpDownload() {
        try {
            try {

                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View layout = inflater.inflate(R.layout.popup_download, null);
                final PopupWindow downloadPupup = new PopupWindow(getActivity());
                downloadPupup.setContentView(layout);
                downloadPupup.setWidth(Constant.widthPixels / 2);
                if (arrSong.size() > 0) {
                    downloadPupup.setHeight((2 * Constant.heightPixels) / 3);
                } else {
                    downloadPupup.setHeight(Constant.heightPixels / 2);
                }
                downloadPupup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                downloadPupup.setFocusable(true);
                downloadPupup.update();

                final ListView listDownload = (ListView) layout.findViewById(R.id.list_download);
                final Button btnDownload = (Button) layout.findViewById(R.id.button_download);
                listDownload.setEmptyView(layout.findViewById(R.id.text_list_empty));

                final ArrayList<EntitySong> lists = new ArrayList<EntitySong>();
                if (arrSong.size() > 0) {
                    for (int i = 0; i < arrSong.size(); i++) {
                        lists.add(arrSong.get(i));
                    }
                }
                Log.e("download", lists.size() + "");

                DownloadAdapter adapter = new DownloadAdapter(getActivity(), lists);
                listDownload.setAdapter(adapter);
                btnDownload.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        boolean download = false;
                        if (lists.size() <= 0) {
                            showMessage(getString(R.string.popup_download_list_empty), getActivity());
                        } else {
                            int count = 0;
                            for (int k = 0 ;k < lists.size(); k++) {

                                if (lists.get(k).isSelect()) {
                                    count++;
                                }
                            }
                            EntitySong[] songDownload = new EntitySong[count];
                            int index = 0;
                            for (int i = 0; i < lists.size(); i++) {
                                if (lists.get(i).isSelect()) {
                                    songDownload[index] = lists.get(i);
                                    index++;
                                    download = true;
                                }
                            }
                            if (download) {
                                UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), songDownload);
                            }
                        }
                        downloadPupup.dismiss();
                    }
                });
            } catch (Exception e) {

                Log.e("download", e.toString());
            }
        } catch (Exception e) {

            Log.e("download", e.toString());
        }
    }

    public void showMessage(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
