package com.iii.musicservice;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;
import com.iii.fragmentSong.EntitySong;
import com.iii.musiccontrol.Control;
import com.iii.mymusic.R;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;


public class MusicService extends Service {

    public static final String SONG_PLAY_COMPLETE = "com.iii.music.songcomplete";
    static MediaPlayer mp;
    Timer timer;
    WakeLock wl = null;
    String  type;


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Constant.SONG_PAUSE = false;
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        if (wl == null)
            wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wl.acquire();
        if (mp == null) {
            mp = new MediaPlayer();
        }
        if (timer == null) {
            timer = new Timer();
        }
        mp.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                if (Constant.REPEAT_SONG.equals("NONE")) {
                    if (Constant.SONG_INDEX < Constant.SONG_LIST.size() - 1) {
                        Control.nextControl(getApplicationContext());
                    } else {
                        try {
                            timer.cancel();
                            Constant.SONG_PAUSE = true;
                            Constant.HANDLER_SEEK_TO_ZERO.sendMessage(Constant.HANDLER_SEEK_TO_ZERO.obtainMessage());
                            sendBroadcast(new Intent(SONG_PLAY_COMPLETE));
                        } catch (Exception e) {
                        }
                    }
                } else if (Constant.REPEAT_SONG.equals("ALL")) {
                    Control.nextControl(getApplicationContext());
                } else if (Constant.REPEAT_SONG.equals("ONE")) {
                    Control.repeatOneSongControl(getApplicationContext());
                } else if (Constant.REPEAT_SONG.equals("RANDOM")) {
                    Control.nextControl(getApplicationContext());
                }
                try {
                    Constant.HANDLER_UPDATE_COMPLETE.sendMessage(Constant.HANDLER_UPDATE_COMPLETE.obtainMessage());
                } catch (Exception e) {

                    Log.e("music_create", e.toString());
                }
            }
        });
        mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub
                if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
                    mp.reset();
                    sendError();
                } else if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN) {
                    mp.reset();
                    sendError();
                }
                return false;
            }
        });
    }

    static class MainTask extends TimerTask {
        public void run() {
            handler.sendEmptyMessage(0);
        }
    }

    private final static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mp != null) {

                try {

                    int progress;
                    Integer i[] = new Integer[3];
                    if (mp.getDuration() > 0) {
                        progress = (mp.getCurrentPosition() * 100) / mp.getDuration();
                        i[1] = mp.getDuration();
                    } else {

                        progress = (mp.getCurrentPosition() * 100);

                    }

                    i[0] = mp.getCurrentPosition();
                    i[1] = mp.getDuration();
                    i[2] = progress;
                    Constant.HANDLER_PROGRESSBAR.sendMessage(Constant.HANDLER_PROGRESSBAR.obtainMessage(0, i));
                } catch (Exception e) {
                    Log.e("position_music_catch", e.toString());
                }
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            playSong();

            try {
                mp.seekTo(Ultilities.progressToTimer((int) Constant.CURR_POSITION, mp.getDuration()));
                Constant.HANDLER_UPDATE_COMPLETE.sendMessage(Constant.HANDLER_UPDATE_COMPLETE.obtainMessage());
            } catch (IllegalStateException e) {
                Log.e("onStartCommand", e.toString());
            }
            Constant.HANDLER_SEEK = new Handler(new Handler.Callback() {
                public boolean handleMessage(Message msg) {
                    Integer i = (Integer) msg.obj;
                    if (mp != null) {
                        mp.seekTo(i);
                    }
                    return false;
                }
            });

            Constant.HANDLER_SONG_CHANGE = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    try {
                        playSong();
                        Constant.HANDLER_UPDATE_COMPLETE.sendMessage(Constant.HANDLER_UPDATE_COMPLETE.obtainMessage());
                    } catch (Exception e) {
                        Log.e("onStartCommand2", e.toString());
                    }
                }

                ;
            };
            Constant.HANDLER_PLAY_PAUSE = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    String message = (String) msg.obj;
                    if (mp == null)
                        if (message.equalsIgnoreCase(getResources().getString(R.string.play))) {
                            Constant.SONG_PAUSE = false;
                            mp.start();
                        } else if (message.equalsIgnoreCase(getResources().getString(R.string.pause))) {
                            Constant.SONG_PAUSE = true;
                            mp.pause();
                        }
                    try {
                        Constant.HANDLER_UPDATE_COMPLETE.sendMessage(Constant.HANDLER_UPDATE_COMPLETE.obtainMessage());
                    } catch (Exception e) {
                        Log.e("onStartCommand3", e.toString());
                    }
                    return false;
                }
            });
        } catch (Exception e) {

            Log.e("onStartCommand4", e.toString());
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mp != null) {
            mp.stop();
            mp = null;
        }
        wl.release();
        super.onDestroy();
    }

    private void playSong() {
        Constant.SONG_PAUSE = false;
        EntitySong song = Constant.SONG_LIST.get(Constant.SONG_INDEX);
        String songData = "aaaa";
        Constant.IS_MUSIC_LOCAL = false;
//        if (songData.equals("") || songData.length() <= 0 || !songData.endsWith("mp3")) {
//            songData = song.getFilepath().replace(" ", "%20");
//
//        } else
//            songData = song.getLinkDriver();

        if (!song.getLinksong().toString().equals(""))
            songData = song.getLinksong();
        else if (!song.getFilepath().toString().equals("null") && (songData.equals("") || !songData.endsWith("mp3")))
            songData = song.getFilepath().replace(" ", "%20");
        else if (!song.getLinkDriver().toString().equals("null"))
            songData = song.getLinkDriver();

        if (song.getSongType() == EntitySong.TYPE_LOCAL) {
            Constant.IS_MUSIC_LOCAL = true;
        }
        try {
            mp.reset();
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setDataSource(songData);
            mp.prepare();
            mp.start();
            timer.schedule(new MainTask(), 0, 100);
        } catch (IOException e) {
            Log.e("playsong", e.toString());
        }
    }

    private void sendError() {
        try {
            Constant.SONG_PAUSE = true;
            Constant.HANDLER_UPDATE_COMPLETE.sendMessage(Constant.HANDLER_UPDATE_COMPLETE.obtainMessage());
            Constant.HANDLER_MEDIA_ERROR.sendMessage(Constant.HANDLER_MEDIA_ERROR.obtainMessage());
        } catch (IllegalStateException e) {
            // TODO: handle exception
            Log.e("sendError", e.toString());
        }
    }
}