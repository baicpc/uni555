package com.iii.Account;

public class UserAccount {
	public static final String URL_PICTURE_NULL = "url_null";

	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String urlPicture;
	
	public UserAccount(int id, String firstName, String lastName, String email, String urlPicture) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.urlPicture = urlPicture;
	}
	public UserAccount() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUrlPicture() {
		return urlPicture;
	}
	public void setUrlPicture(String urlPicture) {
		this.urlPicture = urlPicture;
	}
	
}
