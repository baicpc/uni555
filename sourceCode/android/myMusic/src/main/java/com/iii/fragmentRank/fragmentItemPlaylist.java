package com.iii.fragmentRank;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.iii.Ultils.Constant;
import com.iii.fragmentSong.DownloadSongMultiFileAsync;
import com.iii.Ultils.Helper;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetXBXH;
import com.iii.Webservice.WSupdatecounterPlaylist;
import com.iii.fragmentSong.AdapterSong;
import com.iii.fragmentSong.AdapterSong.customButtonAddListener;
import com.iii.fragmentSong.AdapterSong.customButtonDownloadListener;
import com.iii.fragmentSong.AdapterSong.customButtonShareListener;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.fragmentVideo.FragmentVideo;
import com.iii.mymusic.DownloadAdapter;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;
import com.iii.playVideo.ActivityPlayvideo;
import com.squareup.picasso.Picasso;

public class fragmentItemPlaylist extends Fragment
		implements customButtonAddListener, customButtonDownloadListener, customButtonShareListener {
	View view;
	ImageView ivlike, ivshare, ivdownload, ivplaylist, ivBack;
	TextView tvnameplaylist, tvcounter, tvdescription;
	TextView tvcounterSong, tvcounterPlaylist, tvcounterVideo;
	ListView lvSong;
	EntitySong song;
	AdapterSong adapterSong = null;
	ArrayList<EntitySong> arrSong = null;
	ArrayList<RankEntity> arrRank = null;
	RankEntity rank;
	ArrayList<EntitySong> lists;
	int userid;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	};

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_show_rank, container, false);
		ivplaylist = (ImageView) view.findViewById(R.id.ivPlaylist_rank);
		tvnameplaylist = (TextView) view.findViewById(R.id.tvnameplaylist_rank);
		tvcounterSong = (TextView) view.findViewById(R.id.tvcounterSong);
		tvcounterVideo = (TextView) view.findViewById(R.id.tvcountervideo);
		tvcounter = (TextView) view.findViewById(R.id.tvcountersonglist_rank);
		lvSong = (ListView) view.findViewById(R.id.lv_songlist_rank);
		ivBack = (ImageView) view.findViewById(R.id.imgBack_itemplaylist);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
				// transationToFragment(new FragmentRank());
			}
		});
		arrSong = new ArrayList<EntitySong>();
		adapterSong = new AdapterSong(getActivity(), R.layout.custom_item_home, arrSong);
		adapterSong.setCustomButtonDownloadListener(fragmentItemPlaylist.this);
		adapterSong.setCustomButtonShareListener(fragmentItemPlaylist.this);
		adapterSong.setCustomButtonAddListener(fragmentItemPlaylist.this);
		arrRank = new ArrayList<RankEntity>();
		Bundle bundle = getArguments();
		final int cusor = bundle.getInt("cusor");
		int idcountry = bundle.getInt("idcountry");
		lvSong.setEmptyView(view.findViewById(R.id.text_empty));
		lvSong.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
				if (cusor == 1) {
					Ultilities.stopMusicService(getActivity());
					Constant.SONG_INDEX = position;
					Constant.CURR_POSITION = 0;
					Ultilities.setListMusic(arrSong);
					song = (EntitySong) parent.getItemAtPosition(position);
					Ultilities.UpdateSong(getActivity(), song.getId(), adapterSong, null, arrSong, position);
//					Ultilities.setOneSongListMusic(arrSong.get(position));
				} else if (cusor == 2 && rank.getType() == RankEntity.TYPE_PLAYLIST) {
					Ultilities.UpdatePlaylist(getActivity(), rank.getId());
					WSupdatecounterPlaylist.HANDLER_SUCCESS = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							rank = (RankEntity) parent.getItemAtPosition(position);
							fragmentSongList fragsonglist = new fragmentSongList();
							Bundle bundle = new Bundle();
							bundle.putInt("cusor", 1);
							bundle.putInt("idplaylist", rank.getId());
							bundle.putString("name", rank.getTitle());
							bundle.putString("image", rank.getImage());
							fragsonglist.setArguments(bundle);
							transationToFragment(fragsonglist);
						}
					};
				} else if (cusor == 3 && rank.getType() == RankEntity.TYPE_VIDEO) {
					Ultilities.stopMusicService(getActivity());
					rank = (RankEntity) parent.getItemAtPosition(position);
					Ultilities.UpdateVideo(getActivity(), rank.getId(), null, null, position);
					FragmentVideo.HANDLER_UPDATE_COUNTER = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							Intent myIntent = new Intent(getActivity(), ActivityPlayvideo.class);
							Bundle bundle = new Bundle();
							try {
								String url = rank.getUrl();
								if (url == "null" || url.length() == 0) {
									String filepath = rank.getFilepath();
									bundle.putString("linkvideo", filepath.replaceAll(" ", "%20"));
									bundle.putString("name", rank.getTitle());
									myIntent.putExtra("video", bundle);
									startActivity(myIntent);

								} else {
									bundle.putString("linkvideo", url);
									bundle.putString("name", rank.getTitle());
									myIntent.putExtra("video", bundle);
									startActivity(myIntent);
								}
								Constant.SONG_PAUSE = true;
								MainActivity.changeButton();
							} catch (Exception e) {
								e.toString();
							}
						}
					};

				}
			}
		});
		final TextView textDownload = (TextView) view.findViewById(R.id.text_download);
		switch (cusor) {
		case 1:
			tvnameplaylist.setText(getString(R.string.fragment_charts_song));
			String imgsong = bundle.getString("imgsong");
			lvSong.setAdapter(adapterSong);
			getRankSong(idcountry);
			Picasso.with(getActivity()).load(Constant.URL_IMG + imgsong).placeholder(R.drawable.down)
					.into(ivplaylist);
			break;
		case 2:
			tvnameplaylist.setText(getString(R.string.fragment_charts_playlist));
			textDownload.setVisibility(View.GONE);
			String imgplaylist = bundle.getString("imgplaylist");
			getRankPlaylist(idcountry);
			Picasso.with(getActivity()).load(Constant.URL_IMG + imgplaylist)
					.placeholder(R.drawable.down).into(ivplaylist);
			break;
		case 3:
			tvnameplaylist.setText(getString(R.string.fragment_charts_video));
			textDownload.setVisibility(View.GONE);
			String imgvideo = bundle.getString("imgvideo");
			getRankVideo(idcountry);
			Picasso.with(getActivity()).load(Constant.URL_IMG + imgvideo).placeholder(R.drawable.down)
					.into(ivplaylist);
			break;

		}
		textDownload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showPopUpDownload();
			}
		});

		return view;
	}

	public void getRankSong(int id) {
		try {
			UltilFunction.startAsyncTask2(new WSgetXBXH(getActivity(), id));
			WSgetXBXH.HANDLER_SUCCESS = new Handler() {
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							try {
								JSONArray jsonArray = new JSONArray(Constant.RESULT_API.getSong());
								Log.d("JSON Array: ", jsonArray.toString());
								if (jsonArray.length() >= 3) {
									addSong(jsonArray, 3);
								} else {
									addSong(jsonArray, jsonArray.length());
								}
								adapterSong.notifyDataSetChanged();
								Helper.getListViewSize(lvSong);
							} catch (Exception e) {
								e.toString();
							}
						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void getRankPlaylist(int id) {
		try {
			UltilFunction.startAsyncTask2(new WSgetXBXH(getActivity(), id));
			WSgetXBXH.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							try {
								// JSONObject jsonRootObject = new
								// JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = new JSONArray(Constant.RESULT_API.getPlaylist());
								Log.d("JSON Array: ", jsonArray.toString());
								if (jsonArray.length() >= 3) {
									addPlaylist(jsonArray, 3);
								} else {
									addPlaylist(jsonArray, jsonArray.length());
								}
							} catch (Exception e) {
								e.toString();
							}
						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void getRankVideo(int id) {
		try {
			UltilFunction.startAsyncTask2(new WSgetXBXH(getActivity(), id));
			WSgetXBXH.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							try {
								// JSONObject jsonRootObject = new
								// JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = new JSONArray(Constant.RESULT_API.getVideo());
								Log.d("JSON Array: ", jsonArray.toString());
								if (jsonArray.length() >= 3) {
									addVideo(jsonArray, 3);
								} else {
									addVideo(jsonArray, jsonArray.length());
								}
							} catch (Exception e) {
								e.toString();
							}
						}
					}
				}
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onButtonShareClickListener(EntitySong song, int position) {
		String url = song.getLinksong().replaceAll(" ", "%20");
		if (url.equals("") || url.equals("null")) {
			url = song.getFilepath().replaceAll(" ", "%20");
		}
		if (url.equals("null") || url.equals("")) {

			url = song.getLinkDriver();
		}
		try {
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			shareIntent.setType("text/plain");
			shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, song.getName() + " - " + url);
			getActivity().startActivity(Intent.createChooser(shareIntent, "Share via"));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onButtonDownloadClickListener(EntitySong song, int position) {
		// TODO Auto-generated method stub
		UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), new EntitySong[] { song });
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	public void popupShowAddplaylist(final int idsong) {
		try {
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.popup_playlist, (ViewGroup) view.findViewById(R.id.popup_element));
			final PopupWindow addplaylistPopup = new PopupWindow(layout, Constant.widthPixels / 3,
					LayoutParams.WRAP_CONTENT);
			addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
			addplaylistPopup.setContentView(layout);
			addplaylistPopup.setFocusable(true);
			addplaylistPopup.update();
			final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
			lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
			ArrayList<EntityType> arrType = new ArrayList<EntityType>();
			ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_type, arrType);
			Ultilities.getPlaylistUser(adapterType, arrType, getActivity(), userid);
			lvplaylist.setAdapter(adapterType);
			lvplaylist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					addplaylistPopup.dismiss();
					EntityType type = (EntityType) parent.getItemAtPosition(position);
					Ultilities.InsertSong(getActivity(), type.getIdtype(), idsong);

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onButtonAddClickListener(EntitySong song, int position) {
		// TODO Auto-generated method stub
		popupShowAddplaylist(song.getId());
	}

	private void showPopUpDownload() {
		try {
			try {

				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				final View layout = inflater.inflate(R.layout.popup_download, null);
				final PopupWindow downloadPupup = new PopupWindow(getActivity());
				downloadPupup.setContentView(layout);
				downloadPupup.setWidth(Constant.widthPixels / 2);
				if (arrRank.size() > 0 || arrSong.size() > 0) {
					downloadPupup.setHeight((2 * Constant.heightPixels) / 3);
				} else {
					downloadPupup.setHeight(Constant.heightPixels / 2);
				}
				downloadPupup.showAtLocation(layout, Gravity.CENTER, 0, 0);
				downloadPupup.setFocusable(true);
				downloadPupup.update();

				final ListView listDownload = (ListView) layout.findViewById(R.id.list_download);
				final Button btnDownload = (Button) layout.findViewById(R.id.button_download);
				listDownload.setEmptyView(layout.findViewById(R.id.text_list_empty));

				if (arrSong.size() > 0) {
					lists = arrSong;
				} else {
					lists = new ArrayList<EntitySong>();
					if (arrRank.size() > 0) {
						for (int i = 0; i < arrRank.size(); i++) {
							lists.add(new EntitySong(arrRank.get(i).getTitle(), arrRank.get(i).getUrl()));
						}
					}
				}

				DownloadAdapter adapter = new DownloadAdapter(getActivity(), lists);
				listDownload.setAdapter(adapter);
				btnDownload.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						boolean download = false;
						if (lists.size() <= 0) {
							showMessage(getString(R.string.popup_download_list_empty), getActivity());
						} else {
							EntitySong[] songDownload = new EntitySong[lists.size()];
							for (int i = 0; i < lists.size(); i++) {
								if (lists.get(i).isSelect()) {
									songDownload[i] = lists.get(i);
									download = true;
								}
							}
							if (download) {
								UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), songDownload);
							}
						}
						downloadPupup.dismiss();
					}
				});
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

	public void showMessage(String message, Context context) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	private void addPlaylist(JSONArray jsonArray, int length) {
		for (int j = 0; j < length; j++) {
			try {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int idplaylist = Integer.parseInt(jsonObject.opt("idplaylist").toString());
				String name = jsonObject.optString("playlist").toString();
				String image = jsonObject.opt("image").toString();
				rank = new RankEntity(idplaylist, name, image, RankEntity.TYPE_PLAYLIST);
				arrRank.add(rank);
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
		lvSong.setAdapter(new AdapterRank(getActivity(), arrRank, RankEntity.TYPE_PLAYLIST));
		Helper.getListViewSize(lvSong);
	}

	private void addVideo(JSONArray jsonArray, int length) {
		for (int j = 0; j < length; j++) {
			try {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int idvideo = Integer.parseInt(jsonObject.opt("idvideo").toString());
				String name = jsonObject.optString("namevideo").toString();
				String linkvideo = jsonObject.opt("linkurl").toString();
				String image = jsonObject.opt("image").toString();
				String nameart = jsonObject.opt("artistname").toString();
				String filepath = jsonObject.opt("filepath").toString();
				rank = new RankEntity(idvideo, name, linkvideo, image, nameart, filepath, RankEntity.TYPE_VIDEO);
				arrRank.add(rank);
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
		lvSong.setAdapter(new AdapterRank(getActivity(), arrRank, RankEntity.TYPE_VIDEO));
		Helper.getListViewSize(lvSong);
	}

	private void addSong(JSONArray jsonArray, int length) {
		try {
			for (int j = 0; j < length; j++) {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int idsong = Integer.parseInt(jsonObject.opt("idsong").toString());
				String name = jsonObject.optString("namesong").toString();
				String linksong = jsonObject.opt("linksong").toString();
				String lyric = jsonObject.opt("lyric").toString();
				int counter = Integer.parseInt(jsonObject.opt("counter").toString());
				int idplaylist = Integer.parseInt(jsonObject.opt("idplaylist").toString());
				int idart = Integer.parseInt(jsonObject.opt("idartist").toString());
				String image = jsonObject.opt("image").toString();
				String nameart = jsonObject.opt("artistname").toString();
				String filepath = jsonObject.opt("filepath").toString();
				String driver = jsonObject.getString("linkdrive").toString();
				song = new EntitySong(idsong, name, idart, lyric, image, counter, linksong, idplaylist, nameart,
						filepath, driver);
				arrSong.add(song);

			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
	}
}
