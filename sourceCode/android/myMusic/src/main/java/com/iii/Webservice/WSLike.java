package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Ultilities;
import com.iii.mymusic.R;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class WSLike extends AsyncTask<Void, Void, ResultAPI> {

	private ConfigurationWS mWS;
	private Context mContext;
	private String userid;
	private String playlistid;

	public WSLike(Context mContext, String userid, String playlistid) {
		super();
		this.mContext = mContext;
		this.userid = userid;
		this.playlistid = playlistid;
	}

	@Override
	protected void onPreExecute() {
		Ultilities.showProgress(mContext);
	}

	@Override
	protected synchronized ResultAPI doInBackground(Void... params) {
		mWS = new ConfigurationWS(mContext);
		try {
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/updatelike";
			JSONObject json = new JSONObject();
			json.put("iduser", userid);
			json.put("idplaylist", playlistid);
			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts", stroau);
			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setSuccess(jsonObject.getBoolean("mSuccess"));
				return result;
			}
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	protected void onPostExecute(ResultAPI result) {
		Ultilities.hideProgress(mContext);
		if (result != null)
			Toast.makeText(mContext, mContext.getString(R.string.text_liked), Toast.LENGTH_LONG).show();
		else
			Toast.makeText(mContext, mContext.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
	}

}
