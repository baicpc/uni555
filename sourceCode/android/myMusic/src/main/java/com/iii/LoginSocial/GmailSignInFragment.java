package com.iii.LoginSocial;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSLoginSocial;
import com.iii.mymusic.R;
import com.iii.mymusic.SplashActivity;

public class GmailSignInFragment extends Fragment implements OnClickListener, ConnectionCallbacks,
		OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {
	private View v;
	private static final int RC_SIGN_IN = 0;
	private String TAG = "GOOGLE LOGIN";
	private String userEmail, userName, userId;
	String userPhotoUrl;
	private SignInButton btnSignIn;
	private ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.gmail_signin_fragment, container, false);
		btnSignIn = (SignInButton) v.findViewById(R.id.btnSignInGooglePlus);
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail()
				.requestProfile().build();
		Constant.mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).enableAutoManage(getActivity(), this)
				.addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
		btnSignIn.setSize(SignInButton.SIZE_STANDARD);
		btnSignIn.setScopes(gso.getScopeArray());
		btnSignIn.setOnClickListener(this);
		return v;
	}

	public void onStart() {
		super.onStart();
		OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(Constant.mGoogleApiClient);
		if (opr.isDone()) {
			GoogleSignInResult result = opr.get();
			handleSignInResult(result);
		} else {
			showProgressDialog();
			opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
				@Override
				public void onResult(GoogleSignInResult googleSignInResult) {
					hideProgressDialog();
					handleSignInResult(googleSignInResult);
				}
			});
		}
		Log.d("GOOGLE LOGIN", "Started Google Plus " + Constant.mGoogleApiClient.isConnected());
	}

	// [START handleSignInResult]
	private void handleSignInResult(GoogleSignInResult result) {
		Log.d(TAG, "handleSignInResult:" + result.isSuccess());
		if (result.isSuccess()) {
			GoogleSignInAccount acct = result.getSignInAccount();
			userId = acct.getId();
			userEmail = acct.getEmail();
			userName = acct.getDisplayName();
			userPhotoUrl = String.valueOf(acct.getPhotoUrl());
			Log.d("LOGIN GOOGLE", "User ID: " + userId + ", User Email: " + userEmail + " Username: " + userName
					+ " PhotoUrl: " + userPhotoUrl);
			new WSLoginSocial(getActivity(), userEmail, userName, "").execute();
			WSLoginSocial.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							UltilFunction.clearUserAccount(getActivity());
							Intent intent = new Intent(getActivity(), SplashActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							UltilFunction.saveUserWithSocialAccount(Constant.RESULT_API.getRecord(), getActivity(),
									userPhotoUrl);
							startActivity(intent);
							getActivity().finish();
						}
					}
				};
			};
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.d(TAG, "onConnectionFailed:" + connectionResult);
	}

	// [START onActivityResult]
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from
		// GoogleSignInApi.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			handleSignInResult(result);
		}
	}

	// [END onActivityResult]
	// [START signIn]
	private void signIn() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(Constant.mGoogleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}
	// [END signIn]

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSignInGooglePlus:
			signIn();
			break;
		}

	}

	@Override
	public void onResult(LoadPeopleResult peopleData) {
		// TODO Auto-generated method stub
		if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
			PersonBuffer personBuffer = peopleData.getPersonBuffer();
			try {
				int count = personBuffer.getCount();
				for (int i = 0; i < count; i++) {
					Log.d(TAG, "Display name: " + personBuffer.get(i).getDisplayName());
				}
			} finally {
				personBuffer.release();
			}
		} else {
			Log.e(TAG, "Error requesting visible circles: " + peopleData.getStatus());
		}
	}

	private void showProgressDialog() {
		Ultilities.showProgress(getActivity());
	}

	private void hideProgressDialog() {
		Ultilities.hideProgress(getActivity());
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub

	}

}