package com.iii.framentDialog;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.iii.mymusic.R;

public class AppProgressDialog extends ProgressDialog {
  private Animation animation;
  private ImageView la;
  public static ProgressDialog ctor(Context context) {
    AppProgressDialog dialog = new AppProgressDialog(context);
    dialog.setIndeterminate(true);
    dialog.setCancelable(false);
    return dialog;
  }

  public AppProgressDialog(Context context) {
    super(context);
  }

  public AppProgressDialog(Context context, int theme) {
    super(context, theme);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.custom_progress_dialog);
    la = (ImageView) findViewById(R.id.ivprogressdialog);
    la.setImageResource(R.drawable.anim00);
    animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotation);
  }

  @Override
  public void show() {
    super.show();
    la.startAnimation(animation);
  }

  @Override
  public void dismiss() {
    super.dismiss();
    la.clearAnimation();
  }
}
