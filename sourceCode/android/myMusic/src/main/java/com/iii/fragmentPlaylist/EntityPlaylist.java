package com.iii.fragmentPlaylist;

import java.io.Serializable;

public class EntityPlaylist implements Serializable {

	/**
	 * 
	 */
	public static final int TYPE_ONLINE = 0;
	public static final int TYPE_MY= 1;
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String image;
	private int counter;
	private int type;
	

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public EntityPlaylist() {
		// TODO Auto-generated constructor stub
	}
	public EntityPlaylist(int id,String name,String image,int counter,int type){
		this.id=id;
		this.name=name;
		this.image=image;
		this.counter=counter;
		this.type=type;
	}
	public EntityPlaylist(int id,String name,int type){
		this.id=id;
		this.name=name;
		this.type=type;
	}
	
}
