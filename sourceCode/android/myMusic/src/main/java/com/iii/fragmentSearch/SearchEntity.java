package com.iii.fragmentSearch;

public class SearchEntity {

	public static final int TYPE_SONG = 0;
	public static final int TYPE_VIDEO = 1;
	public static final int TYPE_ARTIST = 2;
	
	private int id;
	private String title;
	private String url;
	private String filepath;
	private String image;
	private String driver;
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private int type;
	
	public SearchEntity(){}
	
	public SearchEntity(int id, String title, String url,String filepath,String image, int type, String driver){
		this.id = id;
		this.title = title;
		this.url = url;
		this.type = type;
		this.filepath=filepath;
		this.image=image;
		this.driver = driver;
	}
	public SearchEntity(int id,String title,int type){
		this.id=id;
		this.title=title;
		this.type=type;
	}
	public String getFilepath() {
		return filepath;
	}
	
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return id + " " + title + " " + url + " " + type;
	}
}
