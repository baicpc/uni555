package com.iii.fragmentArtist;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iii.Ultils.Constant;
import com.iii.mymusic.R;
import com.squareup.picasso.Picasso;

public class AdapterArtist extends ArrayAdapter<EntityArtist> {
	Activity context;
	ArrayList<EntityArtist> arrArtist = null;
	EntityArtist artist;
	int rs_layout;

	public AdapterArtist(Activity context, int resource, ArrayList<EntityArtist> arrArtist) {
		super(context, resource, arrArtist);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrArtist = arrArtist;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, parent, false);
			holder = new ViewHolder();
			holder.ivArtist = (ImageView) convertView.findViewById(R.id.ivArtist);
			holder.tvNameArtist = (TextView) convertView.findViewById(R.id.tvNamelistArtist);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		artist = arrArtist.get(position);
		holder.tvNameArtist.setText(artist.getArtistname());
		Picasso.with(context).load(Constant.URL_IMG + artist.getImage()).placeholder(R.drawable.down)
				.into(holder.ivArtist);
		return convertView;
	}

	public class ViewHolder {
		ImageView ivArtist;
		TextView tvNameArtist;
	}
}
