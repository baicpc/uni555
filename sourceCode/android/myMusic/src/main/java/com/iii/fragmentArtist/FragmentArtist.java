package com.iii.fragmentArtist;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WSArtist;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.mymusic.R;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class FragmentArtist extends Fragment {
	View view;
	EntityArtist artist;
	ArrayList<EntityArtist> arrArtist;
	ArrayAdapter<EntityArtist> adapterArtist;
	GridView grvPlaylist;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_artist, container, false);
		grvPlaylist = (GridView) view.findViewById(R.id.grvArtist);
		arrArtist = new ArrayList<EntityArtist>();
		adapterArtist = new AdapterArtist(getActivity(), R.layout.custom_item_artist, arrArtist);
		grvPlaylist.setAdapter(adapterArtist);
		grvPlaylist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				artist = (EntityArtist) parent.getItemAtPosition(position);
				fragmentSongList fragsonglist = new fragmentSongList();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 4);
				bundle.putInt("idartist", artist.getId());
				bundle.putString("nameart", artist.getArtistname());
				bundle.putString("imgart", artist.getImage());
				fragsonglist.setArguments(bundle);
				transationToFragment(fragsonglist);
			}
		});
		if (!UltilFunction.isConnectingToInternet(getActivity())) {
			Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
		}
		getArtist();
		return view;
	}

	public void getArtist() {
		// TODO Auto-generated method stub
		try {
			UltilFunction.startAsyncTask2(new WSArtist(getActivity()));
			WSArtist.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
							try {
								JSONObject jsonRootObject = new JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = jsonRootObject.optJSONArray("records");
								for (int i = 0; i < jsonArray.length(); i++) {
									JSONObject jsonObject = jsonArray.getJSONObject(i);
									int id = Integer.parseInt(jsonObject.opt("idartist").toString());
									String name = jsonObject.opt("artistname").toString();
									String image = jsonObject.opt("image").toString();
									int country = Integer.parseInt(jsonObject.opt("idcountry").toString());
									artist = new EntityArtist(id, name, image, country);
									arrArtist.add(artist);
									adapterArtist.notifyDataSetChanged();
								}
							} catch (Exception e) {

							}
						}
					}
				}
			};
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

}
