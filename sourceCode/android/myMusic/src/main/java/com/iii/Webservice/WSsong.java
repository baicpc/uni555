package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;
import android.content.Context;
import android.os.AsyncTask;

public class WSsong extends AsyncTask<Void, Void, ResultAPI> {
	private ConfigurationWS mWS;
	private Context mContext;

	public WSsong(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

	}

	@Override
	protected synchronized ResultAPI doInBackground(Void... params) {
		// TODO Auto-generated method stub
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/listsong";
			JSONObject json = new JSONObject();

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setMessage(jsonObject.getString("message"));
				// result.setStatus(jsonObject.getString("status"));
				result.setRecord(jsonObject.getString("records"));
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	protected void onPostExecute(ResultAPI result) {
		Constant.RESULT_API_SONG = result;
	}
}
