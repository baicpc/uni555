package com.iii.mymusic;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.iii.Account.UserAccount;
import com.iii.LoginSocial.FacebookSignFragment;
import com.iii.Ultils.Constant;
import com.iii.fragmentAudioBooks.FragmentAudiobooks;
import com.iii.fragmentNews.FragmentNews;
import com.iii.fragmentSong.DownloadSongMultiFileAsync;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSLogin;
import com.iii.Webservice.WSRegister;
import com.iii.fragmentArtist.FragmentArtist;
import com.iii.fragmentDetail.FragmentDetail;
import com.iii.fragmentHome.FragmentHome;
import com.iii.fragmentNCT.FragmentNCT;
import com.iii.fragmentPlaylist.FragmentAccountPlaylist;
import com.iii.fragmentPlaylist.FragmentPlaylist;
import com.iii.fragmentRank.FragmentRank;
import com.iii.fragmentSearch.FragmentSearch;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentSong.FragmentSong;
import com.iii.fragmentStory.FragmentStory;
import com.iii.fragmentSubject.FragmentSubject;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.fragmentVideo.FragmentVideo;
import com.iii.musiccontrol.Control;
import com.iii.musicservice.MusicService;
import com.squareup.picasso.Picasso;

public class MainActivity extends FragmentActivity implements OnSeekBarChangeListener, OnClickListener {
    public static int START_FIRST = 1;
    public static final int MENU_HOME = 0;
    public static final int MENU_SEARCH = 1;
    public static final int MENU_RANK = 2;
    public static final int MENU_SONG = 3;
    public static final int MENU_PLAYLIST = 4;
    public static final int MENU_VIDEO = 5;
    public static final int MENU_SUBJECT = 6;
    public static final int MENU_ARTIST = 7;
    public static final int MENU_NCT = 8;
    public static final int MENU_DOWNLOAD = 9;
    private boolean isShare = true;

    TextView tvposduration, tvtotalduration;
    static TextView tvnamesong;
    TextView tvartistsong;
    TextView tvnct;
    public static TextView tvaccount;
    static ImageView btnPlay, btnRepeat, btnPrevious, btnNext, btnAddplaylist, btnPlaylist, imgAvata,
            btnDownload, btnShare, imgVolume;
    public static ImageView imgAccount;
    SeekBar seekBar;
    Point p;
    EntitySong song;
    PopupWindow loginPopup, registerPopup;
    static Context context;
    private static long total;
    int userid;

    // ---MENU--//
    LinearLayout layoutMenuHome, layoutMenuSearch, layoutMenuRank, layoutMenuSong, layoutMenuPlaylist, layoutMenuVideo,
            layoutMenuSubject, layoutMenuArtist, layoutMenuNct, layoutMenuDownload;
    TextView textMenuHome, textMenuSearch, textMenuRank, textMenuSong, textMenuPlaylist, textMenuVideo, textMenuSubject,
            textMenuArtist, textMenuNct, textMenuDownload;
    ImageView imageMenuHome, imageMenuSearch, imageMenuRank, imageMenuSong, imageMenuPlaylist, imageMenuVideo,
            imageMenuSubject, imageMenuArtist, imageMenuNct, imageMenuDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Constant.heightPixels = getResources().getDisplayMetrics().heightPixels;
        Constant.widthPixels = getResources().getDisplayMetrics().widthPixels;
        context = MainActivity.this;
        UltilFunction.getFirstApp(context);
        tvaccount = (TextView) findViewById(R.id.tvaccount);
        btnDownload = (ImageView) findViewById(R.id.btnDownload_main);
        imgVolume = (ImageView) findViewById(R.id.btn_volume);

        // --MENU LAYOUT--//
        layoutMenuHome = (LinearLayout) findViewById(R.id.layout_menu_home);
        layoutMenuSearch = (LinearLayout) findViewById(R.id.layout_menu_search);
        layoutMenuRank = (LinearLayout) findViewById(R.id.layout_menu_rank);
        layoutMenuSong = (LinearLayout) findViewById(R.id.layout_menu_song);
        layoutMenuPlaylist = (LinearLayout) findViewById(R.id.layout_menu_playlist);
        layoutMenuVideo = (LinearLayout) findViewById(R.id.layout_menu_video);
        layoutMenuSubject = (LinearLayout) findViewById(R.id.layout_menu_subject);
//        layoutMenuArtist = (LinearLayout) findViewById(R.id.layout_menu_artist);
//        layoutMenuNct = (LinearLayout) findViewById(R.id.layout_menu_nct);
        layoutMenuDownload = (LinearLayout) findViewById(R.id.layout_menu_download);

        textMenuHome = (TextView) findViewById(R.id.text_menu_home);
        textMenuSearch = (TextView) findViewById(R.id.text_menu_search);
        textMenuRank = (TextView) findViewById(R.id.text_menu_rank);
        textMenuSong = (TextView) findViewById(R.id.text_menu_song);
        textMenuPlaylist = (TextView) findViewById(R.id.text_menu_playlist);
        textMenuVideo = (TextView) findViewById(R.id.text_menu_video);
        textMenuSubject = (TextView) findViewById(R.id.text_menu_subject);
//        textMenuArtist = (TextView) findViewById(R.id.text_menu_artist);
//        textMenuNct = (TextView) findViewById(R.id.text_menu_nct);
        textMenuDownload = (TextView) findViewById(R.id.text_menu_download);

        imageMenuHome = (ImageView) findViewById(R.id.image_menu_home);
        imageMenuSearch = (ImageView) findViewById(R.id.image_menu_search);
        imageMenuRank = (ImageView) findViewById(R.id.image_menu_rank);
        imageMenuSong = (ImageView) findViewById(R.id.image_menu_song);
        imageMenuPlaylist = (ImageView) findViewById(R.id.image_menu_playlist);
        imageMenuVideo = (ImageView) findViewById(R.id.image_menu_video);
        imageMenuSubject = (ImageView) findViewById(R.id.image_menu_subject);
//        imageMenuArtist = (ImageView) findViewById(R.id.image_menu_artist);
//        imageMenuNct = (ImageView) findViewById(R.id.image_menu_nct);
        imageMenuDownload = (ImageView) findViewById(R.id.image_menu_download);

        btnDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UltilFunction.isConnectingToInternet(context)) {
                    if (Constant.SONG_LIST.size() > 0) {
                        if (!(Constant.SONG_LIST.get(Constant.SONG_INDEX).getSongType() == EntitySong.TYPE_LOCAL)) {
                            UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(context),
                                    new EntitySong[]{Constant.SONG_LIST.get(Constant.SONG_INDEX)});
                        } else {
                            showMessage(getString(R.string.song_downloaded), context);
                        }
                    } else {
                        showMessage(getString(R.string.popup_download_list_empty), context);
                    }
                } else {
                    showMessage(getString(R.string.main_activity_no_internet), context);
                }
            }
        });

        btnShare = (ImageView) findViewById(R.id.btnShare_main);
        btnShare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (UltilFunction.isConnectingToInternet(context)) {
                    UserAccount userAcc = UltilFunction.getUserFromSharedPreference(context);
                    if (userAcc != null) {
                        userid = userAcc.getId();
                        if (Constant.IS_LISTEN_ONLINE) {
                            if (Constant.SONG_LIST.size() == 0) {
                                showMessage(getString(R.string.no_song), context);
                            } else {
                                String url = Constant.SONG_LIST.get(Constant.SONG_INDEX).getLinksong();
                                if (url.equals("")) {
                                    url = Constant.SONG_LIST.get(Constant.SONG_INDEX).getFilepath().replaceAll(" ", "%20");
                                }

                                Log.e("share_main", url);
                                if (!url.equals("null") && !url.equals("")) {
                                    if (!url.substring(0, 8).equals("/storage")) {
                                        try {
                                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                                            shareIntent.setType("text/plain");
                                            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                                                    Constant.SONG_LIST.get(Constant.SONG_INDEX).getName() + " - " + url);
                                            startActivity(Intent.createChooser(shareIntent, "Share via"));
                                        } catch (Exception e) {

                                            Log.e("share_main", e.toString());
                                        }
                                    } else {
                                        Log.e("share_main", "fail");
                                    }
                                }
                            }
                        }
                    } else {
                        showPopupWindowLogin();
                    }
                } else {
                    showMessage(getString(R.string.main_activity_no_internet), context);
                }
            }
        });
//		PackageInfo info;
//		try {
//			info = getPackageManager().getPackageInfo("com.iii.mymusic", PackageManager.GET_SIGNATURES);
//			for (Signature signature : info.signatures) {
//				MessageDigest md;
//				md = MessageDigest.getInstance("SHA");
//				md.update(signature.toByteArray());
//				String something = new String(Base64.encode(md.digest(), 0));
//				//String something = new String(Base64.encodeBytes(md.digest()));
//				Log.e("hash key-------------", something);
//			}
//		} catch (PackageManager.NameNotFoundException e1) {
//			Log.e("name not found", e1.toString());
//		} catch (NoSuchAlgorithmException e) {
//			Log.e("no such an algorithm", e.toString());
//		} catch (Exception e) {
//			Log.e("exception", e.toString());
//		}
        btnAddplaylist = (ImageView) findViewById(R.id.btnAddsongtoplaylist);
        btnAddplaylist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (UltilFunction.isConnectingToInternet(context)) {
                    UserAccount userAcc = UltilFunction.getUserFromSharedPreference(context);
                    if (userAcc != null) {
                        userid = userAcc.getId();
                        if (Constant.SONG_LIST.size() > 0) {
                            popupShowAddplaylist(Constant.SONG_LIST.get(Constant.SONG_INDEX).getId(), userid);
                        } else {
                            showMessage(getString(R.string.popup_playlist_list_empty), context);
                        }
                    } else {
                        showPopupWindowLogin();
                    }
                } else {
                    showMessage(getString(R.string.main_activity_no_internet), context);
                }
            }
        });
        // Media Player;
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnRepeat = (ImageView) findViewById(R.id.btn_repeat);
        btnPrevious = (ImageView) findViewById(R.id.btn_previous);
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnPlaylist = (ImageView) findViewById(R.id.btn_playlisting);
        tvposduration = (TextView) findViewById(R.id.duration);
        tvtotalduration = (TextView) findViewById(R.id.totaldurantion);
        tvnamesong = (TextView) findViewById(R.id.tvNameSong_main);
        seekBar = (SeekBar) findViewById(R.id.seekBarmedia);
        imgAvata = (ImageView) findViewById(R.id.image_avatar_player);
        imgAccount = (ImageView) findViewById(R.id.ivAvatar);
        seekBar.setOnSeekBarChangeListener(this);


        btnPlay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Constant.CURR_POSITION = seekBar.getProgress() + 1;
                if (Constant.SONG_LIST.size() > 0) {
                    if (Ultilities.isServiceRunning(MusicService.class.getName(), context)) {
                        Ultilities.stopMusicService(getApplicationContext());
                        Constant.SONG_PAUSE = true;
                        changeButton();
                    } else {
                        Ultilities.startMusicService(getApplicationContext());
                    }
                } else {
                    Log.i("LIST SONG", "size of list is 0");
                    showMessage(getString(R.string.select_song_to_play), context);
                }
            }
        });
        btnRepeat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Constant.REPEAT_SONG.equals("RANDOM")) {
                    Constant.REPEAT_SONG = "ONE";
                    btnRepeat.setImageResource(R.drawable.button_repeat_1);
                } else if (Constant.REPEAT_SONG.equals("ONE")) {
                    Constant.REPEAT_SONG = "ALL";
                    btnRepeat.setImageResource(R.drawable.button_repeat_all);
                } else if (Constant.REPEAT_SONG.equals("ALL")) {
                    Constant.REPEAT_SONG = "NONE";
                    btnRepeat.setImageResource(R.drawable.no_repeat);
                } else if (Constant.REPEAT_SONG.equals("NONE")) {
                    Constant.REPEAT_SONG = "RANDOM";
                    btnRepeat.setImageResource(R.drawable.button_shuffle);
                }
                UltilFunction.saveRepeat(context, Constant.REPEAT_SONG);
            }
        });
        btnNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Ultilities.isServiceRunning(MusicService.class.getName(), context)) {
                    Control.nextControl(getApplicationContext());
                } else {
                    Ultilities.updateNextPrevious(Control.NEXT);
                    changeUI();
                }
            }
        });
        btnPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Ultilities.isServiceRunning(MusicService.class.getName(), context)) {
                    Control.previousControl(getApplicationContext());
                } else {
                    Ultilities.updateNextPrevious(Control.PREVIOUS);
                    changeUI();
                }
            }
        });

        btnPlaylist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showPopupPlaylist(v);
            }
        });
        /*
         * tvhome.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub addMain(); } });
		 */
        imgAvata.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Constant.SONG_LIST.size() != 0) {
                    EntitySong song = Constant.SONG_LIST.get(Constant.SONG_INDEX);
                    if (song.getSongType() == EntitySong.TYPE_ONLINE) {
                        if (UltilFunction.isConnectingToInternet(context)) {
                            FragmentDetail fragDetail = new FragmentDetail();
                            Bundle bundle = new Bundle();
                            bundle.putString("name", song.getName());
                            bundle.putString("art", song.getNameart());
                            bundle.putString("lyric", song.getLyric());
                            bundle.putString("image", song.getImage());
                            fragDetail.setArguments(bundle);
                            transationToFragment(fragDetail);
                        } else {
                            showMessage(getString(R.string.main_activity_no_internet), context);
                        }
                    }
                } else {
                    showMessage(getString(R.string.no_song), MainActivity.this);
                }
            }
        });
        imgAccount.setOnClickListener(this);
        imgVolume.setOnClickListener(this);

        // --MENU CLICK--//
        layoutMenuHome.setOnClickListener(this);
        layoutMenuSearch.setOnClickListener(this);
        layoutMenuRank.setOnClickListener(this);
        layoutMenuSong.setOnClickListener(this);
        layoutMenuPlaylist.setOnClickListener(this);
        layoutMenuVideo.setOnClickListener(this);
        layoutMenuSubject.setOnClickListener(this);
//        layoutMenuArtist.setOnClickListener(this);
//        layoutMenuNct.setOnClickListener(this);
        layoutMenuDownload.setOnClickListener(this);

        // --UPDATE USER INFO--//
        updateUserInfo();
        if (UltilFunction.isConnectingToInternet(context)) {
            transationToFragment(new FragmentHome());
        } else {
            FragmentNCT frag = new FragmentNCT();
            Bundle bundle = new Bundle();
            bundle.putInt("cusor", 1);
            frag.setArguments(bundle);
            transationToFragment(frag);
            updateMenu(MENU_NCT);
        }
    }

    ArrayAdapter<EntitySong> adapterPlaylist;

    protected void showPopupPlaylist(View v) {
        // TODO Auto-generated method stub
        try {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_playlist, (ViewGroup) findViewById(R.id.popup_element));
            ListView lvPlaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            final TextView textTitle = (TextView) layout.findViewById(R.id.text_title);
            adapterPlaylist = new AdapterShowPlaylist(MainActivity.this, R.layout.custom_item_type, Constant.SONG_LIST);
            lvPlaylist.setAdapter(adapterPlaylist);
            final PopupWindow backPopup = new PopupWindow(context);
            int location[] = new int[2];
            v.getLocationOnScreen(location);
            backPopup.setContentView(layout);
            backPopup.setWidth(Constant.widthPixels / 3);
            backPopup.setHeight((2 * Constant.heightPixels) / 3);
            backPopup.showAtLocation(layout, Gravity.NO_GRAVITY, Constant.widthPixels - backPopup.getWidth(),
                    Constant.heightPixels - backPopup.getHeight() - (Constant.heightPixels - location[1]));
            backPopup.setFocusable(true);
            backPopup.update();
            if (Constant.SONG_LIST.size() > 0) {
                lvPlaylist.setSelection(Constant.SONG_INDEX);
                textTitle.setText(Constant.TITLE_LIST_IS_PLAYING);
            }
            backPopup.setOnDismissListener(new OnDismissListener() {

                @Override
                public void onDismiss() {

                }
            });
            lvPlaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            lvPlaylist.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (Ultilities.isServiceRunning(MusicService.class.getName(), getApplicationContext())) {
                        Constant.SONG_INDEX = position;
                        try {
                            Control.songChange();
                        } catch (Exception e) {
                        }
                    } else {
                        Ultilities.startMusicService(getApplicationContext());
                    }
                    adapterPlaylist.notifyDataSetChanged();
                }
            });
            adapterPlaylist.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
            e.toString();
        }
    }

    // @Override
    // public boolean onCreateOptionsMenu(Menu menu) {
    // // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.main, menu);
    // return true;
    // }
    //
    // @Override
    // public boolean onOptionsItemSelected(MenuItem item) {
    // int id = item.getItemId();
    // if (id == R.id.action_settings) {
    // return true;
    // }
    // return super.onOptionsItemSelected(item);
    // }

    public int getBufferPercentage() {
        return 0;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = Ultilities.progressToTimer(seekBar.getProgress(), (int) total);
        if (Ultilities.isServiceRunning(MusicService.class.getName(), getApplicationContext()))
            Constant.HANDLER_SEEK.sendMessage(Constant.HANDLER_SEEK.obtainMessage(0, progress));
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean songPause = UltilFunction.getPlayStatus(getApplicationContext());
        Constant.SONG_PAUSE = songPause && !Ultilities.isServiceRunning(MusicService.class.getName(), getApplicationContext());
        if (Constant.isContinue == true) {
            Ultilities.startMusicService(getApplicationContext());
            Constant.isContinue = false;
            Constant.SONG_PAUSE = false;
        }
        Constant.REPEAT_SONG = UltilFunction.getRepeat(context);
        changeUI();
        try {
            Constant.HANDLER_PROGRESSBAR = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    Integer i[] = (Integer[]) msg.obj;
                    tvposduration.setText(Ultilities.milisecondToTimer(i[0]));
                    tvtotalduration.setText(Ultilities.milisecondToTimer(i[1]));
                    seekBar.setProgress(Ultilities.getProgressPercentage(i[0], i[1]));
                    total = i[1];
                }
            };
            Constant.HANDLER_UPDATE_COMPLETE = new Handler() {
                public void handleMessage(Message arg0) {
                    if (adapterPlaylist != null)
                        adapterPlaylist.notifyDataSetChanged();
                    changeUI();
                }
            };
            Constant.HANDLER_SEEK_TO_ZERO = new Handler() {
                public void handleMessage(Message arg0) {
                    tvposduration.setText("00:00");
                    tvtotalduration.setText("00:00");
                    seekBar.setProgress(0);
                    changeButton();
                    if (Constant.SONG_PAUSE) {
                        Ultilities.stopMusicService(context);
                    }
                }
            };
            Constant.HANDLER_MEDIA_ERROR = new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    showMessage(getString(R.string.error_connection), context);
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        UltilFunction.savePlayStatus(getApplicationContext(), true);
        if (Constant.SONG_PAUSE)
            Ultilities.stopMusicService(getApplicationContext());
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAvatar:
                UserAccount userAccount = UltilFunction.getUserFromSharedPreference(context);
                if (userAccount == null) {
                    showPopupWindowLogin();
                } else {
                    transationToFragment(new FragmentAccountPlaylist());
                    if (!UltilFunction.isConnectingToInternet(context)) {
                        showMessage(getString(R.string.main_activity_no_internet), context);
                    }
                }

                break;
            case R.id.btn_volume:
                Ultilities.showVolumeControl(getBaseContext());
                break;
            case R.id.layout_menu_home:
                isShare = true;
                transationToFragment(FragmentHome.getInstants());
                updateMenu(MENU_HOME);
                // if (UltilFunction.isConnectingToInternet(context)) {
                // transationToFragment(FragmentHome.getInstants());
                // updateMenu(MENU_HOME);
                // } else {
                // showMessage(getString(R.string.main_activity_no_internet),
                // context);
                // }
                break;
            case R.id.layout_menu_search:
                isShare = true;
                transationToFragment(FragmentSearch.getInstants());
                updateMenu(MENU_SEARCH);
                // if (UltilFunction.isConnectingToInternet(context)) {
                // transationToFragment(FragmentSearch.getInstants());
                // updateMenu(MENU_SEARCH);
                // } else {
                // showMessage(getString(R.string.main_activity_no_internet),
                // context);
                // }
                break;
            case R.id.layout_menu_rank:
                isShare = true;
                transationToFragment(FragmentStory.getInstants());
                updateMenu(MENU_RANK);
                break;
            case R.id.layout_menu_song:
                isShare = true;
                transationToFragment(FragmentSong.getInstants());
                updateMenu(MENU_SONG);
                break;
            case R.id.layout_menu_playlist:
                isShare = true;
                transationToFragment(FragmentAudiobooks.getInstants());
                updateMenu(MENU_PLAYLIST);
                break;
            case R.id.layout_menu_video:
                isShare = true;
                transationToFragment(FragmentVideo.getInstants());
                updateMenu(MENU_VIDEO);
                break;
            case R.id.layout_menu_subject:
                isShare = true;
                transationToFragment(FragmentNews.getInstants());
                updateMenu(MENU_SUBJECT);
                break;






//            case R.id.layout_menu_artist:
//                isShare = true;
//                transationToFragment(new FragmentArtist());
//                updateMenu(MENU_ARTIST);
//                break;
//            case R.id.layout_menu_nct:
//                isShare = false;
//                FragmentNCT frag2 = new FragmentNCT();
//                Bundle bundle2 = new Bundle();
//                bundle2.putInt("cusor", 2);
//                frag2.setArguments(bundle2);
//                transationToFragment(frag2);
//                updateMenu(MENU_DOWNLOAD);
//                break;
            case R.id.layout_menu_download:
//			FragmentNCT frag2 = new FragmentNCT();
//			Bundle bundle2 = new Bundle();
//			bundle2.putInt("cusor", 2);
//			frag2.setArguments(bundle2);
//			transationToFragment(frag2);
//			updateMenu(MENU_DOWNLOAD);
                break;
            default:
                break;
        }

    }

    // popup login
    private void showPopupWindowLogin() {
        try {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.login_popup, (ViewGroup) findViewById(R.id.popup_element));
            loginPopup = new PopupWindow(layout, Constant.widthPixels / 2, LayoutParams.WRAP_CONTENT);
            loginPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            loginPopup.setFocusable(true);
            loginPopup.update();
            layout.setAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_in));
            final EditText editUser = (EditText) layout.findViewById(R.id.edit_username);
            final EditText editPassword = (EditText) layout.findViewById(R.id.edit_password);
            Button imageAgree = (Button) layout.findViewById(R.id.image_agree);
            ImageView imageFacebook = (ImageView) layout.findViewById(R.id.image_facebook);
            ImageView imageGmail = (ImageView) layout.findViewById(R.id.image_gmail);
            Button btnCancel = (Button) layout.findViewById(R.id.button_cancel);
            Button btnRegister = (Button) layout.findViewById(R.id.text_register);
            imageAgree.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    login(editUser, editPassword);
                }
            });

            imageFacebook.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    loginPopup.dismiss();
                    UltilFunction.clearUserAccount(context);
                    transationToFragment(new FacebookSignFragment());

                }
            });
//			imageGmail.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					loginPopup.dismiss();
//					UltilFunction.clearUserAccount(context);
//					transationToFragment(new GmailSignInFragment());
//
//				}
//			});
            btnCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    if (loginPopup.isShowing())
                        loginPopup.dismiss();
                }
            });
            editPassword.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        Ultilities.hideKeyBoard(MainActivity.this, v);
                        loginPopup.dismiss();
                        login(editUser, editPassword);
                        return true;
                    }
                    return false;
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    loginPopup.dismiss();
                    showPopupWindowRegister();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // popup register
    private void showPopupWindowRegister() {
        try {
            final String blockCharacterSet = "+×÷=%_€£¥₩!@#$/^&*()-'\":;,? ";
            final String blockCharacterSet2 = " ";

            InputFilter filter = new InputFilter() {

                @Override
                public CharSequence filter(CharSequence source, int arg1, int arg2,
                                           Spanned arg3, int arg4, int arg5) {
                    // TODO Auto-generated method stub
                    if (source != null && blockCharacterSet.contains(("" + source))) {
                        return "";
                    }
                    return null;
                }
            };

            InputFilter filter2 = new InputFilter() {

                @Override
                public CharSequence filter(CharSequence source, int arg1, int arg2,
                                           Spanned arg3, int arg4, int arg5) {
                    // TODO Auto-generated method stub
                    if (source != null && blockCharacterSet2.contains(("" + source))) {
                        return "";
                    }
                    return null;
                }
            };

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.register_popup, (ViewGroup) findViewById(R.id.popup_element));
            registerPopup = new PopupWindow(layout, Constant.widthPixels / 2, LayoutParams.WRAP_CONTENT);
            registerPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            registerPopup.setFocusable(true);
            registerPopup.update();
            layout.setAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_in));
            final EditText editUser = (EditText) layout.findViewById(R.id.edit_username);
            editUser.setFilters(new InputFilter[]{filter});
            final EditText editEmail = (EditText) layout.findViewById(R.id.edit_email);
            final EditText editPassword = (EditText) layout.findViewById(R.id.edit_password);
            editPassword.setFilters(new InputFilter[]{filter2});
            final EditText editPasswordAgain = (EditText) layout.findViewById(R.id.edit_password_again);
            editPasswordAgain.setFilters(new InputFilter[]{filter2});
            Button buttonAgree = (Button) layout.findViewById(R.id.image_agree);
            Button buttonCancel = (Button) layout.findViewById(R.id.button_cancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    registerPopup.dismiss();
                }
            });
            buttonAgree.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    String userName = editUser.getText().toString();
                    String email = editEmail.getText().toString();
                    String password = editPassword.getText().toString();
                    String passwordAgain = editPasswordAgain.getText().toString();
                    if (userName.length() == 0 || email.length() == 0 || password.length() == 0
                            || passwordAgain.length() == 0 || !password.equals(passwordAgain)) {
                        showMessage(getString(R.string.register_popup_fill_info), context);
                    } else if (!UltilFunction.isEmailValid(email)) {
                        showMessage(getString(R.string.format_not_email), context);
                    } else {
                        register(userName, password, email, registerPopup);
                    }
                }
            });

            editPassword.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        Ultilities.hideKeyBoard(MainActivity.this, v);
                        loginPopup.dismiss();
                        login(editUser, editPassword);
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMessage(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    private void transationToFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    int backCount = 0;
    Runnable runExit = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            backCount = 0;
        }
    };

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        ImageView ivshowplaylist = (ImageView) findViewById(R.id.btn_playlisting);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        ivshowplaylist.getLocationOnScreen(location);

        // Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

    public void popupShowAddplaylist(final int idsong, int userid) {
        try {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_playlist, null);
            final PopupWindow addplaylistPopup = new PopupWindow(this);
            addplaylistPopup.setContentView(layout);
            addplaylistPopup.setWidth(Constant.widthPixels / 2);
            addplaylistPopup.setHeight(Constant.heightPixels / 2);
            addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            addplaylistPopup.setFocusable(true);
            addplaylistPopup.update();

            final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(MainActivity.this, R.layout.custom_item_type,
                    arrType);
            Ultilities.getPlaylistUser(adapterType, arrType, MainActivity.this, userid);
            lvplaylist.setAdapter(adapterType);
            lvplaylist.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    addplaylistPopup.dismiss();
                    EntityType type = (EntityType) parent.getItemAtPosition(position);
                    Ultilities.InsertSong(context, type.getIdtype(), idsong);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void login(EditText editUser, EditText editPassword) {
        if (editUser.getText().toString().length() == 0 || editPassword.getText().toString().length() == 0) {
            showMessage(getString(R.string.fill_info_login), MainActivity.this);
        } else {
            new WSLogin(MainActivity.this, editUser.getText().toString().trim(),
                    editPassword.getText().toString().trim()).execute();
            Constant.HANDLER_PROGRESS_COMPLETE = new Handler(new Handler.Callback() {

                @Override
                public boolean handleMessage(Message msg) {
                    if (Constant.RESULT != null) {
                        if (Constant.RESULT.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
                            UltilFunction.clearUserAccount(context);
                            loginPopup.dismiss();
                            transationToFragment(new FragmentHome());
                            showMessage(getString(R.string.login_success), MainActivity.this);
                            UltilFunction.saveUser(Constant.RESULT.getRecord(), context);
                            updateUserInfo();
                        } else {
                            showMessage(getString(R.string.error_login_info), MainActivity.this);
                        }
                    } else {
                        showMessage(getString(R.string.error_login_internet), MainActivity.this);
                    }
                    return false;
                }
            });
        }
    }

    private void register(String username, String password, String email, final PopupWindow registerPopup) {
        new WSRegister(MainActivity.this, username, email, password).execute();
        Constant.HANDLER_PROGRESS_COMPLETE = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                if (Constant.RESULT != null) {
                    if (Constant.RESULT.getCode().equalsIgnoreCase("200")) {
                        registerPopup.dismiss();
                        showMessage(getString(R.string.register_success), MainActivity.this);
                    } else {
                        showMessage(getString(R.string.register_error_account), MainActivity.this);
                    }
                } else {
                    showMessage(getString(R.string.register_error), MainActivity.this);
                }
                return false;
            }
        });
    }

    // ---NHAT--//
    public static void changeButton() {
        if (Constant.SONG_PAUSE) {
            btnPlay.setImageResource(R.drawable.button_play);
        } else {
            btnPlay.setImageResource(R.drawable.button_pause);
        }
        if (Constant.REPEAT_SONG.equals("NONE"))
            btnRepeat.setImageResource(R.drawable.no_repeat);
        else if (Constant.REPEAT_SONG.equals("ONE"))
            btnRepeat.setImageResource(R.drawable.button_repeat_1);
        else if (Constant.REPEAT_SONG.equals("ALL"))
            btnRepeat.setImageResource(R.drawable.button_repeat_all);
        else if (Constant.REPEAT_SONG.equals("RANDOM"))
            btnRepeat.setImageResource(R.drawable.button_shuffle);
    }

    public static void changeUI() {
        if (Constant.SONG_LIST.size() > 0)
            updateUI();
        changeButton();
    }

    public static void updateUI() {
        try {
            EntitySong song = Constant.SONG_LIST.get(Constant.SONG_INDEX);
            tvnamesong.setText(song.getName() + (song.getNameart().length() > 0 ? " - " + song.getNameart() : ""));
            updateAlbumArt();
        } catch (Exception e) {

            Log.e("updateUI", e.toString());
        }
    }

    private void updateUserInfo() {
        UserAccount userAccount = UltilFunction.getUserFromSharedPreference(context);
        if (userAccount != null) {
            tvaccount.setVisibility(View.VISIBLE);
            tvaccount.setText(userAccount.getFirstName());
            if (!userAccount.getUrlPicture().equals(UserAccount.URL_PICTURE_NULL)) {
                Picasso.with(MainActivity.this).load(userAccount.getUrlPicture()).into(imgAccount);
            } else {
                imgAccount.setImageResource(R.drawable.avatar_user);
            }
        } else {
            tvaccount.setVisibility(View.GONE);
        }
    }

    public static void updateAlbumArt() {
        String onlineData = Constant.URL_IMG + Constant.SONG_LIST.get(Constant.SONG_INDEX).getImage();
        Log.i("LINK ONLINE: ", onlineData);
        String localData = Constant.SONG_LIST.get(Constant.SONG_INDEX).getFilepath();
        Log.i("LINK OFFLINE: ", localData);
        String data = (Constant.IS_MUSIC_LOCAL) ? localData : onlineData;
        UltilFunction.setImageAlbumArt(context, data, imgAvata);
    }

    @SuppressLint("NewApi")
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private void updateMenu(int menu) {
        if (menu == MENU_HOME) {
            textMenuHome.setTextColor(getResources().getColor(R.color.blue));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_homeclick);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_SEARCH) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.blue));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_searchclick);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_RANK) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.blue));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rankclick);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_SONG) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.blue));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_songclick);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_PLAYLIST) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.blue));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlistclick);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_VIDEO) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.blue));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_videoclick);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_SUBJECT) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.blue));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_subclick);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_ARTIST) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.blue));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artistclick);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_NCT) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.blue));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.ic_nct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuDownload.setImageResource(R.drawable.download);
        } else if (menu == MENU_DOWNLOAD) {
            textMenuHome.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSearch.setTextColor(getResources().getColor(R.color.text_color));
            textMenuRank.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSong.setTextColor(getResources().getColor(R.color.text_color));
            textMenuPlaylist.setTextColor(getResources().getColor(R.color.text_color));
            textMenuVideo.setTextColor(getResources().getColor(R.color.text_color));
            textMenuSubject.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuArtist.setTextColor(getResources().getColor(R.color.text_color));
//            textMenuNct.setTextColor(getResources().getColor(R.color.text_color));
            imageMenuHome.setImageResource(R.drawable.ic_home);
            imageMenuSearch.setImageResource(R.drawable.ic_search);
            imageMenuRank.setImageResource(R.drawable.ic_rank);
            imageMenuSong.setImageResource(R.drawable.ic_song);
            imageMenuPlaylist.setImageResource(R.drawable.ic_playlist);
            imageMenuVideo.setImageResource(R.drawable.ic_video);
            imageMenuSubject.setImageResource(R.drawable.ic_sub);
//            imageMenuArtist.setImageResource(R.drawable.ic_artist);
//            imageMenuNct.setImageResource(R.drawable.icnct);
            textMenuDownload.setTextColor(getResources().getColor(R.color.blue));
            imageMenuDownload.setImageResource(R.drawable.download_select);
        }
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }
    // -- END GET VISIBLE FRAGMENT IN SMSMainActivity --//

    // --ON BACK KEY PRESS--//
    Handler handlerExit = new Handler();

    @Override
    public void onBackPressed() {
        String strFragment = getVisibleFragment().getClass().getSimpleName();
        if (strFragment.equalsIgnoreCase(FragmentArtist.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentHome.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentNCT.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentPlaylist.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentRank.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentSearch.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentSong.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentSubject.class.getSimpleName())
                || strFragment.equalsIgnoreCase(FragmentVideo.class.getSimpleName())) {
            backCount++;
            if (backCount == 2) {
                finish();
            } else {
                handlerExit.postDelayed(runExit, 2500);
                showMessage(getString(R.string.main_activity_back_again_to_exit), context);
            }
        } else {
            super.onBackPressed();
        }
    }
}