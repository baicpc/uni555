package com.iii.fragmentNCT;

public class MusicItem {
	private String title;
	private String artist;
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	private String path;
	public MusicItem(){}
	public MusicItem(String title, String path){
		this.title = title;
		this.path = path;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

}
