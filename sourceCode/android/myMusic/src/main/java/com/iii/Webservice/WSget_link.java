package com.iii.Webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;

import org.json.JSONObject;

public class WSget_link extends AsyncTask<Void, Void, ResultAPI> {
	public static Handler HANDLER_SUCCESS;
	private ConfigurationWS mWS;
	private Context mContext;

	public WSget_link(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

	}

	@Override
	protected synchronized ResultAPI doInBackground(Void... params) {
		// TODO Auto-generated method stub
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URL_ROUTER + "router/getlink/?insecure=cool";
			String jsonData = mWS.getLink(URLAddClient, Constant.APP_NAME);
			JSONObject jsonObject = new JSONObject(jsonData);
			Log.e("result", jsonObject.toString());
			if (jsonObject != null) {
				result.setStatus(jsonObject.getString("response"));
				result.setMessage(jsonObject.getString("link_api"));
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}

	@Override
	protected void onPostExecute(ResultAPI result) {
		Constant.RESULT_API = result;
		HANDLER_SUCCESS.sendMessage(HANDLER_SUCCESS.obtainMessage());
	}
}
