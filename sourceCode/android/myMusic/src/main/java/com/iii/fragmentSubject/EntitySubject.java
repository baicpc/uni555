package com.iii.fragmentSubject;

import java.io.Serializable;

public class EntitySubject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String subname;
	private String descripton;
	private String image;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubname() {
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public EntitySubject() {
		// TODO Auto-generated constructor stub
	}
	public EntitySubject(int id,String name,String description,String image){
		this.id=id;
		this.subname=name;
		this.descripton=description;
		this.image=image;
		
	}

}
