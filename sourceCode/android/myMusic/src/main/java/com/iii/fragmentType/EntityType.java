package com.iii.fragmentType;

import java.io.Serializable;

public class EntityType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idtype;
	private String nametype;
	private boolean select;
	
	public EntityType() {
		super();
	}
	public int getIdtype() {
		return idtype;
	}
	public void setIdtype(int idtype) {
		this.idtype = idtype;
	}
	public String getNametype() {
		return nametype;
	}
	public void setNametype(String nametype) {
		this.nametype = nametype;
	}
	public EntityType(int id,String name){
		this.idtype=id;
		this.nametype=name;
		
	}
	public boolean isSelect() {
		return select;
	}
	public void setSelect(boolean select) {
		this.select = select;
	}
	

}
