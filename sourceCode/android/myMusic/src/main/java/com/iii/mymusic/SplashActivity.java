package com.iii.mymusic;

import com.crashlytics.android.Crashlytics;
import com.iii.Config.ConfigurationServer;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetVideobyidType;
import com.iii.Webservice.WSget_link;
import com.iii.Webservice.WSsong;
import com.iii.fragmentVideo.EntityVideo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Fabric.with(this, new Crashlytics());

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        try {
            UltilFunction.startAsyncTask2(new WSget_link(this));
            WSget_link.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        getLink(Constant.RESULT_API);
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

//		new Handler().postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
////				new WSsong(SplashActivity.this).execute();
//				startActivity(new Intent(SplashActivity.this, MainActivity.class));
//				Ultilities.hideProgress(SplashActivity.this);
//				finish();
//			}
//		}, Constant.SPLASH_TIME);
    }

    private void getLink(ResultAPI resultAPI) {
        if (resultAPI.getStatus().equalsIgnoreCase("ok")) {
            try {
                ConfigurationServer.URLServer = resultAPI.getMessage();
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                Ultilities.hideProgress(SplashActivity.this);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}
