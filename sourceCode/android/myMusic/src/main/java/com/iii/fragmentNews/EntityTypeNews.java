package com.iii.fragmentNews;

import java.io.Serializable;

public class EntityTypeNews implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String  idtype;
	private String nametype;
	private boolean select;

	public EntityTypeNews() {
		super();
	}
	public String getIdtype() {
		return idtype;
	}
	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}
	public String getNametype() {
		return nametype;
	}
	public void setNametype(String nametype) {
		this.nametype = nametype;
	}
	public EntityTypeNews(String id, String name){
		this.idtype=id;
		this.nametype=name;
		
	}
	public boolean isSelect() {
		return select;
	}
	public void setSelect(boolean select) {
		this.select = select;
	}
	

}
