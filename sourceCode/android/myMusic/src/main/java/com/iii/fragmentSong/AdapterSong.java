package com.iii.fragmentSong;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.iii.Account.UserAccount;
import com.iii.LoginSocial.FacebookSignFragment;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSLogin;
import com.iii.Webservice.WSRegister;
import com.iii.fragmentHome.FragmentHome;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;
import com.squareup.picasso.Picasso;

public class AdapterSong extends ArrayAdapter<EntitySong> {
    Activity context;
    ArrayList<EntitySong> arrSong = null;
    int rs_layout;
    int userid;
    PopupWindow loginPopup, registerPopup;


    public interface customButtonDownloadListener {
        public void onButtonDownloadClickListener(EntitySong song, int position);
    }

    public customButtonDownloadListener customDownloadListener;

    public void setCustomButtonDownloadListener(
            customButtonDownloadListener listener) {
        this.customDownloadListener = listener;
    }

    public interface customButtonShareListener {
        void onButtonShareClickListener(EntitySong song, int position);
    }

    public customButtonShareListener customShareListener;

    public void setCustomButtonShareListener(
            customButtonShareListener listener) {
        this.customShareListener = listener;
    }

    public interface customButtonAddListener {
        public void onButtonAddClickListener(EntitySong song, int position);
    }

    public customButtonAddListener customAddListener;

    public void setCustomButtonAddListener(
            customButtonAddListener listener) {
        this.customAddListener = listener;
    }

    public AdapterSong(Activity context, int resource,
                       ArrayList<EntitySong> arrSong) {
        super(context, resource, arrSong);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.rs_layout = resource;
        this.arrSong = arrSong;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(rs_layout, null);
            holder = new ViewHolder();
            holder.tvnameSong = (TextView) convertView
                    .findViewById(R.id.tvnameSong);
            holder.tvnameArtist = (TextView) convertView
                    .findViewById(R.id.tvnameArtist);
            holder.tvcounter = (TextView) convertView
                    .findViewById(R.id.tvcounterSong);
            holder.ivDownload = (ImageView) convertView
                    .findViewById(R.id.ivDownload);
            holder.ivAddplaylist = (ImageView) convertView
                    .findViewById(R.id.tvaddplist);
            holder.ivShare = (ImageView) convertView.findViewById(R.id.ivshare);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final EntitySong song = getItem(position);
        holder.tvnameSong.setText(song.getName());
        holder.tvnameArtist.setText(song.getNameart());
        holder.tvcounter.setText(String.valueOf(song.getCounter()));
        holder.ivDownload.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (customDownloadListener != null) {
                    customDownloadListener.onButtonDownloadClickListener(song,
                            position);
                    Log.d("POSITION", position + "");
                }
            }
        });
        holder.ivShare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (UltilFunction.isConnectingToInternet(context)) {
                    UserAccount userAcc = UltilFunction.getUserFromSharedPreference(context);
                    if (userAcc != null) {
                        if (customShareListener != null) {
                            customShareListener.onButtonShareClickListener(song,
                                    position);
                        }
                    } else {

                        showPopupWindowLogin();
                    }
                } else {
                    showMessage(context.getString(R.string.main_activity_no_internet), context);
                }
            }
        });
        holder.ivAddplaylist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (UltilFunction.isConnectingToInternet(context)) {
                    UserAccount userAcc = UltilFunction.getUserFromSharedPreference(context);
                    if (userAcc != null) {
                        userid = userAcc.getId();
                        popupShowAddplaylist(arrSong.get(position).getId(), userid);
                    } else {

                        showPopupWindowLogin();
                    }
                } else {
                    showMessage(context.getString(R.string.main_activity_no_internet), context);
                }
//				Ultilities.addSongToPlaylist(song, context);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        ImageView ivAddplaylist, ivShare, ivDownload;
        TextView tvnameSong, tvnameArtist, tvcounter;

    }

    public void popupShowAddplaylist(final int idsong, int userid) {
        try {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_playlist, null);
            final PopupWindow addplaylistPopup = new PopupWindow(context);
            addplaylistPopup.setContentView(layout);
            addplaylistPopup.setWidth(Constant.widthPixels / 2);
            addplaylistPopup.setHeight(Constant.heightPixels / 2);
            addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            addplaylistPopup.setFocusable(true);
            addplaylistPopup.update();

            final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(context, R.layout.custom_item_type,
                    arrType);
            Ultilities.getPlaylistUser(adapterType, arrType, context, userid);
            lvplaylist.setAdapter(adapterType);
            lvplaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    addplaylistPopup.dismiss();
                    EntityType type = (EntityType) parent.getItemAtPosition(position);
                    Ultilities.InsertSong(context, type.getIdtype(), idsong);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void showMessage(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    private void updateUserInfo() {
        UserAccount userAccount = UltilFunction.getUserFromSharedPreference(context);
        if (userAccount != null) {
            MainActivity.tvaccount.setVisibility(View.VISIBLE);
            MainActivity.tvaccount.setText(userAccount.getFirstName());
            if (!userAccount.getUrlPicture().equals(UserAccount.URL_PICTURE_NULL)) {
                Picasso.with(context).load(userAccount.getUrlPicture()).into(MainActivity.imgAccount);
            } else {
                MainActivity.imgAccount.setImageResource(R.drawable.avatar_user);
            }
        } else {
            MainActivity.tvaccount.setVisibility(View.GONE);
        }
    }

    private void showPopupWindowLogin() {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.login_popup, (ViewGroup) context.findViewById(R.id.popup_element));
            loginPopup = new PopupWindow(layout, Constant.widthPixels / 2, DrawerLayout.LayoutParams.WRAP_CONTENT);
            loginPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            loginPopup.setFocusable(true);
            loginPopup.update();
            layout.setAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_in));
            final EditText editUser = (EditText) layout.findViewById(R.id.edit_username);
            final EditText editPassword = (EditText) layout.findViewById(R.id.edit_password);
            Button imageAgree = (Button) layout.findViewById(R.id.image_agree);
            ImageView imageFacebook = (ImageView) layout.findViewById(R.id.image_facebook);
            ImageView imageGmail = (ImageView) layout.findViewById(R.id.image_gmail);
            Button btnCancel = (Button) layout.findViewById(R.id.button_cancel);
            Button btnRegister = (Button) layout.findViewById(R.id.text_register);
            imageAgree.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    login(editUser, editPassword);
                }
            });

            imageFacebook.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    loginPopup.dismiss();
                    UltilFunction.clearUserAccount(context);
                    transationToFragment(new FacebookSignFragment());

                }
            });
//			imageGmail.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					loginPopup.dismiss();
//					UltilFunction.clearUserAccount(context);
//					transationToFragment(new GmailSignInFragment());
//
//				}
//			});
            btnCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    if (loginPopup.isShowing())
                        loginPopup.dismiss();
                }
            });
            editPassword.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        Ultilities.hideKeyBoard(context, v);
                        loginPopup.dismiss();
                        login(editUser, editPassword);
                        return true;
                    }
                    return false;
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    loginPopup.dismiss();
                    showPopupWindowRegister();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void login(EditText editUser, EditText editPassword) {
        if (editUser.getText().toString().length() == 0 || editPassword.getText().toString().length() == 0) {
            showMessage(context.getString(R.string.fill_info_login), context);
        } else {
            new WSLogin(context, editUser.getText().toString().trim(),
                    editPassword.getText().toString().trim()).execute();
            Constant.HANDLER_PROGRESS_COMPLETE = new Handler(new Handler.Callback() {

                @Override
                public boolean handleMessage(Message msg) {
                    if (Constant.RESULT != null) {
                        if (Constant.RESULT.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
                            UltilFunction.clearUserAccount(context);
                            loginPopup.dismiss();
                            transationToFragment(new FragmentHome());
                            showMessage(context.getString(R.string.login_success), context);
                            UltilFunction.saveUser(Constant.RESULT.getRecord(), context);
                            updateUserInfo();
                        } else {
                            showMessage(context.getString(R.string.error_login_info), context);
                        }
                    } else {
                        showMessage(context.getString(R.string.error_login_internet), context);
                    }
                    return false;
                }
            });
        }
    }

    private void transationToFragment(Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void showPopupWindowRegister() {
        try {
            final String blockCharacterSet = "+×÷=%_€£¥₩!@#$/^&*()-'\":;,? ";
            final String blockCharacterSet2 = " ";

            InputFilter filter = new InputFilter() {

                @Override
                public CharSequence filter(CharSequence source, int arg1, int arg2,
                                           Spanned arg3, int arg4, int arg5) {
                    // TODO Auto-generated method stub
                    if (source != null && blockCharacterSet.contains(("" + source))) {
                        return "";
                    }
                    return null;
                }
            };

            InputFilter filter2 = new InputFilter() {

                @Override
                public CharSequence filter(CharSequence source, int arg1, int arg2,
                                           Spanned arg3, int arg4, int arg5) {
                    // TODO Auto-generated method stub
                    if (source != null && blockCharacterSet2.contains(("" + source))) {
                        return "";
                    }
                    return null;
                }
            };

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.register_popup, (ViewGroup) context.findViewById(R.id.popup_element));
            registerPopup = new PopupWindow(layout, Constant.widthPixels / 2, DrawerLayout.LayoutParams.WRAP_CONTENT);
            registerPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            registerPopup.setFocusable(true);
            registerPopup.update();
            layout.setAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_in));
            final EditText editUser = (EditText) layout.findViewById(R.id.edit_username);
            editUser.setFilters(new InputFilter[]{filter});
            final EditText editEmail = (EditText) layout.findViewById(R.id.edit_email);
            final EditText editPassword = (EditText) layout.findViewById(R.id.edit_password);
            editPassword.setFilters(new InputFilter[]{filter2});
            final EditText editPasswordAgain = (EditText) layout.findViewById(R.id.edit_password_again);
            editPasswordAgain.setFilters(new InputFilter[]{filter2});
            Button buttonAgree = (Button) layout.findViewById(R.id.image_agree);
            Button buttonCancel = (Button) layout.findViewById(R.id.button_cancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    registerPopup.dismiss();
                }
            });
            buttonAgree.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Ultilities.hideKeyBoard(context, v);
                    String userName = editUser.getText().toString();
                    String email = editEmail.getText().toString();
                    String password = editPassword.getText().toString();
                    String passwordAgain = editPasswordAgain.getText().toString();
                    if (userName.length() == 0 || email.length() == 0 || password.length() == 0
                            || passwordAgain.length() == 0 || !password.equals(passwordAgain)) {
                        showMessage(context.getString(R.string.register_popup_fill_info), context);
                    } else if (!UltilFunction.isEmailValid(email)) {
                        showMessage(context.getString(R.string.format_not_email), context);
                    } else {
                        register(userName, password, email, registerPopup);
                    }
                }
            });

            editPassword.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        Ultilities.hideKeyBoard(context, v);
                        loginPopup.dismiss();
                        login(editUser, editPassword);
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void register(String username, String password, String email, final PopupWindow registerPopup) {
        new WSRegister(context, username, email, password).execute();
        Constant.HANDLER_PROGRESS_COMPLETE = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                if (Constant.RESULT != null) {
                    if (Constant.RESULT.getCode().equalsIgnoreCase("200")) {
                        registerPopup.dismiss();
                        showMessage(context.getString(R.string.register_success), context);
                    } else {
                        showMessage(context.getString(R.string.register_error_account), context);
                    }
                } else {
                    showMessage(context.getString(R.string.register_error), context);
                }
                return false;
            }
        });
    }
}
