package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class WSLoginSocial extends AsyncTask<Void, Void, Void> {
	private static final String TAG = WSLoginSocial.class.getSimpleName();
	public static Handler HANDLER_SUCCESS;
	Context context;
	private String email = "";
	private String firstname = "";
	private String lastname = "";

	public WSLoginSocial(Context context, String email, String firstname, String lastname) {
		this.context = context;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	@Override
	protected void onPreExecute() {
		Ultilities.showProgress(context);
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		try {
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/registerAndLogin";
			JSONObject json = new JSONObject();
			json.put("email", this.email);
			json.put("firstname", this.firstname);
			json.put("lastname", this.lastname);

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = ConfigurationWS.getInstance(context).getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setRecord(jsonObject.getString("records"));
				Constant.RESULT = result;
				Log.d(TAG, "RESULT NOT NULL");
				Constant.RESULT_API = result;
			}
		} catch (Exception e) {
			Log.e(TAG, "ERROR: " + e.getMessage());
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		Ultilities.hideProgress(context);
		try {
			HANDLER_SUCCESS.sendMessage(HANDLER_SUCCESS.obtainMessage());
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}

}
