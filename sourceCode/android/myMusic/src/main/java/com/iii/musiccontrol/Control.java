package com.iii.musiccontrol;

import java.util.Random;

import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;
import com.iii.fragmentHome.FragmentHome;
import com.iii.musicservice.MusicService;
import com.iii.mymusic.R;

import android.content.Context;
import android.util.Log;

public class Control {
	public static final String NEXT = "next";
	public static final String PREVIOUS = "previous";

	public static void playControl(Context context) {
		sendMessage(context.getResources().getString(R.string.play));
		Constant.SONG_PAUSE = false;
	}

	public static void pauseControl(Context context) {
		sendMessage(context.getResources().getString(R.string.pause));
		Constant.SONG_PAUSE = true;
	}

	public static void repeatOneSongControl(Context context) {
		try {
			Constant.HANDLER_SONG_CHANGE.sendMessage(Constant.HANDLER_SONG_CHANGE.obtainMessage());
		} catch (NullPointerException e) {
			// TODO: handle exception
			Log.e("repeatOneSongControl", e.toString());
		}
	}

	public static void nextControl(Context context) {
		boolean isServiceRunning = Ultilities.isServiceRunning(MusicService.class.getName(), context);
		if (!isServiceRunning)
			return;
		if (Constant.SONG_LIST.size() > 1) {
			if (Constant.REPEAT_SONG.equals("RANDOM")) {
				int newSong = Constant.SONG_INDEX;
				while (newSong == Constant.SONG_INDEX) {
					Random rand = new Random();
					newSong = rand.nextInt(Constant.SONG_LIST.size());
				}
				Constant.SONG_INDEX = newSong;
				songChange();
			} else if (Constant.SONG_INDEX < Constant.SONG_LIST.size() - 1) {
				Constant.SONG_INDEX++;
				songChange();
			} else {
				Constant.SONG_INDEX = 0;
				songChange();
			}
			Constant.SONG_PAUSE = false;
		}
	}

	public static void previousControl(Context context) {
		boolean isServiceRunning = Ultilities.isServiceRunning(MusicService.class.getName(), context);
		if (!isServiceRunning)
			return;
		if (Constant.SONG_LIST.size() > 1) {
			if (Constant.REPEAT_SONG.equals("RANDOM")) {
				int newSong = Constant.SONG_INDEX;
				while (newSong == Constant.SONG_INDEX) {
					Random rand = new Random();
					newSong = rand.nextInt(Constant.SONG_LIST.size());
				}
				Constant.SONG_INDEX = newSong;
				songChange();
			} else if (Constant.SONG_INDEX > 0) {
				Constant.SONG_INDEX--;
				songChange();
			} else {
				Constant.SONG_INDEX = Constant.SONG_LIST.size() - 1;
				songChange();
			}
			Constant.SONG_PAUSE = false;
		}
	}

	private static void sendMessage(String message) {
		try {
			Constant.HANDLER_PLAY_PAUSE.sendMessage(Constant.HANDLER_PLAY_PAUSE.obtainMessage(0, message));
		} catch (NullPointerException e) {

			Log.e("sendMessage", e.toString());
		}
	}
	public static void songChange(){
		try {
			Constant.HANDLER_SONG_CHANGE.sendMessage(Constant.HANDLER_SONG_CHANGE.obtainMessage());
		} catch (NullPointerException e) {
			// TODO: handle exception

			Log.e("songChange", e.toString());
		}
	}
	
}
