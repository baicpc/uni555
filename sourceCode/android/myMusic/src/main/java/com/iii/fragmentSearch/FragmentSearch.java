package com.iii.fragmentSearch;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSSearch;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.fragmentVideo.FragmentVideo;
import com.iii.musiccontrol.Control;
import com.iii.musicservice.MusicService;
import com.iii.mymusic.R;
import com.iii.playVideo.ActivityPlayvideo;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentSearch extends Fragment {
	View view;
	ArrayList<SearchEntity> arrSearch;
	SearchAdapter adapterSearch = null;
	SearchEntity search;
	ListView lvSearch;
	EditText tvSearch;
	ImageView ivDelsearch, btnSearch;
	TextView textSearchEmpty;
	TextView tvcounterSong, tvcounterPlaylist, tvcounterVideo;

	private static FragmentSearch fragment;

	public static FragmentSearch getInstants() {
		if (fragment == null)
			fragment = new FragmentSearch();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_search, container, false);
		lvSearch = (ListView) view.findViewById(R.id.lv_search);
		ivDelsearch = (ImageView) view.findViewById(R.id.ivDelsearch);
		tvcounterSong = (TextView) view.findViewById(R.id.tvcounterSong);
		tvcounterPlaylist = (TextView) view
				.findViewById(R.id.tv_counter_playlist);
		tvcounterVideo = (TextView) view.findViewById(R.id.tvcountervideo);
		textSearchEmpty = (TextView) view.findViewById(R.id.text_empty);
		ivDelsearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tvSearch.setText("");
				ivDelsearch.setVisibility(View.GONE);
				btnSearch.setVisibility(View.GONE);
				hideKeyBoard();
			}
		});
		lvSearch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// TODO Auto-generated method stub
				Constant.SONG_INDEX = 0;
				Constant.CURR_POSITION = 0;
				Ultilities.stopMusicService(getActivity());
				search = (SearchEntity) parent.getItemAtPosition(position);
				if (search.getType() == SearchEntity.TYPE_SONG) {
					ArrayList<EntitySong> lists = new ArrayList<EntitySong>();
					EntitySong song = new EntitySong();
					song.setName(search.getTitle());
					String linksong = search.getUrl();
					if (!search.getFilepath().equals("null")) {

						linksong = search.getFilepath().replaceAll(" ", "%20");
					}
					else {

						linksong = search.getDriver();
					}

					song.setLinksong(linksong);
					Ultilities.setOneSongListMusic(song);
					// MainActivity.changeUI();
					Ultilities.UpdateSong(getActivity(), search.getId(), null,
							null, null, position);
					UltilFunction.updateTextListTitle(getString(R.string.nct));

				} else if (search.getType() == SearchEntity.TYPE_ARTIST) {
					fragmentSongList fragsonglist = new fragmentSongList();
					Bundle bundle = new Bundle();
					bundle.putInt("cusor", 4);
					bundle.putString("nameart", search.getTitle());
					bundle.putInt("idartist", search.getId());
					bundle.putString("imgart", search.getImage());
					fragsonglist.setArguments(bundle);
					transationToFragment(fragsonglist);
				} else if (search.getType() == SearchEntity.TYPE_VIDEO) {
					Ultilities.UpdateVideo(getActivity(), search.getId(), null,
							null, position);
					FragmentVideo.HANDLER_UPDATE_COUNTER = new Handler() {
						public void handleMessage(Message msg) {
							Intent myIntent = new Intent(getActivity(),
									ActivityPlayvideo.class);
							Bundle bundle = new Bundle();
							String linkurl = search.getUrl();
							if (linkurl.equals("")) {
								linkurl = search.getFilepath().replaceAll(" ", "%20");
								if (linkurl.equals("null")) {

									linkurl = search.getDriver();
								}
								bundle.putString("linkvideo", linkurl);
							}
							bundle.putString("linkvideo", linkurl);
							bundle.putString("name", search.getTitle());
							myIntent.putExtra("video", bundle);
							try {
								if (Ultilities.isServiceRunning(
										MusicService.class.getName(),
										getActivity())) {
								}
								else
									Log.e("play_video", "no k vao day");
							} catch (NullPointerException e) {
								// TODO: handle exception
								Log.e("play_video", e.toString());
							}
							startActivity(myIntent);
						};
					};
				}

			}
		});
		tvSearch = (EditText) view.findViewById(R.id.etSearch);
		tvSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (tvSearch.length() > 0) {
					if (tvSearch.getText().toString().substring(0).equals(" ")) {
						tvSearch.setText("");
					}
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		btnSearch = (ImageView) view.findViewById(R.id.btnSearch);
		arrSearch = new ArrayList<SearchEntity>();
		adapterSearch = new SearchAdapter(getActivity(), arrSearch);
		lvSearch.setAdapter(adapterSearch);
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (arrSearch.size() > 0) {
					arrSearch.clear();
				}
				String key = tvSearch.getText().toString();
				getSearch(key);
				hideKeyBoard();
			}
		});
		tvSearch.setImeActionLabel("Find", KeyEvent.KEYCODE_ENTER);
		tvSearch.setOnKeyListener(onkeyClick);
		tvSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() <= 0) {
					ivDelsearch.setVisibility(View.GONE);
					btnSearch.setVisibility(View.GONE);
				} else {
					ivDelsearch.setVisibility(View.VISIBLE);
					btnSearch.setVisibility(View.VISIBLE);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		ivDelsearch.setVisibility(View.GONE);
		btnSearch.setVisibility(View.GONE);
		if (!UltilFunction.isConnectingToInternet(getActivity())) {
			Toast.makeText(getActivity(),
					getString(R.string.main_activity_no_internet),
					Toast.LENGTH_LONG).show();
		}
		return view;
	}

	public void getSearch(String key) {
		try {
			UltilFunction.startAsyncTask2(new WSSearch(getActivity(), key));
			WSSearch.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						addToListSong(Constant.RESULT_API);
						addToListVideo(Constant.RESULT_API);
						addToListArt(Constant.RESULT_API);
						if (arrSearch.size() <= 0) {
							textSearchEmpty
									.setText(getString(R.string.fragment_search_not_found)
											+ " "
											+ "\'"
											+ tvSearch.getText().toString()
											+ "\'");
							textSearchEmpty.setVisibility(View.VISIBLE);

						} else {
							textSearchEmpty.setVisibility(View.INVISIBLE);
						}
					}
				}
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void addToListSong(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			try {
				JSONArray jsonArray = new JSONArray(resultAPI.getSong());
				Log.d("JSON Array: ", jsonArray.toString());
				for (int j = 0; j < jsonArray.length(); j++) {
					JSONObject jsonObject = jsonArray.getJSONObject(j);
					int idsong = Integer.parseInt(jsonObject.opt("idsong")
							.toString());
					String namesong = jsonObject.optString("namesong")
							.toString();
					String linkmp3 = jsonObject.opt("linksong").toString();
					String image = jsonObject.opt("image").toString();
					String filepath = jsonObject.opt("filepath").toString();
					String driver = jsonObject.opt("linkdrive").toString();
					search = new SearchEntity(idsong, namesong, linkmp3,
							filepath, image, SearchEntity.TYPE_SONG, driver);
					arrSearch.add(search);
					adapterSearch.notifyDataSetChanged();
				}
			} catch (Exception e) {
				e.toString();
			}
		}

		// Log.d("LIST SIZE: ", arrSearch.size() + "");
	}

	public void addToListVideo(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			// String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				// JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = new JSONArray(resultAPI.getVideo());
				Log.d("JSON Array: ", jsonArray.toString());
				for (int j = 0; j < jsonArray.length(); j++) {
					JSONObject jsonObject = jsonArray.getJSONObject(j);
					int idvideo = Integer.parseInt(jsonObject.opt("idvideo")
							.toString());
					String namevideo = jsonObject.optString("namevideo")
							.toString();
					String linkvideo = jsonObject.opt("linkurl").toString();
					String image = jsonObject.opt("image").toString();
					String filepath = jsonObject.opt("filepath").toString();
					String driver = jsonObject.opt("linkdrive").toString();
					search = new SearchEntity(idvideo, namevideo, linkvideo,
							filepath, image, SearchEntity.TYPE_VIDEO, driver);
					arrSearch.add(search);
				}
				adapterSearch.notifyDataSetChanged();
			} catch (Exception e) {
				e.toString();
			}
		}
	}

	public void addToListArt(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			// String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				// JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = new JSONArray(resultAPI.getArtist());
				Log.d("JSON Array: ", jsonArray.toString());
				for (int j = 0; j < jsonArray.length(); j++) {
					JSONObject jsonObject = jsonArray.getJSONObject(j);
					int idart = Integer.parseInt(jsonObject.opt("idartist")
							.toString());
					String artname = jsonObject.optString("artistname")
							.toString();
					search = new SearchEntity(idart, artname,
							SearchEntity.TYPE_ARTIST);
					search.setImage(jsonObject.optString("image").toString());
					arrSearch.add(search);
				}
				adapterSearch.notifyDataSetChanged();
			} catch (Exception e) {
				e.toString();
			}
		}
	}

	OnKeyListener onkeyClick = new OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN
					&& keyCode == KeyEvent.KEYCODE_ENTER) {
				if (arrSearch.size() > 0) {
					arrSearch.clear();
				}
				String key = tvSearch.getText().toString();
				getSearch(key);
				return true;
			}
			return false;
		}
	};

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment
				.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

	public void hideKeyBoard() {
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(getActivity().INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}
}