package com.iii.fragmentVideo;

import java.io.Serializable;

public class EntityVideo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String nameart;
	private String linkurl;	
	private String image;
	private int counter;
	private int idartist;
	private int idsubject;
	private int idmedia;
	String filepath;
	String filename;
	String filetype;
	String driver;
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getIdartist() {
		return idartist;
	}
	public void setIdartist(int idartist) {
		this.idartist = idartist;
	}
	public int getIdsubject() {
		return idsubject;
	}
	public void setIdsubject(int idsubject) {
		this.idsubject = idsubject;
	}
	public int getIdmedia() {
		return idmedia;
	}
	public void setIdmedia(int idmedia) {
		this.idmedia = idmedia;
	}
	
	public String getNameart() {
		return nameart;
	}
	public void setNameart(String nameart) {
		this.nameart = nameart;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public EntityVideo(){
		
	}
	public EntityVideo(int id, String name, String linkurl,String image,int counter,String nameart,String filepath, String driver){
		this.id=id;
		this.name=name;
		this.linkurl=linkurl;
		this.image=image;
		this.counter=counter;
		this.nameart=nameart;
		this.filepath=filepath;
		this.driver = driver;
	}

}
