package com.iii.fragmentPlaylist;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import com.facebook.Session;
import com.iii.Account.UserAccount;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.RoundedTransformation;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WScreatePlaylist;
import com.iii.Webservice.WSgetPlaylistbyUserId;
import com.iii.fragmentNCT.FragmentNCT;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;
import com.iii.mymusic.SplashActivity;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentAccountPlaylist extends Fragment {
	View view;
	TextView tvAcc;
	Button btnCreatePlaylist;
	ImageView btnSetup, btnback;
	LinearLayout layoutLike;

	TwoWayView gvPlaylist;
	EntityPlaylist list;
	int userid;
	ArrayList<EntityPlaylist> arrPlaylists = null;
	
	public FragmentAccountPlaylist(){
		arrPlaylists = new ArrayList<EntityPlaylist>();
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragmentplaylist_login, container,
				false);
		btnSetup = (ImageView) view.findViewById(R.id.btnSetup);
		btnCreatePlaylist = (Button) view
				.findViewById(R.id.button_create_playlist);
		gvPlaylist = (TwoWayView) view.findViewById(R.id.grvPlaylist_login);
		layoutLike = (LinearLayout) view.findViewById(R.id.layout_like);
		btnback = (ImageView) view.findViewById(R.id.button_back);
		layoutLike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentPlaylistFavorite frag = new FragmentPlaylistFavorite();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 2);
				frag.setArguments(bundle);
				transationToFragment(frag);
				;
			}
		});
		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});
		UserAccount user = UltilFunction
				.getUserFromSharedPreference(getActivity());
		if (user != null) {
			userid = user.getId();
			getPlaylist(userid);
			ImageView imageAvatar = (ImageView) view
					.findViewById(R.id.image_avatar);
			TextView textName = (TextView) view.findViewById(R.id.text_name);
			TextView textEmail = (TextView) view.findViewById(R.id.text_email);
			Picasso.with(getActivity()).load(user.getUrlPicture())
					.placeholder(R.drawable.avatar_user)
					.error(R.drawable.avatar_user)
					.transform(new RoundedTransformation(14, 0))
					.into(imageAvatar);
			String firstName = user.getFirstName();
			if (firstName.equals("null")) {
				firstName = "";
			}
			String lastName = user.getLastName();
			if (lastName.equals("null")) {
				lastName = "";
			}
			textName.setText(firstName + " " + lastName);
			textEmail.setText(user.getEmail());
		}
			gvPlaylist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					list = (EntityPlaylist) parent.getItemAtPosition(position);
					fragmentSongList fragsonglist = new fragmentSongList();
					Bundle bundle = new Bundle();
					bundle.putInt("cusor", 7);
					bundle.putInt("idplaylist", list.getId());
					bundle.putString("name", list.getName());
					bundle.putString("image", list.getImage());
					bundle.putInt("counter", list.getCounter());
					fragsonglist.setArguments(bundle);
					transationToFragment(fragsonglist);

				}
			});
		
		btnCreatePlaylist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (UltilFunction.isConnectingToInternet(getActivity())) {
					showPopupCreatePlaylist(v);
				} else {
					showMessage(getString(R.string.main_activity_no_internet),
							getActivity());
				}
			}
		});
		btnSetup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showPopup(v);
				popupLogout();
			}
		});
		return view;
	}

	public void getPlaylist(int userid) {
		try {
			UltilFunction.startAsyncTask2(new WSgetPlaylistbyUserId(view
					.getContext(), userid));
			WSgetPlaylistbyUserId.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						addPlaylist(Constant.RESULT_API);
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void addPlaylist(ResultAPI resultAPI) {
		arrPlaylists = new ArrayList<EntityPlaylist>();
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = jsonRootObject.optJSONArray("records");
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					int idplaylist = Integer.parseInt(jsonObject.opt(
							"idplaylist").toString());
					String nameplaylist = jsonObject.opt("playlist").toString();
					list = new EntityPlaylist(idplaylist, nameplaylist,
							EntityPlaylist.TYPE_MY);
					arrPlaylists.add(list);
					AdapterPlaylist adapterPlaylist = new AdapterPlaylist(
							getActivity(), R.layout.custom_playlist,
							arrPlaylists);
					gvPlaylist.setAdapter(adapterPlaylist);
				}
			} catch (Exception e) {
				Log.d("Error", e.toString());
			}
		}
	}

	// private void showPopup(View v) {
	//
	// LayoutInflater inflater = (LayoutInflater)
	// getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// View layout = inflater.inflate(R.layout.popup_setup, (ViewGroup)
	// v.findViewById(R.id.popup_element));
	// final PopupWindow popup = new PopupWindow(getActivity());
	// popup.setWidth(Constant.widthPixels / 8);
	// popup.setHeight(LayoutParams.WRAP_CONTENT);
	// popup.setFocusable(true);
	// popup.setContentView(layout);
	// int location[] = new int[2];
	// v.getLocationOnScreen(location);
	// popup.showAtLocation(v, Gravity.NO_GRAVITY, Constant.widthPixels -
	// popup.getWidth() - 2,
	// location[1] + v.getHeight());
	//
	// final Button tvLogout = (Button) layout.findViewById(R.id.tvLogout);
	// tvLogout.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	//
	// }
	// });
	//
	// }

	private void showPopupCreatePlaylist(View v) {

		LayoutInflater inflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.popup_createplaylist,
				null);
		final PopupWindow addplaylistPopup = new PopupWindow(getActivity());
		addplaylistPopup.setContentView(layout);
		addplaylistPopup.setWidth(Constant.widthPixels / 2);
		addplaylistPopup.setHeight(Constant.heightPixels / 2);
		addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
		addplaylistPopup.setFocusable(true);
		addplaylistPopup.update();

		final EditText etCreatePL = (EditText) layout
				.findViewById(R.id.etCreatePlaylist);
		final Button btnOK = (Button) layout.findViewById(R.id.btnOK_playlist);
		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean exists = false;
				String playlist = etCreatePL.getText().toString();
				for (int i = 0; i < arrPlaylists.size(); i++) {
					if (arrPlaylists.get(i).getName().equals(playlist)) {
						exists = true;
						break;
					}
				}
				if (exists) {
					Toast.makeText(
							getActivity(),
							"\'"
									+ playlist
									+ "\'"
									+ " "
									+ getString(R.string.popup_create_playlist_exists),
							Toast.LENGTH_LONG).show();
					return;
				}
				
				if (etCreatePL.getText().toString().replaceAll(" ", "").equals("")) {

					Toast.makeText(getActivity(), getString(R.string.popup_create_playlist_error), Toast.LENGTH_SHORT).show();
					return;
				}
				addplaylistPopup.dismiss();
				CreatePlaylist(playlist);
			}
		});
	}

	public void CreatePlaylist(String namePL) {
		try {
			UltilFunction.startAsyncTask2(new WScreatePlaylist(getActivity(),
					namePL, userid));
			WScreatePlaylist.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.isSuccess()) {
							getPlaylist(userid);
							Toast.makeText(
									getActivity(),
									getString(R.string.popup_create_playlist_success),
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(
									getActivity(),
									getString(R.string.popup_create_playlist_error),
									Toast.LENGTH_SHORT).show();
						}
					}
				};
			};

		} catch (Exception e) {
			// TODO: handle exception
			e.toString();
		}
	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment
				.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

	private void signOutGplus() {
		Constant.mGoogleApiClient = null;
	}

	private void signOutFacebook() throws NullPointerException {
		Session session = Session.getActiveSession();
		session.closeAndClearTokenInformation();
	}

	private void showMessage(String messge, Context context) {
		Toast.makeText(context, messge, Toast.LENGTH_LONG).show();
	}

	private void signOut() {
		if (UltilFunction.isConnectingToInternet(getActivity())) {
			UltilFunction.clearUserAccount(getActivity());
			// signOutGplus();
			// signOutFacebook();
			Intent intent = new Intent(getActivity(), SplashActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			getActivity().finish();
		} else {
			showMessage(getString(R.string.main_activity_no_internet),
					getActivity());
		}
	}

	public void popupLogout() {
		try {

			LayoutInflater inflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View layout = inflater.inflate(R.layout.popup_logout, null);
			final PopupWindow deletePopup = new PopupWindow(getActivity());
			deletePopup.setContentView(layout);
			deletePopup.setWidth(Constant.widthPixels / 2);
			deletePopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
			deletePopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
			deletePopup.setFocusable(true);
			deletePopup.update();

			final Button buttonCancel = (Button) layout
					.findViewById(R.id.button_cancel);
			final Button buttonOk = (Button) layout
					.findViewById(R.id.button_ok);

			buttonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					deletePopup.dismiss();
				}
			});
			buttonOk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					deletePopup.dismiss();
					signOut();
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
