package com.iii.Receiver;

import com.iii.Ultils.Ultilities;
import com.iii.musicservice.MusicService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StopMusicServiceReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if(intent.getAction().equals(MusicService.SONG_PLAY_COMPLETE)){
			if(Ultilities.isServiceRunning(MusicService.class.getName(), context)){
				context.stopService(new Intent(context, MusicService.class));
			}
		}
	}

}
