package com.iii.Ultils;

import com.google.android.gms.common.api.GoogleApiClient;
import java.util.ArrayList;

import com.iii.Modle.ResultAPI;
import com.iii.fragmentSong.EntitySong;

import android.app.ProgressDialog;
import android.graphics.Point;
import android.os.Handler;

public class Constant {

    public static String url;
    public static final String APP_NAME = "uni555";
	public static ResultAPI RESULT_API;
	public static ResultAPI RESULT_API_STORY;
	public static ResultAPI RESULT_API_FILM;
	public static ResultAPI RESULT_API_BOOK;
	public static ResultAPI RESULT_API_NEWS;
    public static ResultAPI RESULT_API_SONG;
	public static Handler HANDLER_SEEK_TO_ZERO;
	public static String REPEAT_SONG="NONE";
//	public static String URL_IMG = "http://api.uni555.xyz/web/app_dev.php/music/public/media/";
	public static String URL_IMG = "http://uni555.xyz/public/media/";
	//public static String URL_FILE = "http://117.6.131.222:8888/music/files/";
    public static final String RESULT_API_SUCCESS = "200";
    public static final String RESULT_API_FAIL = "500";
    public static int heightPixels;
    public static int widthPixels;
    public static boolean isPlaying = false;
    public static Handler HANDLER_DOWN;
    public static ProgressDialog progressDialog;
    public static Handler HANDLER_PROGRESS_COMPLETE;
    public static ResultAPI RESULT;
    public static ArrayList<EntitySong> LIST_SEARCH = new ArrayList<EntitySong>();
    public static boolean IS_LISTEN_ONLINE=true;
    public static GoogleApiClient mGoogleApiClient;
    //Shared Preference user login
    public static final String LOGIN_USER = "user";  // Shared Setting name
    
    //Get infomation user from Shared Preference
    public static final String USER_ID = "id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email";
    public static final String URL_PICTURE = "url";
    public static final String COUNTRY = "COUNTRY";
    public static final String FACEBOOK_ID = "facebook_id";
    public static final String GOOGLE_ID = "google_id";
    public static final String GOOGLE_IMG = "google_name";
    public static	Point p;
    
    public static ProgressDialog PROGRESS_DIALOG;
    
    public static long SPLASH_TIME = 2000;
    
    //--PLAY MUSIC--//
    public static Handler HANDLER_SONG_CHANGE;
    public static int SONG_INDEX = 0;
    public static ArrayList<EntitySong> SONG_LIST = new ArrayList<>();
    public static boolean SONG_PAUSE;
    public static Handler HANDLER_PLAY_PAUSE;
    public static Handler HANDLER_PROGRESSBAR;
    public static Handler HANDLER_SEEK;
    public static Handler HANDLER_UPDATE_COMPLETE;
    public static Handler HANDLER_MEDIA_ERROR;
    public static int CURR_POSITION = 0;
    
    
    //--APP SETTING--//
    public static final String MY_SETTING = "app_setting";
    public static final String KEY_APP_FIRST = "is_first";
    public static final String KEY_REPEAT_MODE = "repeat";
    public static final String KEY_SONG_PAUSE = "song_pause";
    
    public static final String USER_NOT_LOGIN = "xaxaxaxax01201229L";
    
    public static boolean IS_MUSIC_LOCAL;
    
    public static String TITLE_LIST_IS_PLAYING;
    public static boolean isContinue = false;
    
}

