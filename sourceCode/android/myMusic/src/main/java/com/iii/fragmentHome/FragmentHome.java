package com.iii.fragmentHome;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.iii.Config.ConfigurationServer;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Webservice.WSgetRankBook;
import com.iii.Webservice.WSgetRanNews;
import com.iii.Webservice.WSgetRankFilm;
import com.iii.Webservice.WSgetRankStory;
import com.iii.fragmentSong.DownloadSongMultiFileAsync;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetRank;
import com.iii.fragmentPlaylist.AdapterPlaylist;
import com.iii.fragmentPlaylist.EntityPlaylist;
import com.iii.fragmentSong.AdapterSong;
import com.iii.fragmentSong.AdapterSong.customButtonAddListener;
import com.iii.fragmentSong.AdapterSong.customButtonDownloadListener;
import com.iii.fragmentSong.AdapterSong.customButtonShareListener;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.fragmentVideo.AdapterVideo;
import com.iii.fragmentVideo.EntityVideo;
import com.iii.fragmentVideo.FragmentVideo;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;
import com.iii.playVideo.ActivityPlayvideo;

public class FragmentHome extends Fragment implements
        customButtonDownloadListener, customButtonShareListener,
        customButtonAddListener {
    TwoWayView lvPlaylist, lvVideo, GalerrySong, lvSong, lvNews_main;
    //    ListView lvSong;
    View view;
    EntityPlaylist list;
    AdapterPlaylist AdapterPlaylist = null;
    ArrayList<EntityPlaylist> arrPlaylists = null;
    EntitySong song;
    AdapterSong adapterSong = null;
    ArrayList arrSong ,arrStory,arrBook,arrNews;
    ArrayList<EntitySong> arrSongAll;
    EntityVideo video;
    AdapterVideo adapterVideo;
    ArrayList<EntityVideo> arrVideo;
    AdapterHome adapterhome;
    AdapterHome adapterStory;
    AdapterHome adapterBook;
    AdapterHome adapterNews;
    ScrollView sv;
    TextView tvcounterSong, tvcounterPlaylist, tvcounterVideo;
    int userid;

    private static FragmentHome fragment;

    public static FragmentHome getInstants() {
        if (fragment == null)
            fragment = new FragmentHome();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreference = getActivity().getSharedPreferences(Constant.LOGIN_USER, Context.MODE_PRIVATE);
        userid = sharedPreference.getInt(Constant.USER_ID, 0);
        view = inflater.inflate(R.layout.fragment_main, container, false);
        tvcounterSong = (TextView) view.findViewById(R.id.tvcounterSong);
        tvcounterPlaylist = (TextView) view.findViewById(R.id.tv_counter_playlist);
        tvcounterVideo = (TextView) view.findViewById(R.id.tvcountervideo);
        lvPlaylist = (TwoWayView) view.findViewById(R.id.lvPlaylist_main);
        lvVideo = (TwoWayView) view.findViewById(R.id.lvVideo_main);
        lvSong = (TwoWayView) view.findViewById(R.id.lvSong_main);
        lvNews_main = (TwoWayView) view.findViewById(R.id.lvNews_main);
        GalerrySong = (TwoWayView) view.findViewById(R.id.galleryMain);
        arrPlaylists = new ArrayList<>();
        arrSong = new ArrayList<>();
        arrBook = new ArrayList<>();
        arrNews = new ArrayList<>();
        arrStory = new ArrayList<>();
        arrSongAll = new ArrayList<>();
        arrVideo = new ArrayList<>();
        AdapterPlaylist = new AdapterPlaylist(getActivity(), R.layout.custom_item_playlist, arrPlaylists);
        adapterSong = new AdapterSong(getActivity(), R.layout.custom_item_home, arrSongAll);
        adapterSong.setCustomButtonDownloadListener(FragmentHome.this);
        adapterSong.setCustomButtonAddListener(FragmentHome.this);
        adapterSong.setCustomButtonShareListener(FragmentHome.this);
        adapterVideo = new AdapterVideo(getActivity(), R.layout.custom_item_video_home, arrVideo);
        adapterhome = new AdapterHome(getActivity(), R.layout.custom_item_songhome, arrSong);
        adapterStory = new AdapterHome(getActivity(), R.layout.custom_item_songhome, arrStory);
        adapterBook = new AdapterHome(getActivity(), R.layout.custom_item_songhome, arrBook);
        adapterNews = new AdapterHome(getActivity(), R.layout.custom_item_songhome, arrNews);
//        lvPlaylist.setAdapter(AdapterPlaylist);
        sv = (ScrollView) view.findViewById(R.id.scroll);
        lvVideo.setAdapter(adapterVideo);
        GalerrySong.setAdapter(adapterStory);
        GalerrySong.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListStory(arrStory);
                song = (EntitySong) parent.getItemAtPosition(position);
                // Ultilities.setOneSongListMusic(arrSong.get(position));
                Ultilities.UpdateeStory(getActivity(), song.getId(), adapterStory, arrStory, position);
                UltilFunction.updateTextListTitle(getString(R.string.nct));
            }
        });

        ////////////////////////////
        lvSong.setAdapter(adapterhome);
        lvNews_main.setAdapter(adapterNews);
        lvPlaylist.setAdapter(adapterBook);
        lvPlaylist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListAudiobook(arrBook);
                song = (EntitySong) parent.getItemAtPosition(position);
                // Ultilities.setOneSongListMusic(arrSong.get(position));
                Ultilities.UpdateAudioBook(getActivity(), song.getId(), adapterBook,arrBook, position);
                UltilFunction.updateTextListTitle(getString(R.string.nct));
            }
        });
        lvSong.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListMusic(arrSong);
                song = (EntitySong) parent.getItemAtPosition(position);
                // Ultilities.setOneSongListMusic(arrSong.get(position));
                Ultilities.UpdateSong(getActivity(), song.getId(), adapterSong, adapterhome, arrSongAll, position);
                UltilFunction.updateTextListTitle(getString(R.string.nct));
            }
        });
        lvNews_main.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListNews(arrNews);
                song = (EntitySong) parent.getItemAtPosition(position);
                // Ultilities.setOneSongListMusic(arrSong.get(position));
                Ultilities.UpdateNews(getActivity(), song.getId(), null,adapterNews, arrNews, position);
                UltilFunction.updateTextListTitle(getString(R.string.nct));
            }
        });


        //////////////////////////////////
        // put link video
        lvVideo.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                // TODO Auto-generated method stub
                Ultilities.stopMusicService(getActivity());
                video = (EntityVideo) parent.getItemAtPosition(position);
                Ultilities.UpdateVideoHome(getActivity(), video.getId(), arrVideo, adapterVideo, position);
                FragmentVideo.HANDLER_UPDATE_COUNTER = new Handler() {
                    public void handleMessage(Message msg) {
                        Intent myIntent = new Intent(getActivity(), ActivityPlayvideo.class);
                        Bundle bundle = new Bundle();
                        try {
                            String url = video.getLinkurl();
                            if (url.equals("") || url.length() == 0) {
                                String filepath = "";
                                if (!video.getFilepath().toString().equals("null")) {
                                    filepath = video.getFilepath();
                                } else {
                                    filepath = video.getDriver();
                                }
                                Log.e("link", filepath + " ---");
                                bundle.putString("linkvideo", filepath.replaceAll(" ", "%20"));
                                bundle.putString("name", video.getName());
                                myIntent.putExtra("video", bundle);
                                startActivity(myIntent);
                            } else {
                                bundle.putString("linkvideo", url);
                                bundle.putString("name", video.getName());
                                myIntent.putExtra("video", bundle);
                                startActivity(myIntent);
                            }
                            Ultilities.stopMusicService(getActivity());
                            Constant.SONG_PAUSE = true;
                            MainActivity.changeButton();
                        } catch (Exception e) {
                            Log.e("ERROR_VIDEO", e.toString());

                        }
                    }

                    ;
                };

            }
        });

        // put link Song
//        lvSong.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                Ultilities.stopMusicService(getActivity());
//                Constant.SONG_INDEX = position;
//                Constant.CURR_POSITION = 0;
//                Ultilities.setListMusic(arrSongAll);
//                song = (EntitySong) parent.getItemAtPosition(position);
//                Ultilities.UpdateSong(getActivity(), song.getId(), adapterSong,
//                        null, arrSongAll, position);
//                // Ultilities.setOneSongListMusic(arrSong.get(position));
//                UltilFunction.updateTextListTitle(getString(R.string.nct));
//            }
//        });
//        lvPlaylist.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(final AdapterView<?> parent, View view,
//                                    final int position, long id) {
//                list = (EntityPlaylist) parent.getItemAtPosition(position);
//                Ultilities.UpdatePlaylist(getActivity(), list.getId());
//                FragmentPlaylist.HANDLER_UPDATE_COUNT = new Handler() {
//                    @Override
//                    public void handleMessage(Message msg) {
//                        fragmentSongList fragsonglist = new fragmentSongList();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("cusor", 1);
//                        bundle.putInt("idplaylist", list.getId());
//                        bundle.putString("name", list.getName());
//                        bundle.putString("image", list.getImage());
//                        bundle.putInt("counter", list.getCounter());
//                        fragsonglist.setArguments(bundle);
//                        transationToFragment(fragsonglist);
//                    }
//
//                    ;
//                };
//            }
//        });
        if (!UltilFunction.isConnectingToInternet(getActivity())) {
            Toast.makeText(getActivity(),
                    getString(R.string.main_activity_no_internet),
                    Toast.LENGTH_LONG).show();
        }

//        getAllListSong();
//        getHome();
        return view;
    }

    public void getHome() {
        try {
            UltilFunction.startAsyncTask2(new WSgetRank(getActivity()));
            WSgetRank.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToListSong(Constant.RESULT_API);
//                        addToListVideo(Constant.RESULT_API);
//                        addToListPlaylist(Constant.RESULT_API);
//                        Helper.getListViewSize(lvSong);
                        sv.scrollTo(0, sv.getTop());
                        adapterSong.notifyDataSetChanged();
                    }
                }
            };

            UltilFunction.startAsyncTask2(new WSgetRankBook(getActivity()));
            WSgetRankBook.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API_BOOK!= null) {
                        addToListBook(Constant.RESULT_API_BOOK);
                        adapterBook.notifyDataSetChanged();
                    }
                }
            };

            UltilFunction.startAsyncTask2(new WSgetRanNews(getActivity()));
            WSgetRanNews.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API_NEWS != null) {
                        addToListNews(Constant.RESULT_API_NEWS);
                        adapterNews.notifyDataSetChanged();
                    }
                }
            };

            UltilFunction.startAsyncTask2(new WSgetRankStory(getActivity()));
            WSgetRankStory.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API_STORY != null) {
                        addToListStory(Constant.RESULT_API_STORY);
                        adapterStory.notifyDataSetChanged();
                    }
                }
            };
            UltilFunction.startAsyncTask2(new WSgetRankFilm(getActivity()));
            WSgetRankFilm.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API_FILM != null) {
                        addToListVideo(Constant.RESULT_API_FILM);
                        adapterStory.notifyDataSetChanged();
                    }
                }
            };
        } catch (Exception e) {
            Log.e("getHome", e.toString());
        }
    }

    private void addToListVideo(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                arrVideo.clear();
                adapterVideo.clear();
                JSONArray jsonArray = new JSONArray(resultAPI.getRecord());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idfilm",0);
                    String name = jsonObject.optString("namefilm");
                    String linkurl = jsonObject.optString("linkurl");
                    String image = jsonObject.optString("image");
                    int counter = jsonObject.optInt("counter",0);
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath");
                    String driver = jsonObject.optString("linkdrive");
                    video = new EntityVideo(id, name, linkurl, image, counter, nameart, filepath, driver);
                    arrVideo.add(video);
                    adapterVideo.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addToListPlaylist(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                arrPlaylists.clear();
                AdapterPlaylist.clear();
                JSONArray jsonArray = new JSONArray(resultAPI.getPlaylist());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = Integer.parseInt(jsonObject.opt("idplaylist")
                            .toString());
                    String name = jsonObject.opt("playlist").toString();
                    String image = jsonObject.opt("image").toString();
                    int counter = Integer.parseInt(jsonObject.opt("counter")
                            .toString());
                    list = new EntityPlaylist(id, name, image, counter,
                            EntityPlaylist.TYPE_ONLINE);
                    arrPlaylists.add(list);
                    AdapterPlaylist.notifyDataSetChanged();
                }
            } catch (Exception e) {

            }
        }
    }

    public void getAllListSong() {
        String url = ConfigurationServer.URLServer + "api/listsong";
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {
                    if (jsonObject.getInt("code") == 200) {
                        adapterSong.clear();
                        JSONArray jArray = jsonObject.getJSONArray("records");
                        int length = jArray.length();
                        for (int i = 0; i < length; i++) {
                            jsonObject = jArray.getJSONObject(i);
                            int id = Integer.parseInt(jsonObject.getString("idsong"));
                            String name = jsonObject.getString("namesong");
                            String linksong = jsonObject.getString("linksong");
                            String lyric = jsonObject.getString("lyric");
                            int counter = Integer.parseInt(jsonObject.getString("counter"));
//                            int idplaylist = Integer.parseInt(jsonObject.getString("idplaylist"));
                            int idart = Integer.parseInt(jsonObject.getString("idartist"));
                            String image = jsonObject.getString("image");
                            String nameart = jsonObject.getString("artistname");
                            String filepath = jsonObject.getString("filepath");
                            String driver = jsonObject.getString("linkdrive");
                            song = new EntitySong(id, name, idart, lyric, image,
                                    counter, linksong, nameart, filepath, driver);
                            arrSongAll.add(song);
                            Log.e("link_song", song.getLinksong());
                        }
                    }
                } catch (JSONException e) {

                    Log.e("test", e.toString());
                } finally {
                    Log.e("update_music", "no vao song");
                    adapterSong.notifyDataSetChanged();
                    getHome();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.e("test", volleyError.toString());
                adapterSong.notifyDataSetChanged();
            }
        });
        requestQueue.add(json);
    }

    public void addToListSong(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                arrSong.clear();
                adapterhome.clear();
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = new JSONArray(resultAPI.getSong());
                Log.e("result",jsonArray.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idsong",0);
                    String name = jsonObject.optString("namesong");
                    String linksong = jsonObject.optString("linksong");
                    String lyric = jsonObject.optString("lyric");
                    int counter = jsonObject.optInt("counter",0);
//                    int idplaylist = Integer.parseInt(jsonObject.opt("idplaylist").toString());
                    int idart = jsonObject.optInt("idartist",0);
                    String image = jsonObject.optString("image");
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath");
                    String driver = jsonObject.getString("linkdrive");
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, nameart, filepath, driver);
                    arrSong.add(song);
                    adapterhome.notifyDataSetChanged();

                }
            } catch (Exception e) {
                Log.e("addToListSong", e.toString());
            }
        }
    }
    public void addToListStory(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                arrStory.clear();
                arrStory.clear();
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = new JSONArray(resultAPI.getRecord());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idstory");
                    String name = jsonObject.optString("namestory");
                    String linksong = jsonObject.opt("linkstory").toString();
                    String lyric = jsonObject.optString("lyric");
                    int counter = jsonObject.optInt("counter");
//                    int idplaylist = Integer.parseInt(jsonObject.opt("idplaylist").toString());
                    int idart = jsonObject.optInt("idartist");
                    String image = jsonObject.optString("image");
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath");
                    String driver = jsonObject.getString("linkdrive");
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, nameart, filepath, driver);
                    arrStory.add(song);
                    adapterStory.notifyDataSetChanged();
                }
            } catch (Exception e) {
                Log.e("addToListStory", e.toString());
            }
        }
    }
    public void addToListBook(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                arrBook.clear();
                adapterBook.clear();
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = new JSONArray(resultAPI.getRecord());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idbook",0);
                    String name = jsonObject.optString("namebook");
                    String linksong = jsonObject.optString("linkbook");
                    String lyric = jsonObject.optString("lyric");
                    int counter = jsonObject.optInt("counter",0);
//                    int idplaylist = jsonObject.optInt("idplaylist");
                    int idart = jsonObject.optInt("idartist",0);
                    String image = jsonObject.optString("image");
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath");
                    String driver = jsonObject.getString("linkdrive");
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, nameart, filepath, driver);
                    arrBook.add(song);
                    adapterBook.notifyDataSetChanged();

                }
            } catch (Exception e) {
                Log.e("addToListbook", e.toString());
            }
        }
    }
    public void addToListNews(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            try {
                adapterNews.clear();
                arrNews.clear();
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = new JSONArray(resultAPI.getRecord());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idnews",0);
                    String name = jsonObject.optString("name");
                    String linksong = jsonObject.optString("linknews");
                    String lyric = jsonObject.optString("lyric");
                    int counter = jsonObject.optInt("counter",0);
//                    int idplaylist = Integer.parseInt(jsonObject.opt("idplaylist").toString());
                    int idart = jsonObject.optInt("idartist",0);
                    String image = jsonObject.optString("image");
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath");
                    String driver = jsonObject.getString("linkdrive");
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, nameart, filepath, driver);
                    arrNews.add(song);
                    adapterNews.notifyDataSetChanged();

                }
            } catch (Exception e) {
                Log.e("addToListnews", e.toString());
            }
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        getAllListSong();
        adapterSong.notifyDataSetChanged();
        Log.e("update_music", "no vao home");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onButtonDownloadClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), new EntitySong[]{song});
    }

    @Override
    public void onButtonShareClickListener(EntitySong song, int position) {
        String url = song.getLinksong().replaceAll(" ", "%20");
        if (url.equals("") || url.equals("null")) {
            url = song.getFilepath().replaceAll(" ", "%20");
        }

        if (!url.equals("null") && !url.equals("")) {
            try {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, song.getName() + " - " + url);
                Log.e("share", song.getName() + " - " + url);

                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                        shareIntent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }
                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + song.getName() + " - " + url;
                    shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                }

                getActivity().startActivity(
                        Intent.createChooser(shareIntent, "Share via"));
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    public void popupShowAddplaylist(final int idsong) {
        try {

            LayoutInflater inflater = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_playlist, null);
            final PopupWindow addplaylistPopup = new PopupWindow(getActivity());
            addplaylistPopup.setContentView(layout);
            addplaylistPopup.setWidth((1 * Constant.widthPixels) / 3);
            addplaylistPopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            addplaylistPopup.setFocusable(true);
            addplaylistPopup.update();

            final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_type, arrType);
            Ultilities.getPlaylistUser(adapterType, arrType, getActivity(), userid);
            lvplaylist.setAdapter(adapterType);
            lvplaylist.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // TODO Auto-generated method stub
                    addplaylistPopup.dismiss();
                    EntityType type = (EntityType) parent
                            .getItemAtPosition(position);
                    Ultilities.InsertSong(getActivity(), type.getIdtype(),
                            idsong);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void onButtonAddClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        popupShowAddplaylist(song.getId());
    }

    private void transationToFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity()
                .getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, fragment, fragment
                .getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    public void showMessage(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
