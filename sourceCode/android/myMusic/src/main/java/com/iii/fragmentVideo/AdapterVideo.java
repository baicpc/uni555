package com.iii.fragmentVideo;

import java.util.ArrayList;
import java.util.List;

import com.iii.Ultils.Constant;
import com.iii.mymusic.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterVideo extends ArrayAdapter<EntityVideo> {
	Activity context;
	EntityVideo video;
	ArrayList<EntityVideo> arrVideo = null;
	int rs_layout;

	public AdapterVideo(Activity context, int resource, ArrayList<EntityVideo> arrVideo) {
		super(context, resource, arrVideo);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrVideo = arrVideo;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.imgArtist = (ImageView) convertView.findViewById(R.id.imgArtist);
			holder.tvName = (TextView) convertView.findViewById(R.id.tvNameVD);
			holder.tvCounter = (TextView) convertView.findViewById(R.id.tvcountervideo);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		video = arrVideo.get(position);
		holder.tvName.setText(video.getName());
		holder.tvCounter.setText(String.valueOf(video.getCounter()));
		Picasso.with(context).load(Constant.URL_IMG + video.getImage()).placeholder(R.drawable.down)
				.into(holder.imgArtist);
		return convertView;
	}

	static class ViewHolder {
		ImageView imgArtist;
		TextView tvName, tvCounter;
	}

}
