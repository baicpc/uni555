package com.iii.fragmentSong;

import java.io.Serializable;

public class EntitySong implements Serializable {
	
	public static final int TYPE_ONLINE = 0;
	public static final int TYPE_LOCAL = 1;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id;
	String name = "";
	String artist = "";
	int counter;
	String linksong = "";
	String image = "";
	String lyric = "";
	String nameart = "";
	int idartist;
	int idsubject;
	int idtype;
	int idmedia;
	int idplaylist;
	String filepath = "";
	String filename = "";
	String filetype = "";
	String linkDriver = "";
	boolean select;
	int songType = TYPE_ONLINE;

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getNameart() {
		return nameart;
	}

	public void setNameart(String nameart) {
		this.nameart = nameart;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public String getLinksong() {
		return linksong;
	}

	public void setLinksong(String linksong) {
		this.linksong = linksong;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	public int getIdartist() {
		return idartist;
	}

	public void setIdartist(int idartist) {
		this.idartist = idartist;
	}

	public int getIdsubject() {
		return idsubject;
	}

	public void setIdsubject(int idsubject) {
		this.idsubject = idsubject;
	}

	public int getIdtype() {
		return idtype;
	}

	public void setIdtype(int idtype) {
		this.idtype = idtype;
	}

	public int getIdmedia() {
		return idmedia;
	}

	public void setIdmedia(int idmedia) {
		this.idmedia = idmedia;
	}

	public int getIdplaylist() {
		return idplaylist;
	}

	public void setIdplaylist(int idplaylist) {
		this.idplaylist = idplaylist;
	}

	public EntitySong() {

	}
	

	public int getSongType() {
		return songType;
	}

	public void setSongType(int songType) {
		this.songType = songType;
	}

	public EntitySong(int id, String name, int idartist, String lyric,
			String img, int counter, String linksong, int idplaylist,String nameart,String filepath, String linkDriver) {
		this.id = id;
		this.name = name;
		this.idartist = idartist;
		this.lyric = lyric;
		this.image = img;
		this.counter = counter;
		this.linksong = linksong;
		this.idplaylist = idplaylist;
		this.nameart=nameart;
		this.filepath=filepath;
		this.linkDriver = linkDriver;
	}

	public EntitySong(int id, String name, int idartist, String lyric,
					  String img, int counter, String linksong,String nameart,String filepath, String linkDriver) {
		this.id = id;
		this.name = name;
		this.idartist = idartist;
		this.lyric = lyric;
		this.image = img;
		this.counter = counter;
		this.linksong = linksong;
		this.idplaylist = idplaylist;
		this.nameart=nameart;
		this.filepath=filepath;
		this.linkDriver = linkDriver;
	}

	public EntitySong(String name, String linksong) {
		super();
		this.name = name;
		this.linksong = linksong;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}

	public String getLinkDriver() {
		return linkDriver;
	}

	public void setLinkDriver(String linkDriver) {
		this.linkDriver = linkDriver;
	}
}
