package com.iii.fragmentVideo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetType;
import com.iii.Webservice.WSgetTypeFilm;
import com.iii.Webservice.WSgetVideobyidType;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;
import com.iii.playVideo.ActivityPlayvideo;

public class FragmentVideo extends Fragment {
    public static Handler HANDLER_UPDATE_COUNTER;
    View view;
    GridView grvVideo;
    EntityVideo video;
    AdapterVideo adapterVideo;
    ArrayList<EntityVideo> arrVideo;
    TextView tvKhac, tvTitle, tvCounter;
    Button tvMoi;// tvnhactre, tvtrutinh;
    PopupWindow popUpWindow;
    EntityType type;
    ImageView imageOther;

    private static FragmentVideo fragment;

    int id_list = 18;
    static String nameType = "";

    public static FragmentVideo getInstants() {
        if (fragment == null)
            fragment = new FragmentVideo();
        return fragment;
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.fragment_video, container, false);
        grvVideo = (GridView) view.findViewById(R.id.grvVideo);
        tvKhac = (TextView) view.findViewById(R.id.tvKhac);
        tvMoi = (Button) view.findViewById(R.id.tvmoihotvideo);
//        tvnhactre = (Button) view.findViewById(R.id.tvnhactrevideo);
//        tvtrutinh = (Button) view.findViewById(R.id.tvtrutinhvideo);
        tvCounter = (TextView) view.findViewById(R.id.tvcountervideo);
        tvTitle = (TextView) view.findViewById(R.id.tvTitleVd);
        imageOther = (ImageView) view.findViewById(R.id.imageOther);
        tvTitle.setTextColor(getResources().getColor(R.color.blue));
        arrVideo = new ArrayList<EntityVideo>();
        adapterVideo = new AdapterVideo(getActivity(), R.layout.custom_item_video_home, arrVideo);
        grvVideo.setAdapter(adapterVideo);
        grvVideo.setEmptyView(view.findViewById(R.id.text_empty));

        if (nameType.equals("")) {
            nameType = getString(R.string.new_hot);
            tvTitle.setText(R.string.new_hot);
        } else {
            tvTitle.setText(nameType);
        }

        grvVideo.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                // TODO Auto-generated method stub
                Ultilities.stopMusicService(getActivity());
                video = (EntityVideo) parent.getItemAtPosition(position);
                Ultilities.UpdateVideo(getActivity(), video.getId(), arrVideo, adapterVideo, position);
                FragmentVideo.HANDLER_UPDATE_COUNTER = new Handler() {
                    public void handleMessage(Message msg) {
                        Intent myIntent = new Intent(getActivity(), ActivityPlayvideo.class);
                        Bundle bundle = new Bundle();
                        try {
                            String url = video.getLinkurl();
                            if (url == "null" || url.length() == 0) {
                                String filepath = "";
                                if (!video.getFilepath().toString().equals("null")) {
                                    filepath = video.getFilepath();
                                }
                                else {

                                    filepath = video.getDriver();
                                }
                                Log.e("link_video", filepath);
                                bundle.putString("linkvideo", filepath.replaceAll(" ", "%20"));
                                bundle.putString("name", video.getName());
                                myIntent.putExtra("video", bundle);
                                startActivity(myIntent);

                            } else {
                                url = video.getFilepath();
                                bundle.putString("linkvideo", arrVideo.get(position).getLinkurl());
                                bundle.putString("name", video.getName());
                                myIntent.putExtra("video", bundle);
                                startActivity(myIntent);
                            }
                            Ultilities.stopMusicService(getActivity());
                            Constant.SONG_PAUSE = true;
                            MainActivity.changeButton();
                        } catch (Exception e) {
                            e.toString();
                        }
                    }

                    ;
                };

            }
        });
        tvMoi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                adapterVideo.clear();
                getListVideobyidtype(18);
                tvTitle.setText(R.string.new_hot);
                nameType = getResources().getString(R.string.new_hot);
//				tvTitle.setTextColor(getResources().getColor(R.color.blue));
//				tvMoi.setTextColor(getResources().getColor(R.color.blue));
//				tvtrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
//				tvnhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
            }
        });
//        tvnhactre.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                adapterVideo.clear();
//                getListVideobyidtype(3);
//                tvTitle.setText(R.string.nhactre);
//                nameType = getResources().getString(R.string.nhactre);
////				tvTitle.setTextColor(getResources().getColor(R.color.blue));
////				tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
////				tvnhactre.setTextColor(getResources().getColor(R.color.blue));
////				tvtrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
//            }
//        });
//        tvtrutinh.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                adapterVideo.clear();
//                getListVideobyidtype(2);
//                tvTitle.setText(R.string.trutinh);
//                nameType = getResources().getString(R.string.trutinh);
////				tvTitle.setTextColor(getResources().getColor(R.color.blue));
////				tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
////				tvnhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
////				tvtrutinh.setTextColor(getResources().getColor(R.color.blue));
//            }
//        });

        LinearLayout layoutOther = (LinearLayout) view.findViewById(R.id.layout_other);
        layoutOther.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (popUpWindow == null) {
                    showPopup(v);
                } else {
                    if (popUpWindow.isShowing()) {
                        popUpWindow.dismiss();
                    } else {
                        showPopup(v);
                    }
                }
            }
        });
        if (!UltilFunction.isConnectingToInternet(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
        }
        getListVideobyidtype(id_list);
        return view;
    }

    private void getType(final ArrayList<EntityType> arrType, final ArrayAdapter<EntityType> adapterType) {
        // TODO Auto-generated method stub
        try {
            UltilFunction.startAsyncTask2(new WSgetTypeFilm(getActivity()));
            WSgetTypeFilm.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(android.os.Message msg) {
                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
                            String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
                            try {
                                JSONObject jsonRootObject = new JSONObject(strJson);
                                // Get the instance of JSONArray that contains
                                // JSONObjects
                                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    int id = Integer.parseInt(jsonObject.opt("idtype").toString());
                                    String name = jsonObject.optString("nametype").toString();
                                    type = new EntityType(id, name);
                                    arrType.add(type);
                                }
//                                arrType.add(new EntityType(17,"Japan"));
//                                arrType.add(new EntityType(18,"Vietnam"));
//                                arrType.add(new EntityType(19,"Campuchia"));
                                adapterType.notifyDataSetChanged();
                            } catch (Exception e) {

                            }
                        }
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

//	private void getVideo() {
//		// TODO Auto-generated method stub
//
//		try {
//			UltilFunction.startAsyncTask2(new WSgetVideobyidType(view.getContext(), id_list));
//			WSgetVideobyidType.HANDLER_SUCCESS = new Handler() {
//				@Override
//				public void handleMessage(Message msg) {
//					if (Constant.RESULT_API != null) {
//						addToList(Constant.RESULT_API);
//					}
//				}
//			};
//
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}

    public void getListVideobyidtype(int idtype) {
        try {
            UltilFunction.startAsyncTask2(new WSgetVideobyidType(view.getContext(), idtype));
            WSgetVideobyidType.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    public void addToList(ResultAPI resultAPI) {
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
            try {
                JSONObject jsonRootObject = new JSONObject(strJson);
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idfilm");
                    String name = jsonObject.optString("namefilm").toString();
                    String linkurl = jsonObject.optString("linkurl").toString();
                    String image = jsonObject.optString("image").toString();
                    int counter = jsonObject.optInt("counter");
                    String nameart = jsonObject.optString("artistname").toString();
                    String filepath = jsonObject.optString("filepath").toString();
                    String driver = jsonObject.getString("linkdrive").toString();
                    video = new EntityVideo(id, name, linkurl, image, counter, nameart, filepath, driver);
                    arrVideo.add(video);
                }
                adapterVideo.notifyDataSetChanged();
            } catch (Exception e) {

            }
        } else {
            grvVideo.setVisibility(View.GONE);
        }
    }

    private void showPopup(View v) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_type, null);
            popUpWindow = new PopupWindow(layout, (int) ((8.2 * Constant.widthPixels) / 10), LayoutParams.WRAP_CONTENT);
            int location[] = new int[2];
            v.getLocationOnScreen(location);
            popUpWindow.setOutsideTouchable(true);
            imageOther.setImageResource(R.drawable.btn_choose);
            popUpWindow.setOnDismissListener(new OnDismissListener() {

                @Override
                public void onDismiss() {
                    imageOther.setImageResource(R.drawable.btn_chosesdown);
                }
            });
            popUpWindow.setBackgroundDrawable(new BitmapDrawable());
            popUpWindow.showAtLocation(layout, Gravity.NO_GRAVITY, Constant.widthPixels - popUpWindow.getWidth() - 2,
                    location[1] + v.getHeight());
            popUpWindow.setFocusable(true);
            popUpWindow.update();

            final GridView grvType = (GridView) layout.findViewById(R.id.grvType);

            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_menu_popup,
                    arrType);
            getType(arrType, adapterType);
            grvType.setAdapter(adapterType);
            grvType.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    type = (EntityType) parent.getItemAtPosition(position);
                    if (popUpWindow.isShowing()) {
                        popUpWindow.dismiss();
                    }
                    adapterVideo.clear();
                    getListVideobyidtype(type.getIdtype());
                    adapterVideo.notifyDataSetChanged();
                    tvTitle.setText(type.getNametype());
                    tvTitle.setTextColor(getResources().getColor(R.color.blue));
                    tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
//                    tvtrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
//                    tvnhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
                    nameType = type.getNametype();
                    id_list = type.getIdtype();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            e.toString();
        }
    }
}
