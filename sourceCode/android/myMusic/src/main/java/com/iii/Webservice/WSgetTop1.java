package com.iii.Webservice;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;

public class WSgetTop1 extends AsyncTask<Void, Void, ResultAPI>{
	private ConfigurationWS mWS;
	private Context mContext;

	public WSgetTop1(Context context) {
		this.mContext = context;
	}

	@Override
	protected synchronized ResultAPI doInBackground(Void... params) {
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/download";
			JSONObject json = new JSONObject();

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setMessage(jsonObject.getString("message"));
				result.setStatus(jsonObject.getString("status"));
				result.setRecord(jsonObject.getString("records"));
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}
