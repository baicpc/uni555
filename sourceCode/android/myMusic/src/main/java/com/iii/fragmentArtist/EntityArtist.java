package com.iii.fragmentArtist;

import java.io.Serializable;

public class EntityArtist implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String artistname;
	private String image;
	private int idcountry;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArtistname() {
		return artistname;
	}
	public void setArtistname(String artistname) {
		this.artistname = artistname;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(int idcountry) {
		this.idcountry = idcountry;
	}
	public EntityArtist(){
		
	}
	public EntityArtist(int id, String name, String image, int idcountry){
		this.id=id;
		this.artistname=name;
		this.image=image;
		this.idcountry=idcountry;
	}

}
