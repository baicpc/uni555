package com.iii.Ultils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.iii.Account.UserAccount;
import com.iii.fragmentSong.EntitySong;
import com.iii.mymusic.R;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

public class UltilFunction {

    // --START INIT FIST APP--//
    public static void getFirstApp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.MY_SETTING, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(Constant.KEY_APP_FIRST, true)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(Constant.KEY_APP_FIRST, false);
            edit.putString(Constant.KEY_REPEAT_MODE, "NONE");
            edit.commit();
        }
    }
    // --END INIT FIST APP--//

    // --START SAVE REPEAT STATE--//
    public static void saveRepeat(Context context, String repeat) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.MY_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(Constant.KEY_REPEAT_MODE, repeat);
        edit.commit();
    }
    // --END SAVE REPEAT STATE--//

    // --START GET REPEAT FROM SHARED PREFERENCES--//
    public static String getRepeat(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.MY_SETTING, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Constant.KEY_REPEAT_MODE, "NONE");
    }
    // --END GET REPEAT FROM SHARED PREFERENCES--//

    // --START SAVE PLAY STATE--//
    public static void savePlayStatus(Context context, boolean songPaused) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.MY_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(Constant.KEY_SONG_PAUSE, songPaused);
        edit.commit();
    }
    // --END SAVE PLAY STATE--//

    // --START GET PLAY STATUS FROM SHARED PREFERENCES--//
    public static boolean getPlayStatus(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.MY_SETTING, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(Constant.KEY_SONG_PAUSE, true);
    }
    // --END GET PLAY STATUS FROM SHARED PREFERENCES--//

    // --START SAVE USER INFO--//
    public static void saveUser(String userInfo, Context context) {
        try {
            JSONObject jsonObj = new JSONObject(userInfo);
            UserAccount userAccount = new UserAccount();
            userAccount.setId(Integer.parseInt(jsonObj.getString("id")));
            userAccount.setFirstName(jsonObj.getString("firstname"));
            userAccount.setLastName(jsonObj.getString("lastname"));
            userAccount.setEmail(jsonObj.getString("email"));
            saveUserAccountToSharedPreference(userAccount, context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void saveUserAccountToSharedPreference(UserAccount userAccount, Context context) {
        SharedPreferences sharedPreference = context.getSharedPreferences(Constant.LOGIN_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreference.edit();
        edit.putInt(Constant.USER_ID, userAccount.getId());
        edit.putString(Constant.FIRSTNAME, userAccount.getFirstName());
        edit.putString(Constant.LASTNAME, userAccount.getLastName());
        edit.putString(Constant.EMAIL, userAccount.getEmail());
        edit.putString(Constant.URL_PICTURE, userAccount.getUrlPicture());
        edit.commit();
    }
    // --END SAVE USER INFO--//

    // --START GET USER FROM SHARED PREFERENCE--//
    public static UserAccount getUserFromSharedPreference(Context context) {
        SharedPreferences sharedPreference = context.getSharedPreferences(Constant.LOGIN_USER, Context.MODE_PRIVATE);
        UserAccount userAccount = new UserAccount();
        userAccount.setId(sharedPreference.getInt(Constant.USER_ID, 0));
        userAccount.setFirstName(sharedPreference.getString(Constant.FIRSTNAME, Constant.USER_NOT_LOGIN));
        userAccount.setLastName(sharedPreference.getString(Constant.LASTNAME, ""));
        userAccount.setEmail(sharedPreference.getString(Constant.EMAIL, ""));
        userAccount.setUrlPicture(sharedPreference.getString(Constant.URL_PICTURE, UserAccount.URL_PICTURE_NULL));
        if (userAccount.getFirstName().equals(Constant.USER_NOT_LOGIN))
            return null;
        else
            return userAccount;
    }
    // --END GET USER FROM SHARED PREFERENCE--//

    // --START CLEAR USER INFO--//
    public static void clearUserAccount(Context context) {
        SharedPreferences sharedPreference = context.getSharedPreferences(Constant.LOGIN_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreference.edit();
        if (edit != null) {
            edit.remove(Constant.USER_ID);
            edit.remove(Constant.LASTNAME);
            edit.remove(Constant.FIRSTNAME);
            edit.remove(Constant.EMAIL);
            edit.remove(Constant.URL_PICTURE);
            edit.commit();
        }

    }
    // --END CLEAR USER INFO--//

    // --START SAVE USER INFO WHEN LOGIN WITH SOCIAL ACCOUNT--//
    public static void saveUserWithSocialAccount(String userInfo, Context context, String urlPicture) {
        try {
            JSONObject jsonObj = new JSONObject(userInfo);
            UserAccount userAccount = new UserAccount();
            userAccount.setId(Integer.parseInt(jsonObj.getString("id")));
            userAccount.setFirstName(jsonObj.getString("firstname"));
            userAccount.setLastName(jsonObj.getString("lastname"));
            userAccount.setEmail(jsonObj.getString("email"));
            userAccount.setUrlPicture(urlPicture);
            saveUserAccountToSharedPreference(userAccount, context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // --END SAVE USER INFO WHEN LOGIN WITH SOCIAL ACCOUNT--//

    // --START SET IMAGE ALBUM ART TO IMAGEVIEW--//
    public static void setImageAlbumArt(Context context, String data, ImageView image) {
        Bitmap bitmap = null;
        if (Constant.IS_MUSIC_LOCAL) {
            bitmap = getLocalBitmap(data);
            if (bitmap != null) {
                image.setImageBitmap(bitmap);
            } else {
                image.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.music));
            }
        } else {
            try {
                Picasso.with(context).load(data).into(image);
            } catch (NoClassDefFoundError e) {
                // TODO: handle exception
            }
        }
    }

    private static Bitmap getLocalBitmap(String data) {
        Bitmap bitmap = null;
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(data);
            byte[] d = mmr.getEmbeddedPicture();

            if (d != null) {
                bitmap = BitmapFactory.decodeByteArray(d, 0, d.length);
                d = null;
            }
        } catch (Exception e) {

        } finally {

            return bitmap;
        }
    }
    // --END SET IMAGE ALBUM ART TO IMAGEVIEW--//

    // --START CHECK INTERNET CONNECTION--//
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }
    // --END CHECK INTERNET CONNECTION--//

    @SuppressWarnings("unchecked")
    public static void startAsyncTask(@SuppressWarnings("rawtypes") AsyncTask asyncTask, EntitySong[] params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            asyncTask.execute(params);
        }
    }

    @SuppressWarnings("unchecked")
    public static void startAsyncTask2(@SuppressWarnings("rawtypes") AsyncTask asyncTask) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            asyncTask.execute();
        }
    }

    public static void updateTextListTitle(String title) {
        Constant.TITLE_LIST_IS_PLAYING = title;
    }

    public static boolean isEmailValid(String email) {

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
