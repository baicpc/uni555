package com.iii.fragmentSubject;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WSSubject;
import com.iii.fragmentSong.FragmentSong;
import com.iii.fragmentSong.fragmentSongList;
import com.iii.mymusic.R;

public class FragmentSubject extends Fragment {
	View view;
	GridView grvSubject;
	EntitySubject sub;
	ArrayAdapter<EntitySubject> adapterSubject;
	ArrayList<EntitySubject> arrSub;

	private static FragmentSubject fragment;

	public static FragmentSubject getInstants() {
		if (fragment == null)
			fragment = new FragmentSubject();
		return fragment;
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_subject, container, false);
		grvSubject = (GridView) view.findViewById(R.id.grvSubject);
		arrSub = new ArrayList<EntitySubject>();
		adapterSubject = new AdapterSubject(getActivity(), R.layout.custom_item_subject, arrSub);
		grvSubject.setAdapter(adapterSubject);
		grvSubject.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				sub = (EntitySubject) parent.getItemAtPosition(position);
				fragmentSongList fragsonglist = new fragmentSongList();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 2);
				bundle.putInt("idsub", sub.getId());
				bundle.putString("namesub", sub.getSubname());
				bundle.putString("imgsub", sub.getImage());
				fragsonglist.setArguments(bundle);
				transationToFragment(fragsonglist);
			}
		});
		if (!UltilFunction.isConnectingToInternet(getActivity())) {
			Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
		}
		getSubject();

		return view;
	}

	private void getSubject() {
		// TODO Auto-generated method stub
		try {
			UltilFunction.startAsyncTask2(new WSSubject(view.getContext()));
			WSSubject.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
							try {
								JSONObject jsonRootObject = new JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = jsonRootObject.optJSONArray("records");
								for (int i = 0; i < jsonArray.length(); i++) {
									JSONObject jsonObject = jsonArray.getJSONObject(i);
									int id = Integer.parseInt(jsonObject.opt("idsubject").toString());
									String name = jsonObject.optString("subname").toString();
									String description = jsonObject.opt("description").toString();
									String image = jsonObject.opt("image").toString();
									sub = new EntitySubject(id, name, description, image);
									arrSub.add(sub);
								}
								adapterSubject.notifyDataSetChanged();
							} catch (Exception e) {

							}
						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

}
