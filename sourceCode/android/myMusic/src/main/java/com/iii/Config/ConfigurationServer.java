package com.iii.Config;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class ConfigurationServer {

	// ---------------fields------------------------------//
	//public static String URLServer = "http://localhost:8080/api/app_dev.php/";
	//public static String URLServer = "http://192.168.1.130:8888/api/app_dev.php/";
//	public static String URLServer = "http://222.255.46.7:8080/nhaccuatui_ws/web/app_dev.php/";
	public static String URLServer =""; //"http://api.uni555.xyz/web/app_dev.php/";
	public static final String URL_ROUTER = "http://api.3i.com.vn/api/";
	//public static String URLServer = "http://192.168.11.108:8888/api/app_dev.php/";

	
	/*----------------language----------------------------*/

	private static String Lng = "";

	public static int body = 0;

	public static int status = 0;
	public static String table_code = "";
	public static int itable_id = 0;
	public static int user_id = 0;
	public static int language_code = 1;
	public static int floor = -1;
	private Context context;

	public ConfigurationServer(Context context) {
		this.context = context;
	}

//	public void writeToFile(String data) {
//		try {
//			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(FILENAME, Context.MODE_PRIVATE));
//			outputStreamWriter.write(data);
//			outputStreamWriter.close();
//		} catch (Exception e) {
//			Log.e("Error", "File write failed: " + e.toString());
//		}
//	}

//	public String getURLServerFromFile() {
//		String ret = "";
//		try {
//			try {
//				InputStream inputStream = context.openFileInput(FILENAME);
//				if (inputStream != null) {
//					InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//					BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//					String receiveString = "";
//					StringBuilder stringBuilder = new StringBuilder();
//
//					while ((receiveString = bufferedReader.readLine()) != null) {
//						stringBuilder.append(receiveString);
//					}
//					inputStream.close();
//					ret = stringBuilder.toString();
//				}
//			} catch (FileNotFoundException e) {
//				Log.e("Error", "File not found: " + e.toString());
//			} catch (IOException e) {
//				Log.e("Error", "Can not read file: " + e.toString());
//			}
//		} catch (Exception e) {
//		}
//		if (ret.equals(""))
//			ret = "http://117.6.131.222:8686/pos/wspos/";
//		//				ret = "http://117.6.131.222:6789/pos/wspos/";
//		return ret;
//	}

	public static String getURLServer() {
		return URLServer;
	}

	public static void setURLServer(String uRLServer) {
		URLServer = uRLServer;
	}

	public String getLng() {
		return Lng;
	}

	public void setLng(String lng) {
		Lng = lng;
	}



//	public static String getURLServerWallet() {
//		return URLServerWallet + "/";
//	}
//
//	public static void setURLServerWallet(String uRLServerWallet) {
//		URLServerWallet = uRLServerWallet;
//	}
//
//
//
//	public static String getURLServerChat() {
//		return URLServerChat;
//	}
//
//	public static void setURLServerChat(String uRLServerChat) {
//		URLServerChat = uRLServerChat;
//	}

	// ---------check the network available-------------------//
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}
