package com.iii.fragmentPlaylist;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iii.Ultils.Constant;
import com.iii.mymusic.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class AdapterPlaylist extends ArrayAdapter<EntityPlaylist> {

	Activity context;
	ArrayList<EntityPlaylist> ArrList = null;
	int rs_layout;

	public AdapterPlaylist(Activity context, int resource, ArrayList<EntityPlaylist> listplaylist) {
		super(context, resource, listplaylist);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.ArrList = listplaylist;
	}

	@SuppressWarnings("unused")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.imgView = (ImageView) convertView.findViewById(R.id.img_playlist);
			holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
			holder.tvCounter = (TextView) convertView.findViewById(R.id.tv_counter_playlist);
			holder.tvNamelogin = (TextView) convertView.findViewById(R.id.tvName_Typemy);
			holder.tvNamePlaylist = (TextView) convertView.findViewById(R.id.text_playlist_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final EntityPlaylist playlist = ArrList.get(position);
		if (playlist.getType() == EntityPlaylist.TYPE_ONLINE) {
			holder.tvName.setText(playlist.getName());
			holder.tvCounter.setText(String.valueOf(playlist.getCounter()));
			Picasso.with(context).load(Constant.URL_IMG + playlist.getImage())
					.placeholder(R.drawable.down).into(holder.imgView);
		} else if (playlist.getType() == EntityPlaylist.TYPE_MY) {
			holder.tvNamelogin.setText(playlist.getName());
			holder.tvNamePlaylist.setText(playlist.getName());
		}
		return convertView;
	}

	static class ViewHolder {
		ImageView imgView;
		TextView tvName, tvCounter, tvNamelogin, tvNamePlaylist;
	}

}
