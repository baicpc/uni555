package com.iii.fragmentNCT;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.iii.fragmentSong.EntitySong;

public class GetNCT {
	private Context context;
	private static ArrayList<HashMap<String, String>> files;

	public GetNCT(Context ctx) {
		context = ctx;

	}

	public static ArrayList<EntitySong> getMP3() {
		ArrayList<EntitySong> lists = new ArrayList<EntitySong>();
		getMp3Path();
		for (int i = 0; i < files.size(); i++) {
			String filePath = files.get(i).get("data");
			String name = files.get(i).get("name");
			EntitySong song = new EntitySong();
			song.setFilepath(filePath);
			song.setName(name);
			song.setSongType(EntitySong.TYPE_LOCAL);
			lists.add(song);
		}
		return lists;
	}

	private static void getMp3Path() {
		files = new ArrayList<HashMap<String, String>>();
		String sdCardState = Environment.getExternalStorageState();
		if (!sdCardState.equals(Environment.MEDIA_MOUNTED)) {

		} else {
			File root = new File(android.os.Environment.getExternalStorageDirectory(), "/NCT/");
			if (!root.exists())
				root.mkdirs();
			Log.i("PATH: ", root.getAbsolutePath());
			File[] listFile = root.listFiles();
			if (listFile == null) {
				Log.d("ERROR", "-----click---------");
			}
			if (listFile != null && listFile.length > 0) {
				for (File file : listFile) {
					if (file.isDirectory()) {
					//	getMp3Path();
					} else {
						if (file.getName().toLowerCase().endsWith(".mp3")) {
							HashMap<String, String> f = new HashMap<String, String>();
							f.put("name", file.getName());
							f.put("data", file.getPath());
							files.add(f);
						}
					}
				}
			}
		}
	}

	public static ArrayList<EntitySong> listOfSongs(Context context) {
		Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Cursor c = context.getContentResolver().query(uri, null, MediaStore.Audio.Media.IS_MUSIC + " != 0", null, null);
		ArrayList<EntitySong> listOfSongs = new ArrayList<EntitySong>();
		c.moveToNext();
		while (c.moveToNext()) {
			EntitySong musicData = new EntitySong();
			String title = c.getString(c.getColumnIndex(MediaStore.Audio.Media.TITLE));
			String data = c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA));
			musicData.setName(title);
			musicData.setFilepath(data);
			musicData.setSongType(EntitySong.TYPE_LOCAL);
			listOfSongs.add(musicData);
		}
		c.close();
		/*ArrayList<EntitySong> listDownload = getMP3();
		if(listDownload.size() > 0){
			for(int i = 0; i < listDownload.size(); i++){
				listOfSongs.add(listDownload.get(i));
			}
		}*/
		Log.d("SIZE", "SIZE: " + listOfSongs.size());
		return listOfSongs;
	}

}
