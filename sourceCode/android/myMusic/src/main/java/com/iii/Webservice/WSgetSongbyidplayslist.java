package com.iii.Webservice;

import org.json.JSONObject;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

public class WSgetSongbyidplayslist extends AsyncTask<Void, Void, Void> {
	public static Handler HANDLER_SUCCESS;
	private ConfigurationWS mWS;
	private Context mContext;
	int idplaylist;

	public WSgetSongbyidplayslist(Context context, int id) {
		this.mContext = context;
		this.idplaylist = id;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		Ultilities.showProgress(mContext);

	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/findsongbyidplaylist";
			JSONObject json = new JSONObject();
			json.put("id", idplaylist);

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setMessage(jsonObject.getString("message"));
				// result.setStatus(jsonObject.getString("status"));
				result.setRecord(jsonObject.getString("records"));
				Constant.RESULT_API = result;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		Ultilities.hideProgress(mContext);
		HANDLER_SUCCESS.sendMessage(HANDLER_SUCCESS.obtainMessage());
	}

}
