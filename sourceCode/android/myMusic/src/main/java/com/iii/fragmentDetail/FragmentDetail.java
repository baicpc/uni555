package com.iii.fragmentDetail;

import com.iii.Ultils.Constant;
import com.iii.fragmentHome.FragmentHome;
import com.iii.mymusic.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentDetail extends Fragment {
	View view;
	TextView tvnamesong, tvartist, tvlyric;
	ImageView imageBackground;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_detail_search, container, false);
		tvnamesong = (TextView) view.findViewById(R.id.tvnameSongdetail);
		tvartist = (TextView) view.findViewById(R.id.tvnamArtdetail);
		tvlyric = (TextView) view.findViewById(R.id.tvlyric);
		imageBackground = (ImageView) view.findViewById(R.id.image_bg);
		Bundle bundle = this.getArguments();
		String name = bundle.getString("name");
		String art = bundle.getString("art");
		String lyric = bundle.getString("lyric");
		String image = bundle.getString("image");
		tvnamesong.setText(name);
		tvartist.setText(art);
		try {
			tvlyric.setText(Html.fromHtml(lyric).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Picasso.with(getActivity()).load(Constant.URL_IMG + image).into(imageBackground);
		ImageView layoutBack = (ImageView) view.findViewById(R.id.button_back);
		layoutBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});
		AddSong();
		return view;
	}

	public void AddSong() {
		tvnamesong.setText(Constant.SONG_LIST.get(Constant.SONG_INDEX).getName());
		tvartist.setText(Constant.SONG_LIST.get(Constant.SONG_INDEX).getNameart());
		tvlyric.setText(Constant.SONG_LIST.get(Constant.SONG_INDEX).getLyric());
	}

}
