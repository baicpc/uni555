package com.iii.Config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class ConfigurationWS {
    int TIMEOUT_MILLISEC = 10000;
    Context context;

    static ConfigurationWS config;

    public static ConfigurationWS getInstance(Context context) {
        if (config == null) {
            config = new ConfigurationWS(context);
        }
        return config;
    }

    public ConfigurationWS(Context context) {
        this.context = context;
    }

    public JSONArray connectWSPut_Get_Data(String url, JSONObject json, String jsonName, String oau) {
        JSONArray jarr = null;
        try {

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString()));
            request.setEntity(new UrlEncodedFormEntity(pairs));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            String result = ConfigurationWSRestClient.convertStreamToString(instream);
            JSONObject jobj = new JSONObject(result);
            jarr = jobj.getJSONArray(jsonName);

        } catch (Exception t) {
            t.printStackTrace();
        }
        return jarr;
    }

    public JSONArray connectWS_Get_Data(String url, String jsonName) {
        JSONArray jarr = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);

            HttpPost request = new HttpPost(url);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();

            InputStream instream = entity.getContent();

            String result = ConfigurationWSRestClient.convertStreamToString(instream);
            JSONObject jobj = new JSONObject(result);
            jarr = jobj.getJSONArray(jsonName);

        } catch (Exception t) {
        }
        return jarr;
    }

    /**
     * @param url
     * @param json
     * @param jsonName
     * @return lay du lieu kieu jsonObject ve
     */
    public String getDataJson(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString()));
            Log.e("test_kkk", pairs.get(0).toString());

            request.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
            request.setHeader("Authorization", oau);
            Log.e("test_kkk", request.getURI().toString());
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
            t.printStackTrace();
        }
        return result;
    }

    public String getLink(String url, String app_name) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            List<NameValuePair> pairs = new ArrayList<>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("app_name", app_name));
            Log.e("test_kkk", pairs.get(0).toString());
            request.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
            Log.e("test_kkk", request.getURI().toString());
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
            t.printStackTrace();
        }
        return result;
    }

    /**
     * @param url
     * @param json
     * @param jsonName
     * @return lay du lieu kieu jsonObject ve
     */
    public String getDataJsonImage(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString().replace("\\/", "/"), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString()));

            request.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
            t.printStackTrace();
        }
        return result;
    }

    public String getDataJson2(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString().replace("\"[", "[")
                    .replace("]\"", "]")));

            request.setEntity(new UrlEncodedFormEntity(pairs));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
        }
        return result;
    }

    public String getDataJson3(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString().replace("\"[", "[")
                    .replace("]\"", "]").replace("\\", "")));

            request.setEntity(new UrlEncodedFormEntity(pairs));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
            t.printStackTrace();
        }
        return result;
    }

    public String getDataJson4(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString().replace("\"[", "[")
                    .replace("]\"", "]").replace("\\", "").replace("\"{", "{").replace("}\"", "}")
                    .replace("\\\"", "\"")));

            request.setEntity(new UrlEncodedFormEntity(pairs));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
            t.printStackTrace();
        }
        return result;
    }

    public String getDataJson5(String url, JSONObject json, String jsonName, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            StringEntity se = new StringEntity(json.toString(), "UTF-8");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            // pairs.add(new BasicNameValuePair("q",
            // "{\"username\" : \"841689285664\"}"));
            pairs.add(new BasicNameValuePair("q", json.toString()
                    .replace("\"[", "[").replace("]\"", "]")
                    .replace("\\\"", "\"").replace("\"{", "{").replace("}\"", "}")

            ));

            request.setEntity(new UrlEncodedFormEntity(pairs));
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
        }
        return result;
    }

    public String getDataJsonDelete(String url, String oau) {
        String result = null;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            // cach khac de thay vao entity __________________________________
            request.setHeader("Authorization", oau);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            result = ConfigurationWSRestClient.convertStreamToString(instream);
        } catch (Exception t) {
        }
        return result;
    }


    public void connectWS_Put_Data(String url, JSONObject json) {
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
            HttpClient client = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            request.setEntity(new ByteArrayEntity(json.toString().getBytes("UTF8")));
            request.setHeader("json", json.toString());
            client.execute(request);
        } catch (Exception t) {
        }
    }

}
