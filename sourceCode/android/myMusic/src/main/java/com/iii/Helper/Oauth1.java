package com.iii.Helper;

import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.RandomStringUtils;
import android.util.Base64;

public class Oauth1 {
	    private String baseURL = "http://verzview.com/verzecoin/demo/api/user/loginFirstTime";
	    private final String CONSUMER_KEY = "admin";
	    private final String CONSUMER_SECRET = "123456";
	    private String httpMethod = "POST";
	    private String query = "%7B%22username%22%3A%2284972098981%22%7D";
	    private String timeStamp;
	    private String nonce;

	    public Oauth1() {
	        timeStamp = getTimestamp();
	        nonce = getNonce();
	    }

	    private static String getNonce() {
	        return RandomStringUtils.randomAlphanumeric(32);
	    }

	    private static String getTimestamp() {
	        return Long.toString((System.currentTimeMillis() / 1000));
	    }

	    private String getBaseSignature(String baseURL) {
	        String baseString = httpMethod + "&";
	        baseString += URLEncoder.encode(baseURL) + "&";
	        String baseOAuth = "oauth_consumer_key=" + CONSUMER_KEY + "&oauth_nonce=" + nonce
	                + "&oauth_signature_method=HMAC-SHA1" + "&oauth_timestamp=" + timeStamp
	                + "&oauth_version=1.0" + "&q=";
	        baseString += URLEncoder.encode(baseOAuth);
	        baseString += URLEncoder.encode(query);
	        return baseString;
	    }

	    private final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

	    private String getSignature(String baseURL) throws SignatureException, NoSuchAlgorithmException,
	            InvalidKeyException {
	        String data = getBaseSignature(baseURL);
	        String key = URLEncoder.encode(CONSUMER_SECRET) + "&";
	        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
	        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
	        mac.init(signingKey);

	        return Base64.encodeToString(mac.doFinal(data.getBytes()), 0, 20, 0);
	    }

	    public String getAuthorizationHeader(String baseURL) throws InvalidKeyException,
	            SignatureException, NoSuchAlgorithmException {

	        String authorization = "OAuth realm=\"" + baseURL + "\"";
	        authorization += ",q=\"" + query + "\"";
	        authorization += ",oauth_consumer_key=\"" + CONSUMER_KEY + "\"";
	        authorization += ",oauth_signature_method=\"HMAC-SHA1\"";
	        authorization += ",oauth_timestamp=\"" + timeStamp + "\"";
	        authorization += ",oauth_nonce=\"" + nonce + "\"";
	        authorization += ",oauth_version=\"1.0\"";
	        authorization += ",oauth_signature=\"" + URLEncoder.encode(getSignature(baseURL)) + "\"";
	        return authorization;
	    }

	    private String getSignatureGame(String baseURL) throws SignatureException,
	            NoSuchAlgorithmException, InvalidKeyException {
	        String data = getBaseSignature(baseURL);
	        String key = URLEncoder.encode("123456") + "&";
	        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
	        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
	        mac.init(signingKey);

	        return Base64.encodeToString(mac.doFinal(data.getBytes()), 0, 20, 0);
	    }

	    public String getAuthorizationHeaderGame(String baseURL) throws InvalidKeyException,
	            SignatureException, NoSuchAlgorithmException {

	        String authorization = "OAuth realm=\"" + baseURL + "\"";
	        authorization += ",q=\"" + query + "\"";
	        authorization += ",oauth_consumer_key=\"" + CONSUMER_KEY + "\"";
	        authorization += ",oauth_signature_method=\"HMAC-SHA1\"";
	        authorization += ",oauth_timestamp=\"" + timeStamp + "\"";
	        authorization += ",oauth_nonce=\"" + nonce + "\"";
	        authorization += ",oauth_version=\"1.0\"";
	        authorization += ",oauth_signature=\"" + URLEncoder.encode(getSignatureGame(baseURL)) + "\"";
	        return authorization;
	    }

}
