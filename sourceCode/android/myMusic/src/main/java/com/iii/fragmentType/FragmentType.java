package com.iii.fragmentType;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WSgetType;
import com.iii.mymusic.R;

public class FragmentType extends Fragment {
	View view;
	EntityType type;
	ArrayList<EntityType> arrType = null;
	ArrayAdapter<EntityType> adapterType = null;
	GridView grvType;

	// interface type
	public static customButtonTypeListenner customTypeListenner;

	public interface customButtonTypeListenner {
		public void OnButtonTypeClickListenner();
	}

	public void setButtonBuyListenner(customButtonTypeListenner listener) {
		customTypeListenner = listener;
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.popup_type, container, false);
		grvType = (GridView) view.findViewById(R.id.grvType);
		arrType = new ArrayList<EntityType>();
		adapterType = new AdapterType(getActivity(), R.layout.custom_item_type, arrType);
		grvType.setAdapter(adapterType);
		grvType.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				customTypeListenner.OnButtonTypeClickListenner();
			}
		});
		getType();
		return view;
	}

	private void getType() {
		// TODO Auto-generated method stub
		try {
			UltilFunction.startAsyncTask2(new WSgetType(view.getContext()));
			WSgetType.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(android.os.Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
							try {
								JSONObject jsonRootObject = new JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = jsonRootObject.optJSONArray("records");
								for (int i = 0; i < jsonArray.length(); i++) {
									JSONObject jsonObject = jsonArray.getJSONObject(i);
									int id = Integer.parseInt(jsonObject.opt("idtype").toString());
									String name = jsonObject.optString("nametype").toString();
									type = new EntityType(id, name);
									arrType.add(type);
								}
								adapterType.notifyDataSetChanged();
							} catch (Exception e) {

							}
						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
