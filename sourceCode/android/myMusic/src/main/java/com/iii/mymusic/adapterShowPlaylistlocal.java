package com.iii.mymusic;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iii.fragmentNCT.MusicItem;

public class adapterShowPlaylistlocal extends ArrayAdapter<MusicItem>{
	Activity context;
	ArrayList<MusicItem> arrSong = null;
	int rs_layout;

	public adapterShowPlaylistlocal(Activity context, int resource,
			ArrayList<MusicItem> arrSong) {
		super(context, resource, arrSong);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrSong = arrSong;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.tvshowPlaylist = (TextView) convertView
					.findViewById(R.id.tvType);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final MusicItem song = getItem(position);
		holder.tvshowPlaylist.setText(song.getTitle());
		return convertView;
	}

	static class ViewHolder {
		TextView tvshowPlaylist;

	}

}
