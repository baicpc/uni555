package com.iii.Ultils;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.iii.Webservice.WSgetPlaylistbyUserId;
import com.iii.Webservice.WSinsertSongtoPlaylist;
import com.iii.Webservice.WSupdatecounterBook;
import com.iii.Webservice.WSupdatecounterNews;
import com.iii.Webservice.WSupdatecounterPlaylist;
import com.iii.Webservice.WSupdatecounterSong;
import com.iii.Webservice.WSupdatecounterStory;
import com.iii.Webservice.WSupdatecounterVideo;
import com.iii.fragmentAudioBooks.AdapterAudioBook;
import com.iii.fragmentHome.AdapterHome;
import com.iii.fragmentNCT.MusicItem;
import com.iii.fragmentNews.AdapterNews;
import com.iii.fragmentPlaylist.FragmentPlaylist;
import com.iii.fragmentSong.AdapterSong;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentStory.AdapterStory;
import com.iii.fragmentType.EntityType;
import com.iii.fragmentVideo.AdapterVideo;
import com.iii.fragmentVideo.EntityVideo;
import com.iii.fragmentVideo.FragmentVideo;
import com.iii.framentDialog.AppProgressDialog;
import com.iii.musiccontrol.Control;
import com.iii.musicservice.MusicService;
import com.iii.mymusic.R;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class Ultilities {
    public static String milisecondToTimer(long miliseconds) {
        String finalTimerString = "";
        String finalMinutesString = "";
        String secondsString = "";
        int hours = (int) (miliseconds / (1000 * 60 * 60));
        int minutes = (int) (miliseconds % (1000 * 60 * 60) / (1000 * 60));
        int seconds = (int) (miliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000);
        if (hours > 0)
            finalTimerString = hours + ":";
        if (minutes < 10)
            finalMinutesString = "0" + minutes;
        else
            finalMinutesString = "" + minutes;
        if (seconds < 10)
            secondsString = "0" + seconds;
        else
            secondsString = "" + seconds;
        finalTimerString = finalTimerString + finalMinutesString + ":" + secondsString;
        return finalTimerString;
    }

    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;
        long currenSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);
        percentage = (((double) currenSeconds) / totalSeconds) * 100;
        return percentage.intValue();
    }

    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);
        return currentDuration * 1000;
    }

    public static void addSong(ArrayList<EntitySong> from, ArrayList<EntitySong> to) {
        if (to.size() > 0)
            to.clear();
        for (int i = 0; i < from.size(); i++) {
            to.add(from.get(i));
        }
    }

    public static void addSonglocal(ArrayList<MusicItem> from, ArrayList<MusicItem> to) {
        if (to.size() > 0)
            to.clear();
        for (int i = 0; i < from.size(); i++) {
            to.add(from.get(i));
        }
    }

    public static ArrayList<MusicItem> listOfSongs(Context context) {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri, null, MediaStore.Audio.Media.IS_MUSIC + " != 0", null, null);
        ArrayList<MusicItem> listOfSongs = new ArrayList<MusicItem>();
        c.moveToFirst();
        while (c.moveToNext()) {
            MusicItem musicData = new MusicItem();
            String title = c.getString(c.getColumnIndex(MediaStore.Audio.Media.TITLE));
            String data = c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA));
            String artist = c.getString(c.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            musicData.setTitle(title);
            musicData.setPath(data);
            musicData.setArtist(artist);
            listOfSongs.add(musicData);
        }
        c.close();
        Log.d("SIZE", "SIZE: " + listOfSongs.size());
        return listOfSongs;
    }

    // --PROGRESS DIALOG--//
    public static void showProgress(Context ctx) {
        Constant.PROGRESS_DIALOG = AppProgressDialog.ctor(ctx);
        Constant.PROGRESS_DIALOG.show();
        Log.i("PROGRESS DIALOG: ", "SHOW");
    }

    public static void hideProgress(Context ctx) {
        if (Constant.PROGRESS_DIALOG != null) {
            if (Constant.PROGRESS_DIALOG.isShowing()) {
                Constant.PROGRESS_DIALOG.hide();
                Log.i("PROGRESS DIALOG: ", "HIDE");
            }
        }
    }

    // --HIDE KEYBOARD--//
    public static void hideKeyBoard(Context context, View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    // --SHOW KEYBOARD--//
    public static void showKeyboard(View v, Context context) {
        v.requestFocusFromTouch();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
    }

    // --SHOW VOLUME CONTROL--//
    public static void showVolumeControl(Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI);
    }

    // --CHECK SERVICE RUNNING--//
    public static boolean isServiceRunning(String servicename, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (servicename.equals(service.service.getClassName()))
                return true;
        }
        return false;
    }

    // --START SERVICE--//
    public static void startMusicService(Context context) {
        context.startService(new Intent(context, MusicService.class));
    }

    // --STOP SERVICE--//
    public static void stopMusicService(Context context) {
        context.stopService(new Intent(context, MusicService.class));
    }

    public static void updateNextPrevious(String type) {
        if (Constant.SONG_LIST.size() > 1) {
            if (Constant.REPEAT_SONG.equals("RANDOM")) {
                int newSong = Constant.SONG_INDEX;
                while (newSong == Constant.SONG_INDEX) {
                    Random rand = new Random();
                    newSong = rand.nextInt(Constant.SONG_LIST.size());
                }
                Constant.SONG_INDEX = newSong;
            } else {
                if (type.equals(Control.NEXT)) {
                    if (Constant.SONG_INDEX < Constant.SONG_LIST.size() - 1)
                        Constant.SONG_INDEX++;
                    else
                        Constant.SONG_INDEX = 0;
                } else {
                    if (Constant.SONG_INDEX > 0)
                        Constant.SONG_INDEX--;
                    else
                        Constant.SONG_INDEX = Constant.SONG_LIST.size() - 1;
                }
            }
        }
    }

    public static void setListMusic(ArrayList<EntitySong> lists) {
        if (Constant.SONG_LIST.size() > 0) {
            Constant.SONG_LIST.clear();
        }
        if (lists.size() > 0)
            for (int i = 0; i < lists.size(); i++) {
                Constant.SONG_LIST.add(lists.get(i));
            }
    }

    public static void setListAudiobook(ArrayList<EntitySong> lists) {
        if (Constant.SONG_LIST.size() > 0) {
            Constant.SONG_LIST.clear();
        }
        if (lists.size() > 0)
            for (int i = 0; i < lists.size(); i++) {
                Constant.SONG_LIST.add(lists.get(i));
            }
    }

    public static void setListNews(ArrayList<EntitySong> lists) {
        if (Constant.SONG_LIST.size() > 0) {
            Constant.SONG_LIST.clear();
        }
        if (lists.size() > 0)
            for (int i = 0; i < lists.size(); i++) {
                Constant.SONG_LIST.add(lists.get(i));
            }
    }

    public static void setListStory(ArrayList<EntitySong> lists) {
        if (Constant.SONG_LIST.size() > 0) {
            Constant.SONG_LIST.clear();
        }
        if (lists.size() > 0)
            for (int i = 0; i < lists.size(); i++) {
                Constant.SONG_LIST.add(lists.get(i));
            }
    }

    public static void setOneSongListMusic(EntitySong song) {
        if (Constant.SONG_LIST.size() > 0) {
            Constant.SONG_LIST.clear();
        }
        Constant.SONG_LIST.add(song);
    }

    public static void addSongToPlaylist(EntitySong song, Context context) {
        if (Constant.SONG_LIST.size() > 0) {
            boolean add = true;
            for (int i = 0; i < Constant.SONG_LIST.size(); i++) {
                if (song.getName().equals(Constant.SONG_LIST.get(i).getName()))
                    add = false;
            }
            if (add) {
                Constant.SONG_LIST.add(song);
                Toast.makeText(context, context.getString(R.string.text_song_add_to_playlist), Toast.LENGTH_SHORT)
                        .show();
                return;
            } else {
                Toast.makeText(context, context.getString(R.string.text_song_added), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        Constant.SONG_LIST.add(song);
        Toast.makeText(context, context.getString(R.string.text_song_add_to_playlist), Toast.LENGTH_SHORT).show();
    }

    // INSERTSONG
    public static void InsertSong(final Context context, int idplaylist, int idsong) {
        try {
            new WSinsertSongtoPlaylist(context, idplaylist, idsong).execute();
            WSinsertSongtoPlaylist.HANDLER_SUCCESS = new Handler() {
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.isSuccess()) {
                            Toast.makeText(context, context.getString(R.string.add_song_to_playlist),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                ;
            };

        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    // UpdateSong
    public static void UpdateSong(final Context context, int id, final AdapterSong adapterSong, final AdapterHome adapterHome,
                                  final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterSong(context, id).execute();
            WSupdatecounterSong.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (WSupdatecounterSong.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterSong.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (adapterHome != null) {
                            adapterHome.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(), context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {
                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }

    //Update book
    public static void UpdateAudioBook(final Context context, int id, final AdapterHome adapterSong,
                                       final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterBook(context, id).execute();
            WSupdatecounterBook.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (WSupdatecounterBook.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterBook.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(),
                                context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {

                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }

    //UpdateStory
    public static void UpdateeStory(final Context context, int id, final AdapterHome adapterSong,
                                    final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterStory(context, id).execute();
            WSupdatecounterStory.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (WSupdatecounterStory.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterStory.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(),
                                context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {

                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }

    //Update book
    public static void UpdateAudioBook(final Context context, int id, final AdapterAudioBook adapterSong, final AdapterHome adapterHome,
                                       final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterBook(context, id).execute();
            WSupdatecounterBook.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
//                    if (WSupdatecounterBook.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterBook.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (adapterHome != null) {
                            adapterHome.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(),
                                context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {

                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
//                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }

    //UpdateNews
    public static void UpdateNews(final Context context, int id, final AdapterNews adapterSong, final AdapterHome adapterHome,
                                  final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterNews(context, id).execute();
            WSupdatecounterNews.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
//                    if (WSupdatecounterNews.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterNews.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (adapterHome != null) {
                            adapterHome.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(), context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {
                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
//                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }

    //UpdateStory
    public static void UpdateeStory(final Context context, int id, final AdapterStory adapterSong, final AdapterHome adapterHome,
                                    final ArrayList<EntitySong> arrSong, final int position) {
        try {
            new WSupdatecounterStory(context, id).execute();
            WSupdatecounterStory.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
//                    if (WSupdatecounterStory.RESULT != null) {
                        if (arrSong != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(WSupdatecounterStory.RESULT.getStatus());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            arrSong.get(position).setCounter(count);
                        }
                        if (adapterSong != null) {
                            adapterSong.notifyDataSetChanged();
                        }
                        if (adapterHome != null) {
                            adapterHome.notifyDataSetChanged();
                        }
                        if (Ultilities.isServiceRunning(MusicService.class.getName(),
                                context)) {
                            try {
                                Control.songChange();
                            } catch (Exception e) {

                                Log.e("updateSongToPlay", e.toString());
                            }
                        } else {
                            try {

                                Ultilities.startMusicService(context);
                            } catch (Exception e) {

                                Log.e("updateSongToPlay2", e.toString());
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
//                    }
                }
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("updateSongToPlay3", e.toString());
        }
    }


    // UpdateVideo
    public static void UpdateVideo(Context context, int id, final ArrayList<EntityVideo> arrVideo,
                                   final AdapterVideo adapterVideo, final int position) {
        try {
            UltilFunction.startAsyncTask2(new WSupdatecounterVideo(context, id));
            WSupdatecounterVideo.HANDLER_SUCCESS = new Handler() {
                public void handleMessage(Message msg) {
//                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.getCode().equals(Constant.RESULT_API_SUCCESS)) {
                            if (arrVideo != null) {
                                int count = 0;
                                try {
                                    count = Integer.parseInt(Constant.RESULT_API.getStatus());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                arrVideo.get(position).setCounter(count);
                                adapterVideo.notifyDataSetChanged();
//                                Log.d("VIDEO COUNTER: ", Constant.RESULT_API.getStatus());
                            }
                            try {
                                FragmentVideo.HANDLER_UPDATE_COUNTER
                                        .sendMessage(FragmentVideo.HANDLER_UPDATE_COUNTER.obtainMessage());
                            } catch (NullPointerException e) {
                                // TODO: handle exception
                            }
                        }
//                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    // UpdateVideo
    public static void UpdateVideoHome(Context context, int id, final ArrayList<EntityVideo> arrVideo,
                                       final AdapterVideo adapterVideo, final int position) {
        try {
            UltilFunction.startAsyncTask2(new WSupdatecounterVideo(context, id));
            WSupdatecounterVideo.HANDLER_SUCCESS = new Handler() {
                public void handleMessage(Message msg) {
//                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.getCode().equals(Constant.RESULT_API_SUCCESS)) {
                            if (arrVideo != null) {
                                int count = 0;
                                try {
                                    count = Integer.parseInt(Constant.RESULT_API.getStatus());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                arrVideo.get(position).setCounter(count);
                                adapterVideo.notifyDataSetChanged();
//                                Log.d("VIDEO COUNTER: ", Constant.RESULT_API.getStatus());
                            }
                            FragmentVideo.HANDLER_UPDATE_COUNTER
                                    .sendMessage(FragmentVideo.HANDLER_UPDATE_COUNTER.obtainMessage());
                        }
//                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }


    // UpdatePlaylist
    public static void UpdatePlaylist(Context context, int id) {
        try {
            UltilFunction.startAsyncTask2(new WSupdatecounterPlaylist(context, id));
            WSupdatecounterPlaylist.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    try {
                        FragmentPlaylist.HANDLER_UPDATE_COUNT
                                .sendMessage(FragmentPlaylist.HANDLER_UPDATE_COUNT.obtainMessage());
                    } catch (NullPointerException e) {
                        // TODO: handle exception
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }

    // getPlaylistUser
    public static void getPlaylistUser(final ArrayAdapter<EntityType> adapterType, final ArrayList<EntityType> arrType,
                                       Context context, int userid) {
        // TODO Auto-generated method stub
        try {
            new WSgetPlaylistbyUserId(context, userid).execute();
            WSgetPlaylistbyUserId.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
                            String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
                            try {
                                JSONObject jsonRootObject = new JSONObject(strJson);
                                // Get the instance of JSONArray that contains
                                // JSONObjects
                                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    int id = Integer.parseInt(jsonObject.opt("idplaylist").toString());
                                    String name = jsonObject.optString("playlist").toString();
                                    EntityType type = new EntityType(id, name);
                                    arrType.add(type);
                                }
                                adapterType.notifyDataSetChanged();
                            } catch (Exception e) {

                            }
                        }
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            e.toString();
        }
    }
//// getPlaylistUser
//    public static void getPlaylistNewsUser(final ArrayAdapter<EntityTypeNews> adapterType, final ArrayList<EntityTypeNews> arrType,
//                                       Context context, int userid) {
//        // TODO Auto-generated method stub
//        try {
//            new WSgetPlaylistbyUserId(context, userid).execute();
//            WSgetPlaylistbyUserId.HANDLER_SUCCESS = new Handler() {
//                @Override
//                public void handleMessage(Message msg) {
//                    if (Constant.RESULT_API != null) {
//                        if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
//                            String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
//                            try {
//                                JSONObject jsonRootObject = new JSONObject(strJson);
//                                // Get the instance of JSONArray that contains
//                                // JSONObjects
//                                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                    String id = jsonObject.opt("idplaylist").toString();
//                                    String name = jsonObject.optString("playlist").toString();
//                                    EntityTypeNews type = new EntityTypeNews(id, name);
//                                    arrType.add(type);
//                                }
//                                adapterType.notifyDataSetChanged();
//                            } catch (Exception e) {
//
//                            }
//                        }
//                    }
//                }
//
//                ;
//            };
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.toString();
//        }
//    }

    public static boolean isConnectedToServer(String url, int timeout) {
        try {
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
