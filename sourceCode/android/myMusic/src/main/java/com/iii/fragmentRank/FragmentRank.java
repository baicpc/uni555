package com.iii.fragmentRank;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Webservice.WSgetCountry;
import com.iii.Webservice.WSgetXBXH;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ListView;

public class FragmentRank extends Fragment {
	View view;
	RankEntity rank;
	EntityType type;
	ImageView ivKhac, ivAvataSong, ivAvataPlaylist, ivAvataVideo;
	TextView textSong1, textSong2, textSong3, textPlaylist1, textPlaylist2, textPlaylist3, textVideo1, textVideo2,
			textVideo3;
	PopupWindow popUpWindow;
	TextView textRank, tvsongtop1, tvsongtop2, tvsongtop3, tvplaylisttop1, tvplaylisttop2, tvplaylisttop3, tvvideotop1,
			tvvideotop2, tvvideotop3;
	LinearLayout layoutSong, layoutPlaylist, layoutVideo;
	String imgsong = "";
	String imgvideo = "";
	String imgplaylist = "";
	static int idcountry = 1;
	ImageView imageOther;
	String titleOther = "";

	private static FragmentRank fragment;

	public static FragmentRank getInstants() {
		if (fragment == null)
			fragment = new FragmentRank();
		return fragment;
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.e("kakakka", "no vao day");
		view = inflater.inflate(R.layout.fragment_chart, container, false);
		ivKhac = (ImageView) view.findViewById(R.id.tvKhacBXH);
		textRank = (TextView) view.findViewById(R.id.text_rank_title);
		tvsongtop1 = (TextView) view.findViewById(R.id.tvsongtop1);
		tvsongtop2 = (TextView) view.findViewById(R.id.tvsongtop2);
		tvsongtop3 = (TextView) view.findViewById(R.id.tvsongtop3);
		tvplaylisttop1 = (TextView) view.findViewById(R.id.tvplaylisttop1);
		tvplaylisttop2 = (TextView) view.findViewById(R.id.tvplaylisttop2);
		tvplaylisttop3 = (TextView) view.findViewById(R.id.tvplaylisttop3);
		tvvideotop1 = (TextView) view.findViewById(R.id.tvvideotop1);
		tvvideotop2 = (TextView) view.findViewById(R.id.tvvideotop2);
		tvvideotop3 = (TextView) view.findViewById(R.id.tvvideotop3);
		ivAvataSong = (ImageView) view.findViewById(R.id.ivSong_rank);
		ivAvataPlaylist = (ImageView) view.findViewById(R.id.ivPlaylist_rank);
		ivAvataVideo = (ImageView) view.findViewById(R.id.ivVideo_rank);
		layoutSong = (LinearLayout) view.findViewById(R.id.layout_Song);
		layoutPlaylist = (LinearLayout) view.findViewById(R.id.layout_Playlist);
		layoutVideo = (LinearLayout) view.findViewById(R.id.layout_video);
		imageOther = (ImageView) view.findViewById(R.id.tvKhacBXH);
		textSong1 = (TextView) view.findViewById(R.id.text_song_tt1);
		textSong2 = (TextView) view.findViewById(R.id.text_song_tt2);
		textSong3 = (TextView) view.findViewById(R.id.text_song_tt3);
		textPlaylist1 = (TextView) view.findViewById(R.id.text_playlist_tt1);
		textPlaylist2 = (TextView) view.findViewById(R.id.text_playlist_tt2);
		textPlaylist3 = (TextView) view.findViewById(R.id.text_playlist_tt3);
		textVideo1 = (TextView) view.findViewById(R.id.text_video_tt1);
		textVideo2 = (TextView) view.findViewById(R.id.text_video_tt2);
		textVideo3 = (TextView) view.findViewById(R.id.text_video_tt3);

		if (idcountry == 1) {
			titleOther = getResources().getString(R.string.vietnam);
		}

		layoutSong.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fragmentItemPlaylist fragItem = new fragmentItemPlaylist();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 1);
				bundle.putInt("idcountry", idcountry);
				bundle.putString("imgsong", imgsong);
				fragItem.setArguments(bundle);
				transationToFragment(fragItem);

			}
		});
		layoutPlaylist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fragmentItemPlaylist fragItem = new fragmentItemPlaylist();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 2);
				bundle.putInt("idcountry", idcountry);
				bundle.putString("imgplaylist", imgplaylist);
				fragItem.setArguments(bundle);
				transationToFragment(fragItem);
			}
		});
		layoutVideo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fragmentItemPlaylist fragItem = new fragmentItemPlaylist();
				Bundle bundle = new Bundle();
				bundle.putInt("cusor", 3);
				bundle.putInt("idcountry", idcountry);
				bundle.putString("imgvideo", imgvideo);
				fragItem.setArguments(bundle);
				transationToFragment(fragItem);
			}
		});


		LinearLayout layoutOther = (LinearLayout) view.findViewById(R.id.layout_other);
		layoutOther.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (popUpWindow == null) {
					showPopup(v);
				} else {
					if (popUpWindow.isShowing()) {
						popUpWindow.dismiss();
					} else {
						showPopup(v);
					}
				}
			}
		});

		// hide layout
		layoutPlaylist.setVisibility(View.INVISIBLE);
		layoutSong.setVisibility(View.INVISIBLE);
		layoutVideo.setVisibility(View.INVISIBLE);
		textRank.setText(titleOther);
		
		if (!UltilFunction.isConnectingToInternet(getActivity())) {
			Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
		}
		else {

			getRank(idcountry);
		}
		return view;
	}

	private void showPopup(View v) {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_country, null);
		final ListView lvCountry = (ListView) layout.findViewById(R.id.lvPopUp_Country);
		ArrayList<EntityType> arrType = new ArrayList<EntityType>();
		ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_menu_popup, arrType);
		getType(arrType, adapterType);
		lvCountry.setAdapter(adapterType);
		popUpWindow = new PopupWindow(layout, (int) ((2 * Constant.widthPixels) / 10), LayoutParams.WRAP_CONTENT);
		int location[] = new int[2];
		v.getLocationOnScreen(location);
		popUpWindow.setOutsideTouchable(true);
		imageOther.setImageResource(R.drawable.btn_choose);
		popUpWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				imageOther.setImageResource(R.drawable.btn_chosesdown);
			}
		});
		popUpWindow.setBackgroundDrawable(new BitmapDrawable());
		popUpWindow.showAtLocation(layout, Gravity.NO_GRAVITY, Constant.widthPixels - popUpWindow.getWidth() - 2,
				location[1] + v.getHeight());
		popUpWindow.setFocusable(true);
		popUpWindow.update();
		lvCountry.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				popUpWindow.dismiss();
				type = (EntityType) parent.getItemAtPosition(position);
				idcountry = type.getIdtype();
				getRank(idcountry);
				textRank.setText(type.getNametype());
				titleOther = type.getNametype();
			}
		});

	}

	public void getRank(int id) {
		try {
			UltilFunction.startAsyncTask2(new WSgetXBXH(getActivity(), id));
			WSgetXBXH.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						addToListSong(Constant.RESULT_API);
						addToListVideo(Constant.RESULT_API);
						addToListPlaylist(Constant.RESULT_API);
					}
				}
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void getType(final ArrayList<EntityType> arrType, final ArrayAdapter<EntityType> adapterType) {
		// TODO Auto-generated method stub
		try {
			UltilFunction.startAsyncTask2(new WSgetCountry(getActivity()));
			WSgetCountry.HANDLER_SUCCESS = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (Constant.RESULT_API != null) {
						if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
							String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
							try {
								JSONObject jsonRootObject = new JSONObject(strJson);
								// Get the instance of JSONArray that contains
								// JSONObjects
								JSONArray jsonArray = jsonRootObject.optJSONArray("records");
								for (int i = 0; i < jsonArray.length(); i++) {
									JSONObject jsonObject = jsonArray.getJSONObject(i);
									int id = Integer.parseInt(jsonObject.opt("idcountry").toString());
									String name = jsonObject.optString("name").toString();
									Log.e("country", name);
									type = new EntityType(id, name);
									arrType.add(type);
								}
								adapterType.notifyDataSetChanged();
							} catch (Exception e) {

							}
						}
					}
				};
			};
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void addToListSong(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			// String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				// JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = new JSONArray(resultAPI.getSong());
				Log.d("JSON Array: ", jsonArray.toString());
				if (jsonArray.length() >= 3) {
					addSong(jsonArray, 3);
				} else if (jsonArray.length() <= 0) {
					updateSong(0);
				} else {
					addSong(jsonArray, jsonArray.length());
				}
			} catch (Exception e) {
				e.toString();
			}
		}
	}

	public void addToListVideo(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			// String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				// JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = new JSONArray(resultAPI.getVideo());
				Log.d("JSON Array: ", jsonArray.toString());
				if (jsonArray.length() >= 3) {
					addVideo(jsonArray, 3);
				} else if (jsonArray.length() <= 0) {
					updateVideo(0);
				} else {
					addVideo(jsonArray, jsonArray.length());
				}
			} catch (Exception e) {
				e.toString();
			}
		}
	}

	public void addToListPlaylist(ResultAPI resultAPI) {
		if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
			// String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
			try {
				// JSONObject jsonRootObject = new JSONObject(strJson);
				// Get the instance of JSONArray that contains JSONObjects
				JSONArray jsonArray = new JSONArray(resultAPI.getPlaylist());
				Log.d("JSON Array: ", jsonArray.toString());
				if (jsonArray.length() >= 3) {
					addPlaylist(jsonArray, 3);
				} else if (jsonArray.length() <= 0) {
					updatePlaylist(0);
				} else {
					addPlaylist(jsonArray, jsonArray.length());
				}
			} catch (Exception e) {
				e.toString();
			}
		}
	}

	private void transationToFragment(Fragment fragment) {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.main_fragment, fragment, fragment.getClass().getSimpleName());
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
	}

	private void addSong(JSONArray jsonArray, int length) {
		ArrayList<RankEntity> arrRank = new ArrayList<RankEntity>();
		for (int j = 0; j < length; j++) {
			try {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int id = Integer.parseInt(jsonObject.opt("idsong").toString());
				String name = jsonObject.optString("namesong").toString();
				String linksong = jsonObject.opt("linksong").toString();
				String image = jsonObject.opt("image").toString();
				String lyric = jsonObject.opt("lyric").toString();
				String nameart = jsonObject.opt("artistname").toString();
				String filepath = jsonObject.opt("filepath").toString();
				arrRank.add(new RankEntity(id, name, linksong, image, lyric, nameart, filepath, RankEntity.TYPE_SONG));
				if (j == 0) {
					tvsongtop1.setText(name);
				} else if (j == 1) {
					tvsongtop2.setText(name);
				} else if (j == 2) {
					tvsongtop3.setText(name);
				}
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
		updateSong(length);
		Random rand = new Random();
		int randomPos = rand.nextInt(length);
		String imageFinal = arrRank.get(randomPos).getImage();
		imgsong = imageFinal;
		Picasso.with(getActivity()).load(Constant.URL_IMG + "songs/tb/" + imageFinal).placeholder(R.drawable.down)
				.into(ivAvataSong);
	}

	private void addVideo(JSONArray jsonArray, int length) {
		ArrayList<RankEntity> arrRank = new ArrayList<RankEntity>();
		try {
			for (int j = 0; j < length; j++) {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int id = Integer.parseInt(jsonObject.opt("idvideo").toString());
				String name = jsonObject.optString("namevideo").toString();
				String linkvideo = jsonObject.opt("linkurl").toString();
				String image = jsonObject.opt("image").toString();
				String nameart = jsonObject.opt("artistname").toString();
				String filepath = jsonObject.opt("filepath").toString();
				rank = new RankEntity(id, name, linkvideo, image, nameart, filepath, RankEntity.TYPE_VIDEO);
				arrRank.add(rank);
				if (j == 0) {
					tvvideotop1.setText(name);
				} else if (j == 1) {
					tvvideotop2.setText(name);
				} else if (j == 2) {
					tvvideotop3.setText(name);
				}
			}
			updateVideo(length);
			Random rand = new Random();
			int randPos = rand.nextInt(length);
			String imageFinal = arrRank.get(randPos).getImage();
			imgvideo = imageFinal;
			Picasso.with(getActivity()).load(Constant.URL_IMG + "videos/tb/" + imageFinal).placeholder(R.drawable.down)
					.into(ivAvataVideo);
		} catch (JSONException e) {
			// TODO: handle exception
		}
	}

	private void addPlaylist(JSONArray jsonArray, int length) {
		ArrayList<RankEntity> arrRank = new ArrayList<RankEntity>();
		try {
			for (int j = 0; j < length; j++) {
				JSONObject jsonObject = jsonArray.getJSONObject(j);
				int id = Integer.parseInt(jsonObject.opt("idplaylist").toString());
				String name = jsonObject.optString("playlist").toString();
				String image = jsonObject.opt("image").toString();
				arrRank.add(new RankEntity(id, name, image, RankEntity.TYPE_PLAYLIST));
				if (j == 0) {
					tvplaylisttop1.setText(name);
				} else if (j == 1) {
					tvplaylisttop2.setText(name);
				} else if (j == 2) {
					tvplaylisttop3.setText(name);
				}
			}
			updatePlaylist(length);
			Random rand = new Random();
			int randPos = rand.nextInt(length);
			String imageFinal = arrRank.get(randPos).getImage();
			imgplaylist = imageFinal;
			Picasso.with(getActivity()).load(Constant.URL_IMG + "playlists/tb/" + imageFinal)
					.placeholder(R.drawable.down).into(ivAvataPlaylist);
		} catch (JSONException e) {
			// TODO: handle exception
		}
	}

	private void updateSong(int index) {
		if (index == 1) {
			textSong1.setVisibility(View.VISIBLE);
			textSong2.setVisibility(View.INVISIBLE);
			textSong3.setVisibility(View.INVISIBLE);
			tvsongtop1.setVisibility(View.VISIBLE);
			tvsongtop2.setVisibility(View.INVISIBLE);
			tvsongtop3.setVisibility(View.INVISIBLE);
			layoutSong.setVisibility(View.VISIBLE);
		} else if (index == 2) {
			textSong1.setVisibility(View.VISIBLE);
			textSong2.setVisibility(View.VISIBLE);
			textSong3.setVisibility(View.INVISIBLE);
			tvsongtop1.setVisibility(View.VISIBLE);
			tvsongtop2.setVisibility(View.VISIBLE);
			tvsongtop3.setVisibility(View.INVISIBLE);
			layoutSong.setVisibility(View.VISIBLE);
		} else if (index == 3) {
			textSong1.setVisibility(View.VISIBLE);
			textSong2.setVisibility(View.VISIBLE);
			textSong3.setVisibility(View.VISIBLE);
			tvsongtop1.setVisibility(View.VISIBLE);
			tvsongtop2.setVisibility(View.VISIBLE);
			tvsongtop3.setVisibility(View.VISIBLE);
			layoutSong.setVisibility(View.VISIBLE);
		} else {
			layoutSong.setVisibility(View.INVISIBLE);
		}
	}

	private void updatePlaylist(int index) {
		if (index == 1) {
			textPlaylist1.setVisibility(View.VISIBLE);
			textPlaylist2.setVisibility(View.INVISIBLE);
			textPlaylist3.setVisibility(View.INVISIBLE);
			tvplaylisttop1.setVisibility(View.VISIBLE);
			tvplaylisttop2.setVisibility(View.INVISIBLE);
			tvplaylisttop3.setVisibility(View.INVISIBLE);
			layoutPlaylist.setVisibility(View.VISIBLE);
		} else if (index == 2) {
			textPlaylist1.setVisibility(View.VISIBLE);
			textPlaylist2.setVisibility(View.VISIBLE);
			textPlaylist3.setVisibility(View.INVISIBLE);
			tvplaylisttop1.setVisibility(View.VISIBLE);
			tvplaylisttop2.setVisibility(View.VISIBLE);
			tvplaylisttop3.setVisibility(View.INVISIBLE);
			layoutPlaylist.setVisibility(View.VISIBLE);
		} else if (index == 3) {
			textPlaylist1.setVisibility(View.VISIBLE);
			textPlaylist2.setVisibility(View.VISIBLE);
			textPlaylist3.setVisibility(View.VISIBLE);
			tvplaylisttop1.setVisibility(View.VISIBLE);
			tvplaylisttop2.setVisibility(View.VISIBLE);
			tvplaylisttop3.setVisibility(View.VISIBLE);
			layoutPlaylist.setVisibility(View.VISIBLE);
		} else {
			layoutPlaylist.setVisibility(View.INVISIBLE);
		}
	}

	private void updateVideo(int index) {
		if (index == 1) {
			textVideo1.setVisibility(View.VISIBLE);
			textVideo2.setVisibility(View.INVISIBLE);
			textVideo3.setVisibility(View.INVISIBLE);
			tvvideotop1.setVisibility(View.VISIBLE);
			tvvideotop2.setVisibility(View.INVISIBLE);
			tvvideotop3.setVisibility(View.INVISIBLE);
			layoutVideo.setVisibility(View.VISIBLE);
		} else if (index == 2) {
			textVideo1.setVisibility(View.VISIBLE);
			textVideo2.setVisibility(View.VISIBLE);
			textVideo3.setVisibility(View.INVISIBLE);
			tvvideotop1.setVisibility(View.VISIBLE);
			tvvideotop2.setVisibility(View.VISIBLE);
			tvvideotop3.setVisibility(View.INVISIBLE);
			layoutVideo.setVisibility(View.VISIBLE);
		} else if (index == 3) {
			textVideo1.setVisibility(View.VISIBLE);
			textVideo2.setVisibility(View.VISIBLE);
			textVideo3.setVisibility(View.VISIBLE);
			tvvideotop1.setVisibility(View.VISIBLE);
			tvvideotop2.setVisibility(View.VISIBLE);
			tvvideotop3.setVisibility(View.VISIBLE);
			layoutVideo.setVisibility(View.VISIBLE);
		} else {
			layoutVideo.setVisibility(View.INVISIBLE);
		}
	}

}
