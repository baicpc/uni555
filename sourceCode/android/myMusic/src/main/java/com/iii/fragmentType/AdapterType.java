package com.iii.fragmentType;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iii.Ultils.Constant;
import com.iii.mymusic.R;

public class AdapterType extends ArrayAdapter<EntityType> {
	Activity context;
	EntityType type;
	ArrayList<EntityType> arrType = null;
	int rs_layout;
	int width;

	public AdapterType(Activity context, int resource, ArrayList<EntityType> arrType) {
		super(context, resource, arrType);
		this.context = context;
		this.arrType = arrType;
		this.rs_layout = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(rs_layout, null);
			holder = new ViewHolder();
			holder.tvType = (TextView) convertView.findViewById(R.id.tvType);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		width = holder.tvType.getWidth();
		type = arrType.get(position);
		holder.tvType.setText(type.getNametype());
		return convertView;
	}

	public class ViewHolder {
		TextView tvType;
	}

}
