package com.iii.Ultils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Helper {
	public static void getListViewSize(ListView mListView) {
		ListAdapter mListAdapter = mListView.getAdapter();
		if(mListAdapter == null)
			return;
		int totalHeight = 0;
		for(int size = 0; size < mListAdapter.getCount(); size ++){
			View listItem = mListAdapter.getView(size, null, mListView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
			ViewGroup.LayoutParams params = mListView.getLayoutParams();
			params.height = totalHeight + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
			mListView.setLayoutParams(params);
					
		}
    }
	public static void getListViewRank(ListView mListView) {
		ListAdapter mListAdapter = mListView.getAdapter();
		if(mListAdapter == null)
			return;
		int totalHeight = 0;
		for(int size = 0; size < 4; size ++){
			View listItem = mListAdapter.getView(0, null, mListView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
			ViewGroup.LayoutParams params = mListView.getLayoutParams();
			params.height = totalHeight + (mListView.getDividerHeight() * (3));
			mListView.setLayoutParams(params);
					
		}
    }


}
