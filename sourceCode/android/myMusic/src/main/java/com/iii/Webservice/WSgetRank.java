package com.iii.Webservice;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import com.iii.Config.ConfigurationServer;
import com.iii.Config.ConfigurationWS;
import com.iii.Helper.Oauth1;
import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.Ultilities;

public class WSgetRank extends AsyncTask<Void, Void, Void> {
	public static Handler HANDLER_SUCCESS;
	private ConfigurationWS mWS;
	private Context mContext;

	public WSgetRank(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onPreExecute() {
		Ultilities.showProgress(mContext);
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		mWS = new ConfigurationWS(mContext);
		try {
			/* gui len server */
			ResultAPI result = new ResultAPI();
			String URLAddClient = ConfigurationServer.URLServer + "api/rank";
			JSONObject json = new JSONObject();

			Oauth1 oau = new Oauth1();
			String stroau = oau.getAuthorizationHeader(URLAddClient);
			String jsonData = mWS.getDataJson(URLAddClient, json, "posts", stroau);

			JSONObject jsonObject = new JSONObject(jsonData);

			if (jsonObject != null) {
				result.setCode(jsonObject.getString("code"));
				result.setMessage(jsonObject.getString("message"));
				result.setVideo(jsonObject.getString("Video"));
				result.setSong(jsonObject.getString("Song"));
				result.setPlaylist(jsonObject.getString("Playlist"));
				Constant.RESULT_API = result;
			}
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Ultilities.hideProgress(mContext);
		try {
			HANDLER_SUCCESS.sendMessage(HANDLER_SUCCESS.obtainMessage());
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
}
