package com.iii.fragmentSong;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.iii.fragmentSong.EntitySong;
import com.iii.mymusic.R;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class DownloadSongMultiFileAsync extends AsyncTask<EntitySong, String, String> {
	Context context;
	String filename;

	public DownloadSongMultiFileAsync(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		Toast.makeText(context, context.getString(R.string.download_song), Toast.LENGTH_SHORT).show();
	}

	@Override
	protected String doInBackground(EntitySong... aurl) {
		int count = 0;
		downloadFile(count, aurl);
		return null;

	}

	protected void onProgressUpdate(String... progress) {
		Log.d("ANDRO_ASYNC", progress[0]);
	}

	@Override
	protected void onPostExecute(String unused) {
		Log.d("DOWN SC", "");
		Toast.makeText(context, "Downloaded to:\n" + android.os.Environment.getExternalStorageDirectory() + "/NCT/",
				Toast.LENGTH_LONG).show();

	}

	private synchronized void downloadFile(int count, EntitySong... aurl) {

		try {
			File cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "NCT");
			if (!cacheDir.exists())
				cacheDir.mkdirs();
			File f = null;
			for (int i = 0; i < aurl.length; i++) {
				try {
					Log.e("download", i + "");
					EntitySong song = aurl[i];
					filename = song.getName().replaceAll(" ", "");
					Log.e("download", filename);
					f = new File(cacheDir, filename.trim() + ".mp3");
					String urlStr = "";
					if (!song.getLinksong().toString().equals("")) {

						urlStr = song.getLinksong();
						Log.e("download", urlStr);
					}
					else if (!song.getFilepath().toString().equals("null")) {

						urlStr = song.getFilepath().replaceAll(" ", "%20");
						Log.e("download", urlStr);
					}
					else if (!song.getLinkDriver().toString().equals("null")) {

						urlStr = song.getLinkDriver();
						Log.e("download", urlStr);
					}
					URL url = new URL(urlStr);
					URLConnection connection = url.openConnection();
					connection.connect();

					int lenghtOfFile = connection.getContentLength();
					Log.e("download", "Lenght of file: " + lenghtOfFile);

					InputStream input = new BufferedInputStream(url.openStream());
					OutputStream output = new FileOutputStream(f);

					byte data[] = new byte[1024];

					long total = 0;
					while ((count = input.read(data)) != -1) {
						total += count;
						publishProgress("" + (int) ((total * 100) / lenghtOfFile));
						output.write(data, 0, count);
					}
					Log.e("download", "den dc day");
					output.flush();
					output.close();
					input.close();
					// SCAN FILE

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						Intent mediaScanIntent = new Intent(
								Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
						Uri contentUri = Uri.fromFile(cacheDir);
						mediaScanIntent.setData(contentUri);
						context.sendBroadcast(mediaScanIntent);
					} else {
						context.sendBroadcast(new Intent(
								Intent.ACTION_MEDIA_MOUNTED,
								Uri.parse("file://"
										+ Environment.getExternalStorageDirectory())));
					}
//					context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
//							Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				} catch (Exception e) {
					// TODO: handle exception
					f.delete();
					Log.e("download", e.toString());
				}
			}
		} catch (Exception e) {
			Log.d("ERROR DOWN", e.toString());
		}
	}
}
