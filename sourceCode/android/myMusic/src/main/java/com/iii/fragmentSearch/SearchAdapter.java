package com.iii.fragmentSearch;

import java.util.ArrayList;

import com.iii.fragmentHome.PlaySongInterface;
import com.iii.fragmentSong.EntitySong;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchAdapter extends BaseAdapter {
	ArrayList<SearchEntity> lists;
	LayoutInflater inflater;
	Context context;
	
	public SearchAdapter(Context context, ArrayList<SearchEntity> lists){
		this.lists = lists;
		inflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lists.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHoldler viewHoldler;
		if(convertView == null){
			convertView = inflater.inflate(R.layout.custom_item_search, null);
			viewHoldler = new ViewHoldler();
			viewHoldler.icon=(ImageView) convertView.findViewById(R.id.ivIcoin);
			viewHoldler.title=(TextView) convertView.findViewById(R.id.tvNamesearch);
			convertView.setTag(viewHoldler);
		}else{
			viewHoldler = (ViewHoldler) convertView.getTag();
		}
		final SearchEntity search = lists.get(position);
		if(search.getType() == SearchEntity.TYPE_SONG){
			viewHoldler.icon.setImageResource(R.drawable.ic_song_search);
			viewHoldler.title.setText(search.getTitle());
		}else if(search.getType() == SearchEntity.TYPE_ARTIST){
			viewHoldler.icon.setImageResource(R.drawable.ic_avatar_artist);
			viewHoldler.title.setText(search.getTitle());
		}else if(search.getType() == SearchEntity.TYPE_VIDEO){
			viewHoldler.icon.setImageResource(R.drawable.ic_video_search);
			viewHoldler.title.setText(search.getTitle());
		}
		return convertView;
	}
	static class ViewHoldler{
		ImageView icon;
		TextView title;
	}

}
