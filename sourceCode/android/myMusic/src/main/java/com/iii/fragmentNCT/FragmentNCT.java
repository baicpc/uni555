package com.iii.fragmentNCT;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.fragmentHome.FragmentHome;
import com.iii.fragmentNCT.AdapterMusic.customButtonAddListener;
import com.iii.fragmentNCT.AdapterMusic.customButtonDelListener;
import com.iii.fragmentSong.EntitySong;
import com.iii.musiccontrol.Control;
import com.iii.musicservice.MusicService;
import com.iii.mymusic.MainActivity;
import com.iii.mymusic.R;

public class FragmentNCT extends Fragment implements customButtonDelListener, customButtonAddListener {
	View view;
	EditText etSearch;
	ImageView ivDelsearchlocal;
	ListView lvSonglc;
	AdapterMusic adapterMusic = null;
	ArrayList<EntitySong> arrSong = null;
	ArrayList<EntitySong> tempList;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_nct, container, false);
		lvSonglc = (ListView) view.findViewById(R.id.lvSonglc);
		etSearch = (EditText) view.findViewById(R.id.etSearchlocal);
		ivDelsearchlocal = (ImageView) view.findViewById(R.id.ivDelsearchlocal);
		Bundle bundle = getArguments();
		int cusor = bundle.getInt("cusor");
		switch (cusor) {
		case 1:
			arrSong = GetNCT.listOfSongs(getActivity());
			break;
		case 2:
			arrSong = GetNCT.getMP3();
			break;
		}
		tempList = arrSong;
		Constant.LIST_SEARCH = tempList;
		if (MainActivity.START_FIRST == 1) {
			if (!Ultilities.isServiceRunning(MusicService.class.getName(), getActivity())) {
				Ultilities.setListMusic(tempList);
				if (tempList.size() > 0)
					MainActivity.updateUI();
				MainActivity.START_FIRST++;
			}
		}
		adapterMusic = new AdapterMusic(getActivity(), tempList);
		lvSonglc.setAdapter(adapterMusic);
		adapterMusic.setCustomButtonDelListener(FragmentNCT.this);
		lvSonglc.setEmptyView(view.findViewById(R.id.text_empty));
		lvSonglc.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (tempList.size() > 0) {
					Ultilities.stopMusicService(getActivity());
					Constant.SONG_INDEX = position;
					Constant.CURR_POSITION = 0;
					Ultilities.setListMusic(tempList);
					if (Ultilities.isServiceRunning(MusicService.class.getName(),
							getActivity().getApplicationContext())) {
						Control.songChange();
					} else {
						Ultilities.startMusicService(getActivity().getApplicationContext());
					}
					UltilFunction.updateTextListTitle(getString(R.string.local_music));
				} else {
					Toast.makeText(getActivity(), getString(R.string.popup_playlist_list_empty), Toast.LENGTH_LONG)
							.show();
				}
			}
		});
		ivDelsearchlocal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etSearch.setText("");
			}
		});
		etSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				int textLenght = cs.length();
				tempList = new ArrayList<EntitySong>();
				if (textLenght == 0) {
					tempList = arrSong;
				} else {
					for (EntitySong s : Constant.LIST_SEARCH) {
						if (textLenght <= s.getName().length())
							if (s.getName().toLowerCase().contains(cs.toString().toLowerCase()))
								tempList.add(s);

					}
				}
				adapterMusic = new AdapterMusic(getActivity(), tempList);
				lvSonglc.setAdapter(adapterMusic);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		etSearch.setImeActionLabel("OK", KeyEvent.KEYCODE_ENTER);
		etSearch.setOnKeyListener(onkeyClick);
//		Button btnPlayAll = (Button) view.findViewById(R.id.imgLisAll);
//		btnPlayAll.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if (arrSong.size() > 0) {
//					Constant.SONG_INDEX = 0;
//					Ultilities.setListMusic(arrSong);
//					if (Ultilities.isServiceRunning(MusicService.class.getName(),
//							getActivity().getApplicationContext())) {
//						FragmentHome.songChange(getActivity());
//					} else {
//						Ultilities.startMusicService(getActivity().getApplicationContext());
//					}
//					UltilFunction.updateTextListTitle(getString(R.string.local_music));
//				} else {
//					Toast.makeText(getActivity(), getString(R.string.popup_playlist_list_empty), Toast.LENGTH_LONG)
//							.show();
//				}
//			}
//		});
		// /UltilFunction.hideKeyboard(getActivity());
		return view;
	}

	@Override
	public void onButtonDelClickListener(EntitySong item, int position) {
		popupDelete(item, position);
	}

	public void popupDelete(final EntitySong song, final int position) {
		try {

			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View layout = inflater.inflate(R.layout.popup_delete, null);
			final PopupWindow deletePopup = new PopupWindow(getActivity());
			deletePopup.setContentView(layout);
			deletePopup.setWidth(Constant.widthPixels / 2);
			deletePopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
			deletePopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
			deletePopup.setFocusable(true);
			deletePopup.update();

			final Button buttonCancel = (Button) layout.findViewById(R.id.button_cancel);
			final Button buttonOk = (Button) layout.findViewById(R.id.button_ok);

			buttonCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					deletePopup.dismiss();
				}
			});
			buttonOk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					deletePopup.dismiss();
					String path = song.getFilepath();
					File f = new File(path);
					f.delete();
					Log.d("DELETE FILE: ", path);
					getActivity().sendBroadcast(
							new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(path))));
					tempList.remove(position);
					adapterMusic.notifyDataSetChanged();

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onButtonAddClickListener(EntitySong item, int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewStateRestored(savedInstanceState);
		onCreate(savedInstanceState);
	}
	
	OnKeyListener onkeyClick = new OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				Ultilities.hideKeyBoard(getActivity(), v);
				return true;
			}
			return false;
		}
	};

}
