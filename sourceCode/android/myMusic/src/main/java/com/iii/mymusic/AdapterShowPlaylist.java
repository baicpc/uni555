package com.iii.mymusic;

import java.util.ArrayList;

import com.iii.Ultils.Constant;
import com.iii.fragmentSong.EntitySong;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterShowPlaylist extends ArrayAdapter<EntitySong> {
	Activity context;
	ArrayList<EntitySong> arrSong = null;
	int rs_layout;
	TextView tvshowPlaylist, textNumber;
	ImageView imagePlaying;

	public AdapterShowPlaylist(Activity context, int resource, ArrayList<EntitySong> arrSong) {
		super(context, resource, arrSong);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.rs_layout = resource;
		this.arrSong = arrSong;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = context.getLayoutInflater();
		convertView = inflater.inflate(rs_layout, null);
		tvshowPlaylist = (TextView) convertView.findViewById(R.id.tvType);
		textNumber = (TextView) convertView.findViewById(R.id.text_list_count);
		imagePlaying = (ImageView) convertView.findViewById(R.id.image_song_playing);
		final EntitySong song = getItem(position);
		tvshowPlaylist.setText(song.getName());

		if (position == Constant.SONG_INDEX) {
			imagePlaying.setVisibility(View.VISIBLE);
			textNumber.setVisibility(View.GONE);
			imagePlaying.setBackgroundResource(R.drawable.spin_animation);
			if (!Constant.SONG_PAUSE) {
				AnimationDrawable frameAnimation = (AnimationDrawable) imagePlaying.getBackground();
				frameAnimation.start();
			}
		} else {
			textNumber.setVisibility(View.VISIBLE);
			imagePlaying.setVisibility(View.GONE);
			textNumber.setText(String.valueOf(position + 1) + ".");
		}
		return convertView;
	}


}
