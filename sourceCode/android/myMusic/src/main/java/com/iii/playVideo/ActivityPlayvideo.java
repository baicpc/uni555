package com.iii.playVideo;

import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.mymusic.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class ActivityPlayvideo extends Activity {
	public static final String POSITION = "position";
	VideoView videoview;
	private MediaController mediaController;
	int position = 0;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_in_r, R.anim.slide_out_r);
		setContentView(R.layout.video_controller);
		videoview = (VideoView) findViewById(R.id.videoview);
		videoview.requestFocus();
		// get link video
		// lấy intent gọi Activity này
		Intent callerIntent = getIntent();
		// có intent rồi thì lấy Bundle dựa vào MyPackage
		Bundle bundle = callerIntent.getBundleExtra("video");
		String url = bundle.getString("linkvideo");
		String name = bundle.getString("name");
		setTitle(Html.fromHtml(name));
		Ultilities.showProgress(ActivityPlayvideo.this);
		playVideo(url);
	}

	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	public void playVideo(String url) {

		try {
			if (url.equals("") || url.length() == 0) {
				showToast("Link null");
			} else {
				Uri video = Uri.parse(url);
				// Init video control
				mediaController = new MediaController(this);
				mediaController.setMediaPlayer(videoview);
				videoview.setMediaController(mediaController);
				videoview.setVideoURI(video);
			}
			// Refesh like button when click list item
		} catch (Exception e) {
			Ultilities.hideProgress(ActivityPlayvideo.this);
			Log.e("Error", e.toString());
			e.printStackTrace();
		}
		videoview.setOnPreparedListener(videoPrepare);
		videoview.setOnErrorListener(videoError);
		videoview.setOnTouchListener(onTouchVideo);
		videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				finish();
				overridePendingTransition(R.anim.slide_in_l, R.anim.slide_out_l);
			}
		});
		// videoview.setOnErrorListener(videoError);
	}

	OnTouchListener onTouchVideo = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				if (mediaController.isShowing()) {
					mediaController.hide();
				} else {
					mediaController.show();
				}

			}
			return true;
		}
	};

	// video on prepare
	OnPreparedListener videoPrepare = new OnPreparedListener() {

		@Override
		public void onPrepared(MediaPlayer mp) {
			Ultilities.hideProgress(ActivityPlayvideo.this);
			if (position == 0) {
				videoview.start();
			} else {
				videoview.pause();
			}

		}
	};

	OnErrorListener videoError = new OnErrorListener() {

		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			Ultilities.hideProgress(ActivityPlayvideo.this);
			return false;
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_l, R.anim.slide_out_l);
		Constant.isContinue=true;
	};

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt(POSITION, videoview.getCurrentPosition());
		videoview.pause();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		position = savedInstanceState.getInt(POSITION);
		videoview.seekTo(position);
	}
}
