package com.iii.fragmentNews;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;

import com.iii.Modle.ResultAPI;
import com.iii.Ultils.Constant;
import com.iii.Ultils.UltilFunction;
import com.iii.Ultils.Ultilities;
import com.iii.Webservice.WSgetNewsbyidtype;
import com.iii.Webservice.WSgetType;
import com.iii.fragmentSong.DownloadSongMultiFileAsync;
import com.iii.fragmentSong.EntitySong;
import com.iii.fragmentType.AdapterType;
import com.iii.fragmentType.EntityType;
import com.iii.mymusic.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FragmentNews extends Fragment
        implements AdapterNews.customButtonDownloadListener, AdapterNews.customButtonShareListener, AdapterNews.customButtonAddListener {
    View view;
    EntitySong song;
    AdapterNews adapterSong = null;
    ArrayList<EntitySong> arrSong = null;
    TextView ivKhac, tvMoi, tvTitle;//, tvNhactre, tvTrutinh;
    ListView lvSong;
    PopupWindow popUpWindow;
    EntityTypeNews type;
    int userid;
    TextView tvcounterSong;
    ImageView imageOther;

    String id_list;
    static String nameType = "";
//    Calendar calendar;
    SimpleDateFormat dateFormat;

    private static FragmentNews fragment;

    public static FragmentNews getInstants() {
        if (fragment == null)
            fragment = new FragmentNews();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    ;

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        Calendar calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (id_list == null)
            id_list = dateFormat.format(calendar.getTime());
        view = inflater.inflate(R.layout.fragment_news, container, false);
        ivKhac = (TextView) view.findViewById(R.id.tvKhac);
        lvSong = (ListView) view.findViewById(R.id.lvSong);
        tvcounterSong = (TextView) view.findViewById(R.id.tvcounterSong);
        tvMoi = (TextView) view.findViewById(R.id.tvnew_hot);
//		tvNhactre = (TextView) view.findViewById(R.id.tvnhactre);
//		tvTrutinh = (TextView) view.findViewById(R.id.tvtrutinh);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        arrSong = new ArrayList<>();
        adapterSong = new AdapterNews(getActivity(), R.layout.custom_item_home, arrSong);
        adapterSong.setCustomButtonDownloadListener(FragmentNews.this);
        adapterSong.setCustomButtonShareListener(FragmentNews.this);
        adapterSong.setCustomButtonAddListener(FragmentNews.this);
        lvSong.setAdapter(adapterSong);
        if (nameType.equals("")) {
            nameType = getString(R.string.new_hot);
            tvTitle.setText(R.string.new_hot);
        } else {
            tvTitle.setText(nameType);
        }
        imageOther = (ImageView) view.findViewById(R.id.image_select_other_song);
        tvTitle.setTextColor(getResources().getColor(R.color.blue));

        lvSong.setEmptyView(view.findViewById(R.id.text_empty));
        lvSong.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                song = (EntitySong) parent.getItemAtPosition(position);
                Ultilities.stopMusicService(getActivity());
                Constant.SONG_INDEX = position;
                Constant.CURR_POSITION = 0;
                Ultilities.setListMusic(arrSong);
                Ultilities.UpdateNews(getActivity(), song.getId(), adapterSong, null, arrSong, position);
                adapterSong.notifyDataSetChanged();
                UltilFunction.updateTextListTitle(tvTitle.getText().toString());
            }
        });
        tvMoi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar calendar = Calendar.getInstance();
                id_list = dateFormat.format(calendar.getTime());
                getListSongbyidtype(id_list);
                tvTitle.setText(R.string.today);
                nameType = getResources().getString(R.string.today);
                // tvTitle.setTextColor(getResources().getColor(R.color.blue));
                // tvMoi.setTextColor(getResources().getColor(R.color.blue));
                // tvTrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
                // tvNhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
            }
        });
//		tvNhactre.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				id_list = 3;
//				getListSongbyidtype(id_list);
//				tvTitle.setText(R.string.today);
//				nameType = getResources().getString(R.string.nhactre);
//				// tvTitle.setTextColor(getResources().getColor(R.color.blue));
//				// tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
//				// tvNhactre.setTextColor(getResources().getColor(R.color.blue));
//				// tvTrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
//			}
//		});
//		tvTrutinh.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				id_list = 2;
//				getListSongbyidtype(id_list);
//				tvTitle.setText(R.string.trutinh);
//				nameType = getResources().getString(R.string.trutinh);
//				// tvTitle.setTextColor(getResources().getColor(R.color.blue));
//				// tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
//				// tvNhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
//				// tvTrutinh.setTextColor(getResources().getColor(R.color.blue));
//			}
//		});
        SharedPreferences sharedPreference = getActivity().getSharedPreferences(Constant.LOGIN_USER,
                Context.MODE_PRIVATE);
        userid = sharedPreference.getInt(Constant.USER_ID, 0);
        getListSongbyidtype(id_list);
        final ImageView imageOther = (ImageView) view.findViewById(R.id.image_select_other_song);
        imageOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });

        LinearLayout layoutOther = (LinearLayout) view.findViewById(R.id.layout_other);
        layoutOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (popUpWindow == null) {
                    showpopup(v);
                } else {
                    if (popUpWindow.isShowing()) {
                        popUpWindow.dismiss();
                    } else {
                        showpopup(v);
                    }
                }
            }
        });

        if (!UltilFunction.isConnectingToInternet(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.main_activity_no_internet), Toast.LENGTH_LONG).show();
        }

        return view;
    }

    public void getListSongbyidtype(String idtype) {
        try {
            arrSong.clear();
            adapterSong.clear();
            UltilFunction.startAsyncTask2(new WSgetNewsbyidtype(view.getContext(), idtype));
            WSgetNewsbyidtype.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(android.os.Message msg) {
                    if (Constant.RESULT_API != null) {
                        addToList(Constant.RESULT_API);
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("getListSongId", e.toString());
        } finally {
            adapterSong.notifyDataSetChanged();
        }
    }

    private void getType(final ArrayList<EntityTypeNews> arrType, final ArrayAdapter<EntityTypeNews> adapterType) {
        // TODO Auto-generated method stub
        try {
            final Calendar calendar = Calendar.getInstance();
            final Calendar calendar1 = Calendar.getInstance();
            final Calendar calendar2 = Calendar.getInstance();
            final Calendar calendar3 = Calendar.getInstance();
            calendar1.add(Calendar.DAY_OF_MONTH, -1);
            calendar2.add(Calendar.DAY_OF_MONTH, -2);
            calendar3.add(Calendar.DAY_OF_MONTH, -3);
            UltilFunction.startAsyncTask2(new WSgetType(getActivity()));
            WSgetType.HANDLER_SUCCESS = new Handler() {
                @Override
                public void handleMessage(android.os.Message msg) {
                    if (Constant.RESULT_API != null) {
                        if (Constant.RESULT_API.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
                            String strJson = "{\"records\":" + Constant.RESULT_API.getRecord() + "}";
                            try {
                                JSONObject jsonRootObject = new JSONObject(strJson);
                                // Get the instance of JSONArray that contains
                                // JSONObjects
                                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                                arrType.add(new EntityTypeNews(dateFormat.format(calendar.getTime()), "Today"));
                                arrType.add(new EntityTypeNews(dateFormat.format(calendar1.getTime()), "One day ago"));
                                arrType.add(new EntityTypeNews(dateFormat.format(calendar2.getTime()), "Two day ago"));
                                arrType.add(new EntityTypeNews(dateFormat.format(calendar3.getTime()), "Three day ago"));
//								for (int i = 0; i < jsonArray.length(); i++) {
//									JSONObject jsonObject = jsonArray.getJSONObject(i);
//									int id = Integer.parseInt(jsonObject.opt("idtype").toString());
//									String name = jsonObject.optString("nametype").toString();
//									Log.e("getType", id + "");
//									type = new EntityType(id, name);
//									arrType.add(type);
//								}
                                adapterType.notifyDataSetChanged();
                            } catch (Exception e) {

                            }
                        }
                    }
                }

                ;
            };
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("getType", e.toString());
        }
    }

    // public void getSong() {
    // try {
    // UltilFunction.startAsyncTask2(new WSgetNewsbyidtype(view.getContext(),
    // id_list));
    // WSgetNewsbyidtype.HANDLER_SUCCESS = new Handler() {
    // @Override
    // public void handleMessage(android.os.Message msg) {
    // if (Constant.RESULT_API != null) {
    // addToList(Constant.RESULT_API);
    // }
    // };
    // };
    // } catch (Exception e) {
    // // TODO: handle exception
    // }
    // }

    public void addToList(ResultAPI resultAPI) {
        arrSong.clear();
        adapterSong.clear();
        if (resultAPI.getCode().equalsIgnoreCase(Constant.RESULT_API_SUCCESS)) {
            String strJson = "{\"records\":" + resultAPI.getRecord() + "}";
            try {
                JSONObject jsonRootObject = new JSONObject(strJson);
                // Get the instance of JSONArray that contains JSONObjects
                JSONArray jsonArray = jsonRootObject.optJSONArray("records");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.optInt("idnews");
                    String name = jsonObject.optString("name");
                    String linksong = jsonObject.optString("linknews");
                    String lyric = jsonObject.optString("lyric","");
                    int counter = jsonObject.optInt("counter");
//                    int idplaylist = Integer.parseInt(jsonObject.getString("idplaylist"));
                    int idart = 0;
                    if (!jsonObject.optString("idartist").equals("null"))
                        idart = jsonObject.optInt("idartist");
                    String image = jsonObject.optString("image").toString();
                    String nameart = jsonObject.optString("artistname");
                    String filepath = jsonObject.optString("filepath").toString();
                    String driver = jsonObject.optString("linkdrive");
                    song = new EntitySong(id, name, idart, lyric, image, counter, linksong, nameart, filepath, driver);
                    arrSong.add(song);
                }
            } catch (Exception e) {
                Log.e("fragmentsong", e.toString());
            } finally {
                adapterSong.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onButtonDownloadClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        UltilFunction.startAsyncTask(new DownloadSongMultiFileAsync(getActivity()), new EntitySong[]{song});
    }

    @Override
    public void onButtonShareClickListener(EntitySong song, int position) {
        String url = song.getLinksong().replaceAll(" ", "%20");
        if (url.equals("") || url.equals("null")) {
            url = song.getFilepath().replaceAll(" ", "%20");
        }
        if (!url.equals("null") && !url.equals("")) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT,
                        song.getName() + " - " + url);

                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getActivity().getPackageManager()
                        .queryIntentActivities(shareIntent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith(
                            "com.facebook.katana")) {
                        shareIntent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }
                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u="
                            + song.getName() + " - " + url;
                    shareIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(sharerUrl));
                }

                getActivity().startActivity(
                        Intent.createChooser(shareIntent, "Share via"));
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    private void showpopup(View v) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_type, null);
            popUpWindow = new PopupWindow(layout, (int) ((8.2 * Constant.widthPixels) / 10), LayoutParams.WRAP_CONTENT);
            int location[] = new int[2];
            v.getLocationOnScreen(location);
            popUpWindow.setOutsideTouchable(true);
            imageOther.setImageResource(R.drawable.btn_choose);
            popUpWindow.setOnDismissListener(new OnDismissListener() {

                @Override
                public void onDismiss() {
                    imageOther.setImageResource(R.drawable.btn_chosesdown);
                }
            });
            popUpWindow.setBackgroundDrawable(new BitmapDrawable());
            popUpWindow.showAtLocation(layout, Gravity.NO_GRAVITY, Constant.widthPixels - popUpWindow.getWidth() - 2,
                    location[1] + v.getHeight());
            popUpWindow.setFocusable(true);
            popUpWindow.update();

            final GridView grvType = (GridView) layout.findViewById(R.id.grvType);

            ArrayList<EntityTypeNews> arrType = new ArrayList<EntityTypeNews>();
            ArrayAdapter<EntityTypeNews> adapterType = new AdapterTypeNews(getActivity(), R.layout.custom_item_menu_popup,
                    arrType);
            getType(arrType, adapterType);
            grvType.setAdapter(adapterType);
            grvType.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    if (popUpWindow.isShowing()) {
                        popUpWindow.dismiss();
                    }
                    type = (EntityTypeNews) parent.getItemAtPosition(position);
                    adapterSong.clear();
                    getListSongbyidtype(type.getIdtype());
                    adapterSong.notifyDataSetChanged();
                    tvTitle.setText(((EntityTypeNews) parent.getItemAtPosition(position)).getNametype());
                    tvTitle.setTextColor(getResources().getColor(R.color.blue));
                    tvMoi.setTextColor(getResources().getColor(R.color.ownhome_white));
//					tvTrutinh.setTextColor(getResources().getColor(R.color.ownhome_white));
//					tvNhactre.setTextColor(getResources().getColor(R.color.ownhome_white));
                    nameType = tvTitle.getText().toString();
                    id_list = type.getIdtype();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            e.toString();
        }
    }

    public void popupShowAddplaylist(final int idsong) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_playlist, null);
            final PopupWindow addplaylistPopup = new PopupWindow(getActivity());
            addplaylistPopup.setContentView(layout);
            addplaylistPopup.setWidth((1 * Constant.widthPixels) / 3);
            addplaylistPopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            addplaylistPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            addplaylistPopup.setFocusable(true);
            addplaylistPopup.update();

            final ListView lvplaylist = (ListView) layout.findViewById(R.id.lvplaylist_main_local);
            lvplaylist.setEmptyView(layout.findViewById(R.id.text_list_empty));
            ArrayList<EntityType> arrType = new ArrayList<EntityType>();
            ArrayAdapter<EntityType> adapterType = new AdapterType(getActivity(), R.layout.custom_item_type, arrType);
            Ultilities.getPlaylistUser(adapterType, arrType, getActivity(), userid);
            lvplaylist.setAdapter(adapterType);
            lvplaylist.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    addplaylistPopup.dismiss();
                    EntityType type = (EntityType) parent.getItemAtPosition(position);
                    Ultilities.InsertSong(getActivity(), type.getIdtype(), idsong);

                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void onButtonAddClickListener(EntitySong song, int position) {
        // TODO Auto-generated method stub
        popupShowAddplaylist(song.getId());
    }

}
